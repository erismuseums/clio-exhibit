// AnnotatedImage.js
// Used for the Annotated Image and Comparison activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Informational buttons at the bottom of the activity.  Opens a pop-up with more information.
	$(document).on("click", ".twentytwenty_identifier", function() {

		var title = $(this).data("title")
		var description = $(this).data("description")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"aiciden_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

		
	})

	//Toolbar - Toggle tooltip visibility.
	$(document).on("click", "#tooltip_toggle", function() {

		if ($(this).hasClass('hide')) {
			$(this).removeClass('hide')
			$(this).addClass('visible')

			$('.tooltip').each(function() {
				$(this).removeClass('secret')
				$(this).addClass('hint')
			})

		} else if ($(this).hasClass('visible')) {
			$(this).removeClass('visible')
			$(this).addClass('hide')

			$('.tooltip').each(function() {
				$(this).removeClass('hint')
				$(this).addClass('secret')
			})

		}

	})

	//Open each tooltip into an informational pop-up.
	$(document).on("click", ".tooltip", function() {

		var title = $(this).data("title")
		var description = $(this).data("description")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"aiciden_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadAnnotatedImage(program, content) {
	
	var program_details = loadProgramDetails(program)

	//Autohide the subheader

	var start_position = content.Configuration["Start Position"]
	var object1_image = content.Configuration.Images["Over"];
	var object2_image = content.Configuration.Images["Under"];

	var invisible = "";
	var fixed = "";
	if (content.References.Configuration["Fixed"] == true) {
		fixed = "fixed"
	}

	if (content.References.Configuration["Invisible"] == true) {
		invisible = "secret";
	}

	if (content.References.Configuration["Toggleable"] == true) {
		invisible = "hint";
		$("#twentytwenty").append('<div id="tooltip_toggle" class="visible"></div>');
	}

	//Add the 20/20 container

	if (object1_image["Image"]) {
		$('#twentytwenty').append('<img src="./content/' + program + '/@Media/' + object1_image["Image"] + '">')
	}

	if (object2_image["Image"]) {
		$('#twentytwenty').append('<img src="./content/' + program + '/@Media/' + object2_image["Image"] + '">')
	}



	if (object2_image["Image"] == "") {
		$("#twentytwenty").wrap('<div class="twentytwenty-wrapper"></div>')
		$("#twentytwenty").append('<div class="twentytwenty-overlay"></div>');
	} else if (start_position != undefined) {
		$("#twentytwenty").twentytwenty({
			default_offset_pct: start_position,
			move_with_handle_only: false
		});
	} else {
		$("#twentytwenty").twentytwenty({
			move_with_handle_only: false
		});
	}

	if (object1_image["Header"] != undefined && object1_image["Description"]) {
		var parsed1 = BBCodeParser.process(object1_image["Description"]);
		$('#twentytwenty').append('<div class="twentytwenty_identifier" id="twentytwenty_image1" data-description="' + parsed1 + '" data-title="' + object1_image["Header"] + '" ><div class="button"></div><div class="title">' + object1_image["Header"] + '</div></div>');
	}

	if (object2_image["Header"] != undefined && object2_image["Description"]) {
		var parsed2 = BBCodeParser.process(object2_image["Description"]);
		$('#twentytwenty').append('<div class="twentytwenty_identifier" id="twentytwenty_image2" data-description="' + parsed2 + '" data-title="' + object2_image["Header"] + '" ><div class="title">' + object2_image["Header"] + '</div><div class="button"></div></div>');
	}

	var tooltips = content.References["Tooltips"]

	$.each(tooltips, function(index, val) {
		var x = val.Geometry.Position["x"];
		var y = +val.Geometry.Position["y"];
		var radius = +val.Geometry["Radius"];
		var diameter = +radius * 2;

		var title = val["Title"];
		
		var content = "";
		var content_type = val.Content["Type"];
		var content_data = val.Content["Data"];
		
		switch(content_type){
			case "Rich Text":

				content = BBCodeParser.process(content_data);
				break;
		}

		

		$('#twentytwenty .twentytwenty-overlay').append('<div class="tooltip ' + fixed + ' ' + invisible + '" style="margin-left: -' + radius + 'px; margin-top: -' + radius + 'px; width: ' + diameter + 'px; height: ' + diameter + 'px; left:' + x + 'px; top: ' + y + 'px" data-title="' + title + '" data-description="' + content + '"></div>')
	})

}