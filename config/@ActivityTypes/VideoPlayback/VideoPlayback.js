// VideoPlayback.js
// Used for the Video Playback activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Replay video pop-up button.
	$(document).on("click", "#replay", function() {

		$("#jquery_jplayer_1").jPlayer('play')
		$('#jp-control-bar').show();
		$('#replay').hide();
	});

	//Initialize currentVol.
  var currentVol;

  //On page load, set current volume level.
  $("#jquery_jplayer_1").bind($.jPlayer.event.ready, function(event){
    currentVol = event.jPlayer.options.volume;
  });

  //On volume change, update the current volume level.
  $("#jquery_jplayer_1").bind($.jPlayer.event.volumechange, function(event){
    currentVol = event.jPlayer.options.volume;
  });

  //Mute/unmute button scripting.
  $(document).on("click",'#volume',function(){

    //Format currentVol for setVolumeBar function.
    currentVol = Number(currentVol).toFixed(2).replace('0.','').replace('.','');

    //If player is currently muted, then unmute and resume playback.
    if ($('#jp-control-bar').hasClass("jp-state-muted")){
      $(this).removeClass('muted');
      $("#jquery_jplayer_1").jPlayer("unmute");
      $("#jquery_jplayer_1").jPlayer("play");

      //Use switch function to apply current volume level to bar.
      setVolumeBar(currentVol);

    } else {
      //If not already muted, then mute.
      $(this).addClass('muted');
      $("#jquery_jplayer_1").jPlayer("mute");
      $("#jquery_jplayer_1").jPlayer("pause");
      
      $('.volume-level').each(function(){
        $(this).removeClass('selected');
      })

    }
  });

  //Individual volume notch function.
  $(document).on("click",'.volume-level',function(){
    var volume = $(this).attr('id').replace('vol_','');

    if ($('#jp-control-bar').hasClass("jp-state-muted")){
      $("#volume").removeClass('muted');
      $("#jquery_jplayer_1").jPlayer("unmute");
      $("#jquery_jplayer_1").jPlayer("play");
    } 

    setVolumeBar(volume);
  });

  //Rewind function.
   $(document).on("click",'.jp-rewind',function(){

    var currentTime = $('#jquery_jplayer_1').data('jPlayer').status.currentTime;
    
    if (currentTime < 10) {
      $("#jquery_jplayer_1").jPlayer("play", 0);     
    } else {
      $("#jquery_jplayer_1").jPlayer("play", currentTime - 10);
    }

  });

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadVideoPlayback(program, content) {

	
	var video_1 = content["1"]

  console.log(content)

	$("#jquery_jplayer_1").jPlayer({
		ready: function(event) {
			$(this).jPlayer("setMedia", {
				m4v: './content/' + program + '/@Media/' + video_1["Video"]
			})
			$('#jp-control-bar').show()

			$("#jquery_jplayer_1").jPlayer('play')


		},
		ended: function() {
			$('#replay').show();
			$('#jp-control-bar').hide()
		},
		swfPath: "js/jplayer/jplayer",
		volume: 0.25,
		supplied: "m4v",
		size: {
			width: "",
			height: ""
		},
		cssSelectorAncestor: "#jp-control-bar",
		useStateClassSkin: true,
		smoothPlayBar: true,
		remainingDuration: true,

		cssSelector: {
			play: '.jp-play',
			pause: '.jp-pause',
			seekBar: '.jp-seek-bar',
			playBar: '.jp-play-bar',
			duration: '.jp-duration',
			noSolution: '.jp-no-solution'
		}

	});

	if (video_1["IncludeAudio"] == "false") {
		$('#jp-control-bar').addClass('no-audio')
		$("#jquery_jplayer_1").jPlayer("mute");
	} else if (video_1["IncludeAudio"] == "true") {
		$('#jp-control-bar').removeClass('no-audio')
		$("#jquery_jplayer_1").jPlayer("unmute");
	}

	$('#jquery_jplayer_1').idle({
		onIdle: function() {
			$('#jp-control-bar').fadeOut(500)
		},
		onActive: function() {
			$('#jp-control-bar').show()
		},
		idle: 5000
	})

}

function setVolumeBar(setToVolume){
    
    //Set the volume.
    switch(setToVolume){

      //Set volume to 25%.
      case "25":
        $("#jquery_jplayer_1").jPlayer("volume", 0.25);
        
        $('#vol_50, #vol_75, #vol_100').removeClass('selected');
        $('#vol_25').addClass('selected');
        
        break;

      //Set volume to 50%.
      case "50":
        $("#jquery_jplayer_1").jPlayer("volume", 0.50);

        $('#vol_75, #vol_100').removeClass('selected');
        $('#vol_25, #vol_50').addClass('selected');
        break;

      //Set volume to 75%.
      case "75":
        $("#jquery_jplayer_1").jPlayer("volume", 0.75);
        
        $('#vol_100').removeClass('selected');
        $('#vol_25, #vol_50, #vol_75').addClass('selected');
        break;

      //Set volume to 100%.
      case "100":
        $("#jquery_jplayer_1").jPlayer("volume", 1.00);

        $('#vol_25, #vol_50, #vol_75, #vol_100').addClass('selected');
        break;
    }
  }
