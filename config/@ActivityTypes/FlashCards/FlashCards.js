// FlashCards.js
// Used for the Flashcards activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Flip a card from one side to the other.
	$(document).on("click", ".card", function() {

		$.each($(this).siblings('.card'), function(index, val) {
			if ($(this).find('.front').hasClass('open') == true) {
				$(this).find('.front').removeClass('open')
				$(this).find('.front').addClass('closed')
			}
		})

		if ($(this).find('.front').hasClass('open') == true) {

			$(this).find('.front').removeClass('open')
			$(this).find('.front').addClass('closed')


		} else

		if ($(this).find('front').hasClass('closed') == false) {

			$(this).find('.front').removeClass('closed')
			$(this).find('.front').addClass('open')

		}
	})

	//View more information about this card by opening an informational pop-up.
	$(document).on("click", "span.card_more", function() {

		var title = $(this).data("title")
		var description = $(this).data("description")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":"auto",
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cg_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadFlashCards(program, content) {
	var program_details = loadProgramDetails(program)

	$.each(content, function(index, val) {
		var header = val["Header"];
		var description = val.Description["Brief"];
		var full_description = BBCodeParser.process(val.Description["Full"]);
		var image_url = "content/" + program + "/@Media/" + val["Image"]
		//$('#activity_content .owl-carousel').append("<div class='slide' data-dot='" + index + "'>" + content + "<div class='owl-next-button'></div></div>")

		var background = ""
		if (val["Image"] != "") {
			background = "background"
		}

		$('#flash_cards .owl-carousel').append('<div class="card"><div class="front closed"><div class="description"><div class="description_header">' + header + '</div><div class="description_content"><span>' + description + '</span></div></div><span class="card_more" data-title="' + header + '" data-description="' + full_description + '"></span></div><div class="back" style="background-image: url(' + image_url + ');"><span class="description ' + background + '">' + header + '</span></span></div></div>')
	})

	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 0,
		dots: true,
		nav: true,
		items: 1,
		slideTransition: 'none'
	})

	$('.owl-carousel').on('changed.owl.carousel', function(event) {
		$('.card').each(function() {
			$(this).find('.front').removeClass('open')
			$(this).find('.front').addClass('closed')
		})
	})

}


