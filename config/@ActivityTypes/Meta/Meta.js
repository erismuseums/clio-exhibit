// Meta.js
// Used for the Meta activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Loading a child activity within a meta activity.
	$(document).on("click", '#meta_activities .activity', function() {

		var url = getCurrentURL();

		var program = $(this).data("box");
		var activity = $(this).data("activity");

		$('#meta_activities').hide();
		$('#meta_content').show();
		$('#meta_back').show();
		$('#return_home').addClass('meta');

		loadChildActivity(program, activity);

	})

	//Return from the child of a meta activity to the main activity.
	$(document).on("click", '#meta_back', function() {

		$('#meta_back').hide();
		$('#meta_content').hide();
		$('#meta_activities').show();
		$('#return_home').removeClass('meta');
		$('#meta_content').html('')
		$('body').removeClass("child_activity");

	});


})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadMeta(program, content, container) {

	$.each(content, function(index, val) {

		var activity_details = loadActivityDetails(val["Program"], val["Activity"])

		var description = val['Description']


		$("#meta_activities").append('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '" data-box="' + val["Program"] + '" data-activity="' + val["Activity"] + '"><div class="image" style="background-image: url(content/' + val['Program'] + '/@Media/' + val['Preview'] + ')" ></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div></div><div class="activity_description">' + description + '</div><div class="activity_link"></div></div></div>');
	})

	initializeContainerScrollBar('#meta_activities',true,false,true)
};

function loadChildActivity(program, activity) {

	var activity_details = loadActivityDetails(program, activity);


	var type = activity_details.Activity["Type"]
	var content = activity_details.Content
	

	var url = getCurrentURL();

	//Load the program details.
	var program_details = loadProgramDetails(program)

	var loaded_types = $("#activity_content").attr("data-types")
	
	var types_JSON = {}
	
	if (loaded_types != "" && loaded_types != undefined){
		types_JSON = JSON.parse(loaded_types);
	} 

	//Get Information about the Activity Types.
	var file = "config/@ActivityTypes/" + type + "/@About.json";
	var activitytype_content = parseJSONContent(file);

	var initScript = activitytype_content.Initialize["Script"];
	var initFunction = activitytype_content.Initialize["Function"];

	if(!types_JSON.hasOwnProperty(type)){
		//Get the CSS to add.
		var stylesheet_directory = "../config/@ActivityTypes/" + type + "/@Stylesheets/";
		var available_stylesheets = parseDirectory(stylesheet_directory);

		$.each(available_stylesheets,function(index,val){
			//Apply any custom stylesheets.
	    	$('head').append('<link rel="stylesheet" href="' + url['base'] + "config/@ActivityTypes/" + type + "/@Stylesheets/" + val + '" type="text/css" />')
		})
	}
		

	//Creates the HTML template to load.
	var template_file = url['base'] + "config/@ActivityTypes/" + type + "/@Templates/" + activitytype_content.Initialize["Template"] + ".html";

	//Loads the HTML file into a variable.
	var activity_template = loadHTML(template_file);

	var container = "#meta_content";
	
	//Append the template to the activity container.
	$(container).append(activity_template)

	if(!types_JSON.hasOwnProperty(type)){ 
		//Get the JS libraries to add.
		var library_directory = "../config/@ActivityTypes/" + type + "/@Libraries/";
		var available_libraries = parseDirectory(library_directory);

		$.each(available_libraries,function(index,val){
			//Apply any custom JavaScript.
	    	
	    	var script = url['base'] + "config/@ActivityTypes/" + type + "/@Libraries/" + val;

			$.getScript(script, function() {
			    console.log(script + " script loaded and executed.");

			});

		})

		$.getScript(url['base'] + "config/@ActivityTypes/" + type + "/" + initScript, function() {
			window[initFunction](program, content);
		});
	} else {
		$.getScript(url['base'] + "config/@ActivityTypes/" + type + "/" + initScript, function() {
			window[initFunction](program, content);
		});
	}

	types_JSON[type] = { "loaded":"true"}
	var types_encode = JSON.stringify(types_JSON);

	$("#activity_content").attr("data-types",types_encode)
	$('body').addClass("child_activity");
	






}















