// Timeline.js
// Used for the Timeline activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	
	//Open an image into the lightbox.
    $(document).on("click", ".tl_image", function() {
		
		var link = $(this).data("link")
		

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cg_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": {
	        	src: link,
	        	caption: undefined
	        }
	      }
	      
	    };
	    createLookCloserPopUp(options);

	})



})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadTimeline(program, content) {

	var date_width = content.Configuration['Date Width']

	$.each(content.Dates, function(index, val) {
		$('#dates').append('<li style="width: ' + date_width + 'px !important"><a href="#">' + index + '</a></li>')

		var content = BBCodeParser.process(val["Content"])
		var safe_index = index.replace(/\./g, '_')
		var image_url = "./content/" + program + "/@Media/" + val["Image"]



		$('#issues').append('<li id="timeline' + safe_index + '"><div class="timeline_content"><div class="tl_image" data-link="' + image_url + '" style="background-image:url(' + image_url + ')"><div class="tl_expand" ></div></div><div class="content">' + content + '</div></div></li>')

		if (val["Image"] == "") {
			$("#timeline" + safe_index + " .tl_image").hide()
			console.log('hiding')
		}

	})


	$().timelinr({
		prevButton: '#time_prev',
		nextButton: '#time_next',
		datesSpeed: 0,
		issuesSpeed: 0,
		issuesTransparencySpeed: 100
	});

}


