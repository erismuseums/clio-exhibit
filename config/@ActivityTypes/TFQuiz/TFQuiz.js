// BinaryQuiz.js
// Used for the Binary Quiz activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	$(document).on("click", 'button.affirmative, button.negative', function() {

		var clicked = $(this).attr('class')
		var current_slide = $('#tfquiz').attr("data-question");


		var answer = $('#question_' + current_slide).data("answer")
		var correct_response = $('#question_' + current_slide).data("correct")
		var incorrect_response = $('#question_' + current_slide).data("incorrect")

		var response = ""
		var interaction = ""

		if(clicked == answer){
			
			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"success",
		        "Text":"Correct!"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		          "Type":"custom",
		          "HTML":'<button class="lookcloser_next" id="tfq_next">Next</button>',
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + correct_response + '</div>'
		      }
		    };

		} else {
			
			var options = {
		      "Header":{
		        "Style":"error",
		        "Text":"Try again."
		      },
		      "Scrollbar":false,
		      "Interaction":{
		          "Type":"custom",
		          "HTML":'<button class="lookcloser_tryagain" id="tfq_retry">Retry</button><button class="lookcloser_next" id="tfq_skip">Skip</button>',
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + incorrect_response + '</div>'
		      }
		    };

		}

		createLookCloserPopUp(options);


	})

	//Next Question
	$(document).on("click", '#tfq_next, #tfq_skip', function() {
		
		var current_slide = $('#tfquiz').attr("data-question");
		var next_slide = +current_slide + 1;

		$.each($(".question"),function(index, val){

			var slide_name = "question_" + next_slide;

			if($(this).attr("id") != slide_name){
				console.log('hiding')
				$(this).hide()

			} else {
				console.log('showing')
				$(this).show()

			}
			

		})

		$("#tfquiz").attr("data-question",next_slide);
		destroyLookCloserPopUp();
	});

	//Retry Question.
	$(document).on("click", '#tfq_retry', function() {
		destroyLookCloserPopUp();
	});

	//Open the question image
	$(document).on("click", ".question_image", function() {

		var gallery = $(this).data("gallery");
		var link = $(this).data("link")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"tfq_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": {
				src: link,
				caption: ""
			}
	      }
	      
	    };
	    createLookCloserPopUp(options);

	})


	
})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadTFQuiz(program, content) {
	
	$.each(content, function(index, val) {

		$('#tfquiz').append('<div class="question" id="question_' + index + '" data-answer="' + val["Answer"] + '" data-correct="' + val.Response["Correct"] + '" data-incorrect="' + val.Response["Incorrect"] + '"></div>');

		var image_url = "content/" + program + "/@Media/" + val.Content["Image"];

		var image_hidden = "";
		var text_hidden = "";

		var all_hidden = "";

		if(val.Content["Image"] == "" || val.Content["Image"] == undefined){
			image_hidden = "hidden"
		}

		if(val.Content["Text"] == "" || val.Content["Text"] == undefined){
			text_hidden = "hidden"
		}

		if(image_hidden == "hidden" && text_hidden == "hidden"){
			all_hidden = "hidden";
		}

		console.log(all_hidden)

		var richtext = BBCodeParser.process(val.Content["Text"]);
		console.log(richtext)


		$('#question_' + index).append('<div class="content_wrapper ' + all_hidden + '"><div class="question_description ' + text_hidden + '">' + richtext + '</div><div class="question_image ' + image_hidden + '" data-link="' + image_url + '" style="background-image: url(' + image_url + ')"><div class="tf_expand" ></div></div></div>');
		$('#question_' + index).append('<div class="text_wrapper"></div>');
		$('#question_' + index + ' .text_wrapper').append('<div class="text">' + val.Question["Text"] + '</div>');

		var affirmative = "";
		var negative = ""

		switch (val.Question["Type"]) {
			case "True_False":
				affirmative = "True";
				negative = "False";
				break;

			case "Yes_No":
				affirmative = "Yes";
				negative = "No";
				break;

			case "Affirmative_Negative":
				affirmative = "Affirmative";
				negative = "Negative";
				break;

			case "none":
				affirmative = "";
				negative = "";
				break;
		}

		$('#question_' + index + ' .text_wrapper').append('<div class="response"><div class="true"><button class="affirmative">' + affirmative + '</button></div><div class="false"><button class="negative">' + negative + '</button></div></div>');


	})

}