// PathFinder.js
// Used for the Path Finder activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {
	

	$(window).resize(function() {
	  var current_question = parseInt($("#pathfinder").attr("data-current_question")) - 1;

	  updateLines(current_question);
	  
	});

	$(document).on("click","#reset_path",function(){
		location.reload();
	});

	$(document).on("click",".button_wrapper.enabled .button",function(){
		
		if($(this).parents(".button_wrapper").hasClass("selected")){
			
			$(this).parents(".column").find(".button_wrapper").each(function(){
				$(this).removeClass("selected");
				$(this).removeClass("unselected")
			})

			$("#confirm_path").removeClass("enabled");
			$("#confirm_path").addClass("disabled");

		} 

		else {
			
			$(this).parents(".column").find(".button_wrapper").each(function(){
				$(this).removeClass("selected");
				$(this).addClass("unselected")
			})
			$(this).parents(".button_wrapper").removeClass("unselected");
			$(this).parents(".button_wrapper").addClass("selected");

			$("#confirm_path").removeClass("disabled");
			$("#confirm_path").addClass("enabled");

		}

		
	})

	$(document).on("click", "#badge_origin, #badge_destination", function() {
		
		var title = $(this).find('.info_button').attr("data-title");
		var image = $(this).attr("data-image");
		console.log(image)

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"pf_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data":{
							"src": image,
							"caption": ""
						}
	      }
	    };
	    createLookCloserPopUp(options);


	    return false;

	});

	$(document).on("click", ".info_button", function() {
		
		var title = $(this).attr("data-title");
		var description = $(this).attr("data-description");

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"pf_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);


	    return false;

	});

	$(document).on("click","#confirm_path.enabled",function(){
		
		var current_question = $("#pathfinder").attr("data-current_question");
		
		var correct_answer = $(".column[data-qid='" + current_question + "']").attr("data-answer");
		var correct_response = $(".column[data-qid='" + current_question + "']").attr("data-correct");
		var incorrect_response = $(".column[data-qid='" + current_question + "']").attr("data-incorrect");
		var selected_answer = $(".column[data-qid='" + current_question + "']").find(".button_wrapper.selected .button").attr("data-oid")

		if(selected_answer == correct_answer ){
			
			var buttons;
			if(current_question == 4){
				buttons = '<button class="lookcloser_reset" id="pfdone">Restart</button>';
			} else {
				buttons = '<button class="lookcloser_next" id="pfcorrect_close">Next</button>';
			}

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"success",
		        "Text":"Correct!"
		      },
		      "Scrollbar":"auto",
		      "Interaction":{
		          "Type":"custom",
		          "HTML": buttons,
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + correct_response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);
	    	return false;



		}

		else {

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"error",
		        "Text":"Try Again"
		      },
		      "Scrollbar":"auto",
		      "Interaction":{
		          "Type":"custom",
		          "HTML":'<button class="lookcloser_tryagain" id="pfincorrect_close">Retry</button>',
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + incorrect_response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);
	    	return false;

		}
	})


	$(document).on("click", "#pfcorrect_close", function() {
		var current_qid = parseInt($("#pathfinder").attr("data-current_question"));
		
		$(".column[data-qid='" + current_qid + "']").find(".button_wrapper.unselected").each(function(){
			$(this).addClass("incorrect");
			$(this).removeClass("unselected")
			$(this).removeClass("enabled")
		})


		$(".column[data-qid='" + current_qid + "']").find(".button_wrapper.selected").addClass("correct")
		$(".column[data-qid='" + current_qid + "']").find(".button_wrapper.selected").removeClass("enabled")
		$(".column[data-qid='" + current_qid + "']").find(".button_wrapper.selected").removeClass("selected")
		

		var new_qid = current_qid + 1;
		console.log("new_QID",new_qid)
		
		if (new_qid <= 3){
			var new_question = $(".column[data-qid='" + new_qid + "']").attr("data-question")
			$(".column[data-qid='" + new_qid + "'] .column_header").removeClass("disabled")
			$(".column[data-qid='" + new_qid + "'] .column_header").addClass("enabled")

			$(".column[data-qid='" + new_qid + "']").find(".button_wrapper").each(function(){
				$(this).addClass("enabled");
				$(this).removeClass("disabled")
			})

			
		} else {
			var new_question = $("#badge_destination").attr("data-prompt");
			$("#confirm_path").hide();
			$("#reset_path").show();
		}
			
		$('.question_area').html(new_question)

		removeSelectorClassByPrefix("#pathfinder", "question");
		$("#pathfinder").addClass("question" + new_qid)		

		$("#pathfinder").attr("data-current_question",new_qid)
		
		$("#confirm_path").removeClass("enabled");
		$("#confirm_path").addClass("disabled");
		console.log(current_qid)

		

		updateLines(current_qid);
		
		
			tippy('[data-tippy-content]');

		destroyLookCloserPopUp();
	
	});

	$(document).on("click", "#pfincorrect_close", function() {
		var current_qid = $("#pathfinder").attr("data-current_question");

		$(".column[data-qid='" + current_qid + "']").find(".button_wrapper").each(function(){
				$(this).removeClass("selected");
				$(this).removeClass("unselected")
		})

		$("#confirm_path").removeClass("enabled");
		$("#confirm_path").addClass("disabled");
		destroyLookCloserPopUp();
	});

	$(document).on("click", "#pfdone", function() {
		location.reload();
	})
})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadPathFinder(program, content, container) {

	console.log(getCurrentURL());

	//Origin Badge
	var origin = content["Origin"];
	var origin_image = 'content/' + program + '/@Media/' + origin["Image"];
	$("#badge_origin").css("background-image","url(" + origin_image + ")")
	$("#badge_origin").attr("data-image",origin_image)	
	$("#badge_origin").attr("data-tippy-content",origin["Title"])
	$("#badge_origin .info_button").attr("data-title",origin["Title"]);
	var origin_description = BBCodeParser.process(origin["Description"])
	$("#badge_origin .info_button").attr("data-description",origin_description);

	

	//Row 1
	var question_1 = content.Questions["1"];
	$("#column1 .column_header").html(question_1["Header"])
	$('#column1').attr("data-question",question_1["QuestionText"])
	

	$('#column1').attr("data-answer",question_1["QuestionAnswer"])
	var column1_correct = BBCodeParser.process(question_1["CorrectResponse"]);
	$('#column1').attr("data-correct",column1_correct)
	var column1_incorrect = BBCodeParser.process(question_1["IncorrectResponse"]);
	$('#column1').attr("data-incorrect",column1_incorrect)
	
	$('.question_area').html(question_1["QuestionText"])
	
	var q1_1 = question_1.Options["1"];
	var q1_2 = question_1.Options["2"];
	var q1_3 = question_1.Options["3"];
	var q1_4 = question_1.Options["4"];
	
	$('#row1_1 .title').html(q1_1["Title"])
	$('#row1_1 .info_button').attr("data-title",q1_1["Title"]);
	$('#row1_1 .info_button').attr("data-description",q1_1["Description"]);

	$('#row1_2 .title').html(q1_2["Title"])
	$('#row1_2 .info_button').attr("data-title",q1_2["Title"]);;
	$('#row1_2 .info_button').attr("data-description",q1_2["Description"]);

	$('#row1_3 .title').html(q1_3["Title"])
	$('#row1_3 .info_button').attr("data-title",q1_3["Title"]);
	$('#row1_3 .info_button').attr("data-description",q1_3["Description"]);

	$('#row1_4 .title').html(q1_4["Title"])
	$('#row1_4 .info_button').attr("data-title",q1_4["Title"]);
	$('#row1_4 .info_button').attr("data-description",q1_4["Description"]);

	//Row 2
	var question_2 = content.Questions["2"];
	$("#column2 .column_header").html(question_2["Header"])
	$('#column2').attr("data-question",question_2["QuestionText"])

	$('#column2').attr("data-answer",question_2["QuestionAnswer"])
	var column2_correct = BBCodeParser.process(question_2["CorrectResponse"]);
	$('#column2').attr("data-correct",column2_correct)
	var column2_incorrect = BBCodeParser.process(question_2["IncorrectResponse"]);
	$('#column2').attr("data-incorrect",column2_incorrect)

	var q2_1 = question_2.Options["1"];
	var q2_2 = question_2.Options["2"];
	var q2_3 = question_2.Options["3"];
	var q2_4 = question_2.Options["4"];
	
	$('#row2_1 .title').html(q2_1["Title"])
	$('#row2_1 .info_button').attr("data-title",q2_1["Title"]);
	$('#row2_1 .info_button').attr("data-description",q2_1["Description"]);

	$('#row2_2 .title').html(q2_2["Title"])
	$('#row2_2 .info_button').attr("data-title",q2_2["Title"]);
	$('#row2_2 .info_button').attr("data-description",q2_2["Description"]);

	$('#row2_3 .title').html(q2_3["Title"])
	$('#row2_3 .info_button').attr("data-title",q2_3["Title"]);
	$('#row2_3 .info_button').attr("data-description",q2_3["Description"]);

	$('#row2_4 .title').html(q2_4["Title"])
	$('#row2_4 .info_button').attr("data-title",q2_4["Title"]);
	$('#row2_4 .info_button').attr("data-description",q2_4["Description"]);


	//Row 3
	var question_3 = content.Questions["3"];
	$("#column3 .column_header").html(question_3["Header"])
	$('#column3').attr("data-question",question_3["QuestionText"])
	$('#column3').attr("data-answer",question_3["QuestionAnswer"])
	var column3_correct = BBCodeParser.process(question_3["CorrectResponse"]);
	$('#column3').attr("data-correct",column3_correct)
	var column3_incorrect = BBCodeParser.process(question_3["IncorrectResponse"]);
	$('#column3').attr("data-incorrect",column3_incorrect)

	var q3_1 = question_3.Options["1"];
	var q3_2 = question_3.Options["2"];
	var q3_3 = question_3.Options["3"];
	var q3_4 = question_3.Options["4"];
	
	$('#row3_1 .title').html(q3_1["Title"])
	$('#row3_1 .info_button').attr("data-title",q3_1["Title"]);
	$('#row3_1 .info_button').attr("data-description",q3_1["Description"]);

	$('#row3_2 .title').html(q3_2["Title"])
	$('#row3_2 .info_button').attr("data-title",q3_2["Title"]);
	$('#row3_2 .info_button').attr("data-description",q3_2["Description"]);

	$('#row3_3 .title').html(q3_3["Title"])
	$('#row3_3 .info_button').attr("data-title",q3_3["Title"]);
	$('#row3_3 .info_button').attr("data-description",q3_3["Description"]);

	$('#row3_4 .title').html(q3_4["Title"])
	$('#row3_4 .info_button').attr("data-title",q3_4["Title"]);
	$('#row3_4 .info_button').attr("data-description",q3_4["Description"]);

	//Destination Badge
	var destination = content["Destination"];
	var destination_image = 'content/' + program + '/@Media/' + destination["Image"];
	$("#badge_destination").css("background-image","url(" + destination_image + ")")	
	$("#badge_destination").attr("data-image",destination_image);
	$("#badge_destination").attr("data-tippy-content",destination["Title"]);
	$("#badge_destination").attr("data-prompt",destination["Prompt"])
	$("#badge_destination .info_button").attr("data-title",destination["Title"]);
	var destination_description = BBCodeParser.process(destination["Description"])
	$("#badge_destination .info_button").attr("data-description",destination_description);


	$('#column1 .options').shuffleChildren();
	$('#column2 .options').shuffleChildren();
	$('#column3 .options').shuffleChildren();



	tippy('[data-tippy-content]');

};

function updateLines(qid){
	
	$("#line_area").html("")
	var activityWidth = $("#line_area").width();
	var activityHeight = $("#line_area").height();
	
	console.log(qid)
	// Make an instance of two and place it on the page.
	var elem = document.getElementById('line_area');
	var params = { width: activityWidth, height: activityHeight};
    var two = new Two(params).appendTo(elem);

	switch(qid){
		

		case 3:
			console.log("running 3")
			var column2Offset = $("#column2 .correct").offset();
			var column2Width = $("#column2 .correct").width();
			var column2Height = $("#column2 .correct").height();

			var column2X = (column2Offset.left + column2Width / 2);
			var column2Y = (column2Offset.top + column2Height / 2) - 60;

			var path3Offset = $("#column3 .correct").offset();
			var path3Width = $("#column3 .correct").width();
			var path3Height = $("#column3 .correct").height();

			var path3X = (path3Offset.left + path3Width / 2);
			var path3Y = (path3Offset.top + path3Height / 2) - 60;

			console.log(column2X, column2Y,path3X,path3Y)
		    
		    var line3 = two.makeLine(column2X, column2Y, path3X, path3Y);
		    line3.linewidth = 15;
		    line3.stroke = "#8cbe8b";

		    var title3 = $("#column3 .correct .info_button").attr("data-title");
		    $("#column3 .correct .info_button").attr("data-tippy-content",title3);

		    console.log("running 4")
			var column3Offset = $("#column3 .correct").offset();
			var column3Width = $("#column3 .correct").width();
			var column3Height = $("#column3 .correct").height();

			var column3X = (column3Offset.left + column3Width / 2);
			var column3Y = (column3Offset.top + column3Height / 2) - 60;

			var badgeDestOffset = $("#badge_destination").offset();
			var badgeDestWidth = $("#badge_destination").width();
			var badgeDestHeight = $("#badge_destination").height();

			var badgeDestX = (badgeDestOffset.left + badgeDestWidth / 2);
			var badgeDestY = (badgeDestOffset.top + badgeDestHeight / 2) - 60;

			console.log(column3X, column3Y,badgeDestX,badgeDestY)
		    
		    var line3 = two.makeLine(column3X, column3Y, badgeDestX, badgeDestY);
		    line3.linewidth = 15;
		    line3.stroke = "#8cbe8b";

		case 2:
			console.log("running 2")
			var column1Offset = $("#column1 .correct").offset();
			var column1Width = $("#column1 .correct").width();
			var column1Height = $("#column1 .correct").height();

			var column1X = (column1Offset.left + column1Width / 2);
			var column1Y = (column1Offset.top + column1Height / 2) - 60;

			var path2Offset = $("#column2 .correct").offset();
			var path2Width = $("#column2 .correct").width();
			var path2Height = $("#column2 .correct").height();

			var path2X = (path2Offset.left + path2Width / 2);
			var path2Y = (path2Offset.top + path2Height / 2) - 60;

			console.log(column1X, column1Y,path2X,path2Y)
		    
		    var line2 = two.makeLine(column1X, column1Y, path2X, path2Y);
		    line2.linewidth = 15;
		    line2.stroke = "#8cbe8b";

		    var title2 = $("#column2 .correct .info_button").attr("data-title");
		    $("#column2 .correct .info_button").attr("data-tippy-content",title2);

		case 1:
			console.log("running 1")
			var badgeOffset = $("#badge_origin").offset();
			var badgeWidth = $("#badge_origin").width();
			var badgeHeight = $("#badge_origin").height();

			var badgeX = (badgeOffset.left + badgeWidth / 2);
			var badgeY = (badgeOffset.top + badgeHeight / 2) - 60;

			var path1Offset = $("#column1 .correct").offset();
			var path1Width = $("#column1 .correct").width();
			var path1Height = $("#column1 .correct").height();

			var path1X = (path1Offset.left + path1Width / 2);
			var path1Y = (path1Offset.top + path1Height / 2) - 60;


		    var line1 = two.makeLine(badgeX, badgeY, path1X, path1Y);
		    line1.linewidth = 15;
		    line1.stroke = "#8cbe8b";

		    var title1 = $("#column1 .correct .info_button").attr("data-title");
		    $("#column1 .correct .info_button").attr("data-tippy-content",title1);

			
			

			break;

		}

		two.update();

	
}
