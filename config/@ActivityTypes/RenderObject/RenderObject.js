// RenderObject.js
// Used for the Render Object activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Information Bar - Button about Object
	$(document).on("click", "#render_info", function() {

		var title = $(this).data("title")
		var description = $(this).data("description")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"renderiden_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);


	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadRenderObject(program, content, container) {
	
	var url = getCurrentURL();

	var first = Object.keys(content)[0];

	var first_content = content

	var first_data = url["base"] + 'content/' + program + '/@Media/' + first_content["Object"]
	var first_description = BBCodeParser.process(first_content["Description"])

	$('#render_content .view').append('<div class="render_identifier" id="render_info" data-description="' + first_description + '" data-title="' + first_content["Title"] + '" ><div class="button"></div><div class="title">' + first_content["Title"] + '</div></div>');

	if (!Detector.webgl) {
		Detector.addGetWebGLMessage();
	}

	var container;

	var c_height = $("#render_view").height();
	var c_width = $("#render_view").width();

	console.log(c_width)

	var camera, controls, material, scene, renderer;
	var lighting, ambient, keyLight, fillLight, backLight, spotLight;

	var mouseX = 0,
		mouseY = 0;

	var windowHalfX = window.innerWidth / 2;
	var windowHalfY = window.innerHeight / 2;

	init();
	animate();

	function init() {


		container = document.getElementById("render_view")
		
		
		camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

		camera.position.z = first_content.Configuration["Scale"];

		/* Renderer */

		renderer = new THREE.WebGLRenderer();
		renderer.setSize(c_width, c_height);

		var background_color = first_content.Configuration["Background Color"]

		renderer.setClearColor(new THREE.Color(background_color));

		/* Controls */
		ambient = new THREE.AmbientLight(0xffffff, 0.75);

		keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 0.5);
		keyLight.position.set(-100, 0, 100);

		fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.35);
		fillLight.position.set(100, 0, 100);

		backLight = new THREE.DirectionalLight(0xffffff, 1.0);
		backLight.position.set(100, 0, -100).normalize();

		spotLight = new THREE.SpotLight(0xffffff);
		spotLight.position.set(200, 400, 300);

		var object_color = first_content.Configuration["Object Color"]

		//* Materials *//

		switch (first_content.Configuration["Material"]) {
			case "wireframe":
				material = new THREE.MeshBasicMaterial({
					color: object_color,
					wireframe: true
				});
				break;

			case "flat":
				material = new THREE.MeshBasicMaterial({
					color: object_color
				});
				break;

			case "smooth":
				material = new THREE.MeshLambertMaterial({
					color: object_color,
					reflectivity: 0.5
				});
				break;

			case "shiny":
				material = new THREE.MeshPhongMaterial({
					color: object_color,
					reflectivity: 1,
					shininess: 75
				});
				break;

			case "matte":
				material = new THREE.MeshStandardMaterial({
					color: object_color,
					metalness: 0,
					roughness: 1
				});
				break;

			case "metallic":
				material = new THREE.MeshStandardMaterial({
					color: object_color,
					metalness: 0.8,
					roughness: 0.5
				});
				break;

			case "cartoon":
				var colors = new Uint8Array(4);

				for (var c = 0; c <= colors.length; c++) {

					colors[c] = (c / colors.length) * 256;

				}

				var gradientMap = new THREE.DataTexture(colors, colors.length, 1, THREE.LuminanceFormat);
				gradientMap.minFilter = THREE.NearestFilter;
				gradientMap.magFilter = THREE.NearestFilter;
				gradientMap.generateMipmaps = false;

				var material = new THREE.MeshToonMaterial({
					color: object_color,
					gradientMap: gradientMap
				});
				break;

			case "":
			default:
				material = new THREE.MeshLambertMaterial({
					color: object_color
				});
				break;
		}


		scene = new THREE.Scene();
		lighting = true;

		if (first_content.Configuration.Lighting["Ambient"] == true) {
			scene.add(ambient);
		}

		if (first_content.Configuration.Lighting["Key"] == true) {
			scene.add(keyLight);
		}

		if (first_content.Configuration.Lighting["Fill"] == true) {
			scene.add(fillLight);
		}

		if (first_content.Configuration.Lighting["Back"] == true) {
			scene.add(backLight);
		}

		if (first_content.Configuration.Lighting["Spot"] == true) {
			scene.add(spotLight);
		}



		/* Model */

		var objLoader = new THREE.OBJLoader();

		objLoader.load(first_data, function(object) {

			object.traverse(function(child) {
				if (child instanceof THREE.Mesh) {
					child.material = material;
				}
			});
			object.rotateX(first_content.Configuration.Rotation["X"]);
			object.rotateY(first_content.Configuration.Rotation["Y"]);
			object.rotateZ(first_content.Configuration.Rotation["Z"]);
			scene.add(object);

		});




		container.appendChild(renderer.domElement);

		if (first_content.Configuration["Restrict Camera"] == true) {
			document.addEventListener('mousemove', onDocumentMouseMove, false);
			console.log('hi')
		} else {
			controls = new THREE.OrbitControls(camera, renderer.domElement);
			controls.enableDamping = true;
			controls.dampingFactor = 0.25;
			controls.enableZoom = first_content.Configuration.Controls["EnableZoom"];
			controls.autoRotate = first_content.Configuration["AutoRotate"];
			controls.autoRotateSpeed = 1;
			controls.enablePan = first_content.Configuration.Controls["EnablePan"];
			controls.enableRotate = first_content.Configuration.Controls["EnableRotate"];
		}

	}

	function onDocumentMouseMove(event) {

		mouseX = (event.clientX - windowHalfX) / 3;
		mouseY = (event.clientY - windowHalfY) / 3;

	}

	function animate() {

		requestAnimationFrame(animate);

		if (first_content.Configuration["Restrict Camera"] != true) {
			controls.update();
		}


		render(renderer, scene, camera);

	}

	function render() {
		if (first_content.Configuration["Restrict Camera"] == true) {
			camera.position.x += (mouseX - camera.position.x) * .05;
			camera.position.y += (-mouseY - camera.position.y) * .05;

			camera.lookAt(scene.position);
		}

		renderer.render(scene, camera);

	}

}


