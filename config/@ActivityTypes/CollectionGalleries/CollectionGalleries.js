// CollectionGalleries.js
// Used for the Collection Galleries activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	$(document).on('click', "a.relatedobject_gallery", function() {

		var gallery = $(this).data("gallery");

		var images = []

		$('.relatedobject_gallery[data-gallery="' + gallery + '"]').each(function(index, val) {

			var title = $(this).data("title");
			var description = BBCodeParser.process($(this).data("caption"));
			images.push({
				src: $(this).data('image'),
				caption: "<b>" + title + "</b><br />" + description
			})
		})

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cg_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": images
	      }
	    };
	    createLookCloserPopUp(options);

	});

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadCollectionGalleries(program, content) {

	$.each(content, function(index, val) {
		var object_index = index;
		//

		var images = val["Images"];
		var first_image = images[Object.keys(images)[0]]

		var primary_image = "./content/" + program + "/@Media/" + first_image["Image"];
		var primary_content = first_image["Caption"]
		var title = val["Title"]
		var final_images = [];

		$.each(images, function(index, val) {
			if (index > 1) {
				var image_url = "./content/" + program + "/@Media/" + val["Image"];
				var content = BBCodeParser.process(val["Caption"])
				console.log(val["Title"])
				final_images.push('<a class="relatedobject_gallery" data-title="' + title + '" data-image="' + image_url + '" data-gallery="gallery' + object_index + '" data-caption="' + content + '" ></a>')
			}

		})




		$('#objects').append('<a data-image="' + primary_image + '" data-gallery="gallery' + object_index + '" class="relatedobject_gallery"  data-caption="' + primary_content + '" data-title="' + val["Title"] + '"><div class="object relatedobject_button" data-gallery="gallery' + object_index + '" data-title="' + val["Title"] + '" ><div class="title">' + val["Title"] + '</div><div class="relatedobject_wrapper"><div class="image" style="background-image: url(' + primary_image + ')"></div> <div class="details"><div class="content_wrapper"><div class="location">' + val["Subtitle"] + '</div><div class="description">' + val["Description"] + '</div></div></div> <div class="view_more"></div> </div></div></a><div style="display: none;">' + final_images.join("") + '</div>')

	})

	initializeContainerScrollBar('#related_objects',true,false,true)
}


