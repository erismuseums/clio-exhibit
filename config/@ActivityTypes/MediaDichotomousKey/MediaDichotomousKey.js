// MediaDichotomousKey.js
// Used for the Media Dichotomous Key activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {
	
	$(document).on("click","#dichotomous_key .questions .options .option",function(){
		
		if($(this).hasClass("selected")){
			
			$(this).removeClass("selected");
			$(this).addClass("unselected");
			$("#dk_next").addClass("disabled")

		} else {
			
			$("#dichotomous_key .questions .options .option").each(function(){
				$(this).removeClass("selected")
				$(this).addClass("unselected")
			})
			$(this).removeClass("unselected")
			$(this).addClass("selected")
			
			$("#dk_next").removeClass("disabled")

		}
		
	})

	$(document).on("click","#dk_next",function(){

		var selected_type = $("#dichotomous_key .questions .options .selected").attr("data-goto-type");
		var selected_location = $("#dichotomous_key .questions .options .selected").attr("data-goto-location");

		if(selected_type == "Question"){

			var current_qid = $("#dk_option1").attr("data-qid");
			var new_qid = selected_location;
			var new_prompt = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-prompt");

			var new_option1_text = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option1-text");
			var new_option1_goto_type = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option1-goto-type");
			var new_option1_goto_location = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option1-goto-location");

			var new_option2_text = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option2-text");
			var new_option2_goto_type = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option2-goto-type");
			var new_option2_goto_location = $("#activity_questions").find("[data-qid='" + new_qid + "']").attr("data-option2-goto-location");

			$("#dichotomous_key .questions .prompt").html(new_prompt);
			$("#dk_option1 .text").html(new_option1_text);
			$("#dk_option1").attr("data-qid",new_qid);
			$("#dk_option1").attr("data-goto-type",new_option1_goto_type);
			$("#dk_option1").attr("data-goto-location",new_option1_goto_location);
			$("#dk_option2 .text").html(new_option2_text);
			$("#dk_option2").attr("data-qid",new_qid);
			$("#dk_option2").attr("data-goto-type",new_option2_goto_type);
			$("#dk_option2").attr("data-goto-location",new_option2_goto_location);
			
			$("#dichotomous_key .questions .options .option").each(function(){
				$(this).removeClass("selected")
				$(this).addClass("unselected")
			})

			$("#dk_next").addClass("disabled")
			

		} else

		if(selected_type == "Answer"){

			$("#dichotomous_key .overlay_wrapper").show();

			console.log(selected_location)
			var ans = selected_location;

			var answer_title = $("#activity_answers").find("[data-ans='" + ans + "']").attr("data-title");
			var answer_description = $("#activity_answers").find("[data-ans='" + ans + "']").attr("data-description");
			var answer_image = $("#activity_answers").find("[data-ans='" + ans + "']").attr("data-image");;
			console.log(answer_title)
			$(".check_match").attr("data-user-title",answer_title);
			$(".check_match").attr("data-user-description",answer_description);
			$(".check_match").attr("data-user-image",answer_image);
		}


	})

	$(document).on("click",".check_match",function(){

		$("#dichotomous_key .overlay_wrapper .confirm_selection").hide()
		var correct_answer = $("#dichotomous_key").attr("data-correct-answer");

		var answer_title = $("#activity_answers").find("[data-ans='" + correct_answer + "']").attr("data-title");
		var answer_description = $("#activity_answers").find("[data-ans='" + correct_answer + "']").attr("data-description");
		var answer_image = $("#activity_answers").find("[data-ans='" + correct_answer + "']").attr("data-image");

		var user_title = $(this).attr("data-user-title");
		var user_description = $(this).attr("data-user-description");
		var user_image = $(this).attr("data-user-image");

		console.log(user_description)
		
		
		$("#dichotomous_key .overlay_wrapper .answer .correct_answer .title").html(answer_title);
		$("#dichotomous_key .overlay_wrapper .answer .correct_answer .image").css("background-image", "url(" + answer_image + ")");	

		if(answer_description == undefined || answer_description == ""){
			$("#dichotomous_key .overlay_wrapper .answer .correct_answer .image .more_info").hide()
		} else {
			$("#dichotomous_key .overlay_wrapper .answer .correct_answer .image .more_info").show()
			$("#dichotomous_key .overlay_wrapper .answer .correct_answer .image .more_info").attr("data-title",answer_title)
			$("#dichotomous_key .overlay_wrapper .answer .correct_answer .image .more_info").attr("data-description",answer_description)
			
		}

		$("#dichotomous_key .overlay_wrapper .answer .user_answer .title").html(user_title);
		$("#dichotomous_key .overlay_wrapper .answer .user_answer .image").css("background-image", "url(" + user_image + ")");	

		if(user_description == undefined || user_description == ""){
			$("#dichotomous_key .overlay_wrapper .answer .user_answer .image .more_info").hide()
		} else {
			$("#dichotomous_key .overlay_wrapper .answer .user_answer .image .more_info").show()
			$("#dichotomous_key .overlay_wrapper .answer .user_answer .image .more_info").attr("data-title",user_title)
			$("#dichotomous_key .overlay_wrapper .answer .user_answer .image .more_info").attr("data-description",user_description)
			
		}

		if(answer_title == user_title){
			$("#action_button").addClass("correct")
			$("#action_button span").html("Correct!")
		} else {
			$("#action_button").addClass("incorrect")
			$("#action_button span").html("Try Again")
		}



		$("#dichotomous_key .overlay_wrapper .answer").show()

	})

	$(document).on("click", "#dichotomous_key .overlay_wrapper .answer .image .more_info", function() {
		
		var title = $(this).attr("data-title");
		var description = $(this).attr("data-description");

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cm_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

	    return false;

	});

	$(document).on("click", "#action_button.incorrect", function() {
		location.reload();
	})

	$(document).on("click", "#dichotomous_key .media .media_wrapper .media_button", function() {
		
		var clicked_image = $(this).index();
		var images = []

		$('.media_button[data-gallery="Media"]').each(function(index, val) {

			var description = BBCodeParser.process($(this).data("caption"));
			images.push({
				src: $(this).data('link'),
				caption: description
			})
		})

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cg_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": images,
	        "LoadIndex":clicked_image
	      }
	      
	    };
	    createLookCloserPopUp(options);

	})


})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadMediaDichotomousKey(program, content, container) {

	
	var media_type = content.Media["Type"];
	var media_data = content.Media["Data"];

	switch(media_type){
		case "Gallery":
			
			$.each(media_data,function(index,val){
				var url = 'content/' + program + '/@Media/' + val["src"];
				$("#dichotomous_key .media .media_wrapper").append('<div data-gallery="Media" data-caption="' + val["caption"] + '" data-link="' + url + '" class="media_button"><div class="image" style="background-image: url(' + url + ')"><div class="more_info"></div></div></div>')
			})

			break;

	}

	initializeContainerScrollBar("#dichotomous_key .media .media_wrapper",false,false,true)
	

	var questions = content["Questions"];
	var answers = content["Answers"];
	var q1_key = Object.keys(questions)[0];
	var q1_content = questions[q1_key];

	$("#dichotomous_key").attr("data-correct-answer",content.Configuration["CorrectAnswer"])
	$("#dichotomous_key .questions .prompt").html(q1_content["Prompt"]);

	var q1_option1 = q1_content.Options["1"]
	$("#dk_option1 .text").html(q1_option1["Text"])
	$("#dk_option1").attr("data-qid",q1_key);
	$("#dk_option1").attr("data-goto-type",q1_option1.Goto["Type"])
	$("#dk_option1").attr("data-goto-location",q1_option1.Goto["Location"])


	var q1_option2 = q1_content.Options["2"]
	$("#dk_option2 .text").html(q1_option2["Text"])
	$("#dk_option2").attr("data-qid",q1_key);
	$("#dk_option2").attr("data-goto-type",q1_option2.Goto["Type"])
	$("#dk_option2").attr("data-goto-location",q1_option2.Goto["Location"])

	$.each(questions,function(index,val){

		var option1 = val.Options["1"]
		var option2 = val.Options["2"]
		$("#activity_questions").append('<a data-qid="' + index + '" data-prompt="' + val["Prompt"] + '" data-option1-text="' + option1["Text"] + '" data-option1-goto-type="' + option1.Goto["Type"] + '" data-option1-goto-location="' + option1.Goto["Location"] + '" data-option2-text="' + option2["Text"] + '" data-option2-goto-type="' + option2.Goto["Type"] + '" data-option2-goto-location="' + option2.Goto["Location"] + '"></a>')
	})

	$.each(answers,function(index,val){
		var image = "content/" + program + "/@Media/" + val["Image"];
		$("#activity_answers").append('<a data-ans="' + index + '" data-title="' + val["Title"] + '" data-description="' + val["Description"] + '" data-image="' + image + '"></a>')
	})

	$("#dichotomous_key .overlay_wrapper .confirm_selection .top_text").html(content.Configuration.CheckAnswer["Top"]);
	$("#dichotomous_key .overlay_wrapper .confirm_selection .bottom_text").html(content.Configuration.CheckAnswer["Bottom"]);

	$(".user_answer .header").html(content.Configuration.AnswerTitles["User"]);
	$(".correct_answer .header").html(content.Configuration.AnswerTitles["Correct"]);
};