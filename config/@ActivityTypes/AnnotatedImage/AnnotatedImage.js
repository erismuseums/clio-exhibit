// AnnotatedImage.js
// Used for the Annotated Image and Comparison activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	var resizeId;
	$(window).resize(function() {
	    
	});

	$(window).on( "resize", function() {
	  if($("#annotated_image").hasClass("scaled")){
	  	clearTimeout(resizeId);
	    resizeId = setTimeout(doResize, 100);

	  }
	});

	//Informational buttons at the bottom of the activity.  Opens a pop-up with more information.
	$(document).on("click", ".twentytwenty_identifier", function() {

		var title = $(this).data("title")
		var description = $(this).data("description")

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"aiciden_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

		
	})

	//Toolbar - Toggle tooltip visibility.
	$(document).on("click", "#tooltip_toggle", function() {

		if ($(this).hasClass('hide')) {
			$(this).removeClass('hide')
			$(this).addClass('visible')

			$('.tooltip').each(function() {
				$(this).removeClass('secret')
				$(this).addClass('hint')
			})

		} else if ($(this).hasClass('visible')) {
			$(this).removeClass('visible')
			$(this).addClass('hide')

			$('.tooltip').each(function() {
				$(this).removeClass('hint')
				$(this).addClass('secret')
			})

		}

	})

	//Open each tooltip into an informational pop-up.
	$(document).on("click", ".tooltip", function() {

		var title = $(this).data("title")
		var type = $(this).data("type")
		var content = $(this).data("content")
		var options = {};
		
		switch(type){
			case "Rich Text":
				
				//Create Popup 
			    options = {
			      "Header":{
			        "Style":"default",
			        "Text":title
			      },
			      "Scrollbar":"auto",
			      "Interaction":{
			        "Type":"acknowledge",
			        "Prefix":"aiciden_"
			      },
			      "Content":{
			        "Type":"html",
			        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + content + '</div>'
			      }
			    };
				break;

			case "Gallery":
				
				//Create Popup 
			    options = {
			      "Header":{
			        "Style":"default",
			        "Text":title
			      },
			      "Scrollbar":false,
			      "Interaction":{
			        "Type":"acknowledge",
			        "Prefix":"aiciden_"
			      },
			      "Content":{
			        "Type":"gallery",
			        "Data":content
			      }
			    };
				break;

			
		}

			
	    createLookCloserPopUp(options);

	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadAnnotatedImage(program, content) {
	
	var program_details = loadProgramDetails(program)

	//Autohide the subheader

	var start_position = content.Configuration["Start Position"]
	var object1_image = content.Configuration.Image["Source"];
	var object1_description = content.Configuration.Image["Description"];
	var object1_header = content.Configuration.Image["Header"];

	var invisible = "";
	var fixed = "";
	if (content.References.Configuration["Fixed"] == true) {
		fixed = "fixed"
	}

	if (content.References.Configuration["Invisible"] == true) {
		invisible = "secret";
	}

	//Add the 20/20 container

	if (object1_image) {
		$('#annotated_image .image').append('<img src="./content/' + program + '/@Media/' + object1_image + '">')
	}


	console.log(content)
		$("#annotated_image .content").attr('style', 'height: ' + content.Configuration.Image.Dimensions["Height"] + 'px !important; width: ' + content.Configuration.Image.Dimensions["Width"] + 'px !important');
		$("#annotated_image .content .image").attr('style', 'height: ' + content.Configuration.Image.Dimensions["Height"] + 'px !important; width: ' + content.Configuration.Image.Dimensions["Width"] + 'px !important');
		$("#annotated_image .content .annotation").attr('style', 'height: ' + content.Configuration.Image.Dimensions["Height"] + 'px !important; width: ' + content.Configuration.Image.Dimensions["Width"] + 'px !important');

	if (content.References.Configuration["Toggleable"] == true) {
		invisible = "hint";
		$("#annotated_image .annotation").append('<div id="tooltip_toggle" class="visible"></div>');
	}

	if (object1_header != undefined && object1_description) {
		var parsed1 = BBCodeParser.process(object1_description);
		$('#annotated_image .annotation').append('<div class="twentytwenty_identifier" id="twentytwenty_image1" data-description="' + parsed1 + '" data-title="' + object1_header + '" ><div class="button"></div><div class="title">' + object1_header + '</div></div>');
	}

	

	var tooltips = content.References["Tooltips"]

	$.each(tooltips, function(index, val) {
		var x = val.Geometry.Position["x"];
		var y = +val.Geometry.Position["y"];
		var radius = +val.Geometry["Radius"];
		var diameter = +radius * 2;

		var title = val["Title"];
		var icon_image = val.Icon["Image"];
		var icon_scale = val.Icon["Scale"];
		
		var content = "";
		var content_type = val.Content["Type"];
		var content_data = val.Content["Data"];
		
		switch(content_type){
			case "Rich Text":

				content = BBCodeParser.process(content_data);
				break;

			case "Gallery":
				var images = {};
				$.each(content_data,function(index,val){
					val["src"] = "content/" + program + "/@Media/" + val["src"];
				})


				content = JSON.stringify(content_data);
				
				break;

		}

		var background_image = "";
		if(icon_image != "" && icon_image != undefined){
			var background_image = 'background-image: url(content/' + program + '/@Media/' + icon_image + ') !important;'
		}

		var background_scale = "";
		if(icon_scale != "" && icon_scale != undefined){
			var background_scale = 'background-size: ' + icon_scale + ' !important;'
		}
		

		$('#annotated_image .annotation').append('<div data-tippy-content="' + title + '" class="tooltip ' + fixed + ' ' + invisible + '" style="margin-left: -' + radius + 'px; margin-top: -' + radius + 'px; width: ' + diameter + 'px; height: ' + diameter + 'px; left:' + x + 'px; top: ' + y + 'px; ' + background_image + ' ' + background_scale + '" data-title="' + title + '" data-type="' + content_type + '" data-content=\'' + content + '\'></div>')
	})

	tippy('[data-tippy-content]');

	if (content.Configuration["ScaleToFit"] == true || content.Configuration["ScaleToFit"] == "true" ) {
		var resizeId;

		clearTimeout(resizeId);
	    resizeId = setTimeout(doResize, 100);
		$("#annotated_image").addClass("scaled")
	}

	


}

function doResize(){
	
	contentHeight = $("#annotated_image .image").height();
	contentWidth = $("#annotated_image .image").width();

	console.log("Content Height:" + contentHeight)
	console.log("Content Width:" + contentWidth)

	

	availableHeight = $("#activity_content").height();
	availableWidth = $("#activity_content").width();

	console.log("Available Height:" + availableHeight)
	console.log("Available Width:" + availableWidth)

	scale = Math.min(
	    availableWidth / contentWidth,    
	    availableHeight / contentHeight
	  );

	console.log('Scale: '+ scale)


	$("#annotated_image .content").css('transform', 'scale(' + scale + ')');


}
