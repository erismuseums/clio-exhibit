// RichTextNarration.js
// Used for the Rich Text with Narration activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadRichTextNarration(program, content) {

	
	var audio = content["Audio"]

	var richtext = BBCodeParser.process(content["Richtext"])
	var audio_url = "./content/" + program + "/@Media/" + audio["1"]

	$('#richtext').html(richtext)

	if (audio["1"] != "") {
		$('#richtext_audio').addClass('audio_player')
		$('#richtext_audio').append('<div id="audio"></div>')
		$('#audio').append('<div id="jquery_jplayer_1"></div>')
		$('#audio').append('<div id="jp-control-bar" class="white"><div class="jp-controls"><button class="jp-play" role="button" tabindex="0"></button><button class="jp-rewind" role="button" tabindex="1"></button></div><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div><div class="jp-duration">-99:99</div><div class="jp-audio-controls"><div id="volume"></div><div class="volume-current"><div class="first volume-level selected" id="vol_25"></div><div class="volume-level" id="vol_50"></div><div class="volume-level" id="vol_75"></div><div class="last volume-level" id="vol_100"></div></div></div>')
		$("#jquery_jplayer_1").jPlayer({
			ready: function(event) {
				$(this).jPlayer("setMedia", {
					oga: audio_url
				})

				$("#jquery_jplayer_1").jPlayer('play')

			},
			ended: function() {

			},
			swfPath: "js/jplayer/jplayer",
			volume: 0.25,
			supplied: "oga",
			cssSelectorAncestor: "#jp-control-bar",
			useStateClassSkin: true,
			smoothPlayBar: true,
			remainingDuration: true,

			cssSelector: {
				play: '.jp-play',
				pause: '.jp-pause',
				seekBar: '.jp-seek-bar',
				playBar: '.jp-play-bar',
				duration: '.jp-duration',
				noSolution: '.jp-no-solution'
			}

		});
	}




	initializeContainerScrollBar('#richtext_audio #richtext_content',true,false,true)
}


