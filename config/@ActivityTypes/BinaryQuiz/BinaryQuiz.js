// BinaryQuiz.js
// Used for the Binary Quiz activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Binary buttons.
	$(document).on("click", 'button.affirmative, button.negative', function() {

		var clicked = $(this).attr('class')
		var current_slide = +$('#true_or_false').attr("data-slide");
		$('.owl-carousel').trigger('to.owl.carousel', current_slide)

		var question_id = $('.owl-carousel .owl-item.active').find('.question').attr("id")

		var answer = $('.owl-carousel .owl-item.active').find('.question').data("answer")
		var correct_response = $('.owl-carousel .owl-item.active').find('.question').data("correct")
		var incorrect_response = $('.owl-carousel .owl-item.active').find('.question').data("incorrect")

		var response = ""
		var interaction = ""

		switch (clicked == answer) {
			case true:
				response = correct_response
				interaction = "<button class='next_tf'>Next</button>";
				break;

			case false:
				response = incorrect_response
				interaction = "<button class='retry_tf'>Try Again</button><button class='next_tf'>Skip</button>";
				break;
		}

		var next_slide = +$('#true_or_false').attr("data-slide") + 1;
		var total_slide = $('#true_or_false').attr("data-total");

		if (next_slide == total_slide && clicked == answer) {
			interaction = "<button class='done_tf'  onclick='$.fancybox.destroy();'>Done</button>"
		} else if (next_slide == total_slide && clicked != answer) {
			interaction = "<button class='retry_tf'>Try Again</button>"
		}

		$.fancybox.open({
			src: '<div id="tf_message"><div id="' + question_id + '" class="response"><div class="text"><span>' + response + '</span></div><div class="interaction">' + interaction + '</div></div></div>',
			type: 'html',
			opts: {
				idleTime: false,
				animationDuration: 0,
				toolbar: true,
				clickSlide: false,
				touch: false,
				modal: true,
				buttons: []
			}
		});

	})

	//Next Question
	$(document).on("click", 'button.next_tf', function() {

		var next_slide = +$('#true_or_false').attr("data-slide") + 1;
		$('#true_or_false').attr("data-slide", next_slide)

		$('.owl-carousel').trigger('next.owl.carousel')
		$.fancybox.destroy();
	});

	//Retry Question.
	$(document).on("click", 'button.retry_tf', function() {

		var current_slide = $('#true_or_false').attr("data-slide");

		$('.owl-carousel').trigger('to.owl.carousel', current_slide)
		$.fancybox.destroy();
	});

	//Restart Quiz
	$(document).on("click", 'button.restart_tf', function() {
		location.reload();
	});

	//Open the question image
	$(document).on("click", ".question_image", function() {

		var link = $(this).data("link")

		$.fancybox.open({
			src: link,
			type: 'image',
			opts: {
				idleTime: false,
				animationDuration: 0,
				toolbar: true
			}
		});
	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadBinaryQuiz(program, content) {
	

	var total = Object.keys(content).length;
	$("#binary_quiz").append('<div class="owl-carousel owl-theme" id="true_or_false" data-slide="0" data-total="' + total + '"></div>');


	$.each(content, function(index, val) {

		$('#true_or_false').append('<div class="question" id="question_' + index + '" data-answer="' + val["Answer"] + '" data-correct="' + val.Response["Correct"] + '" data-incorrect="' + val.Response["Incorrect"] + '"></div>');

		var image_url = "content/" + program + "/@Media/" + val["Image"];

		$('#question_' + index).append('<div class="image_wrapper"><div class="question_image" data-link="' + image_url + '" style="background-image: url(' + image_url + ')"><div class="tf_expand" ></div></div></div>');
		$('#question_' + index).append('<div class="text_wrapper"></div>');
		$('#question_' + index + ' .text_wrapper').append('<div class="text">' + val.Question["Text"] + '</div>');

		var affirmative = "";
		var negative = ""

		switch (val.Question["Type"]) {
			case "True_False":
				affirmative = "True";
				negative = "False";
				break;

			case "Yes_No":
				affirmative = "Yes";
				negative = "No";
				break;

			case "Affirmative_Negative":
				affirmative = "Affirmative";
				negative = "Negative";
				break;

			case "none":
				affirmative = "";
				negative = "";
				break;
		}

		$('#question_' + index + ' .text_wrapper').append('<div class="response"><div class="true"><button class="affirmative">' + affirmative + '</button></div><div class="false"><button class="negative">' + negative + '</button></div></div>');

	})

	$('.owl-carousel').owlCarousel({
		loop: false,
		margin: 0,
		dots: false,
		nav: false,
		items: 1,
		touchDrag: false,
		pullDrag: false,
		slideTransition: 'none'
	})
}


