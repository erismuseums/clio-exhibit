// CardMatch.js
// Used for the Card Match activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {
	
	$(document).on("click", "#cmcorrect_close", function() {
		
		$("#cards .right").removeClass("selected");
		$("#cards .left").removeClass("selected");
		$("#cards").removeClass("check")

		$("#cards .right .popup .more_info").attr("data-title","")
		$("#cards .right .popup .more_info").attr("data-description","")
		$("#cards .right .popup .more_info").attr("data-name","");
		$("#cards .right .popup .more_info").attr("data-answer","");
		$("#cards .right .popup .more_info").attr("data-correct","");
		$("#cards .right .popup .more_info").attr("data-incorrect","");
		$("#cards .right .popup .image").css("background-image", "unset");	

		$("#cards .left .popup .more_info").attr("data-title","")
		$("#cards .left .popup .more_info").attr("data-description","")
		$("#cards .left .popup .more_info").attr("data-name","");
		$("#cards .left .popup .more_info").attr("data-answer","");
		$("#cards .left .popup .more_info").attr("data-correct","");
		$("#cards .left .popup .more_info").attr("data-incorrect","");
		$("#cards .left .popup .image").css("background-image", "unset");	
		destroyLookCloserPopUp();

		return false;
	});

	$(document).on("click", "#cmincorrect_close", function() {
		
		$("#cards .right").removeClass("selected");
		$("#cards .left").removeClass("selected");
		$("#cards").removeClass("check")

		$("#cards .right .popup .more_info").attr("data-title","")
		$("#cards .right .popup .more_info").attr("data-description","")
		$("#cards .right .popup .more_info").attr("data-name","");
		$("#cards .right .popup .more_info").attr("data-answer","");
		$("#cards .right .popup .more_info").attr("data-correct","");
		$("#cards .right .popup .more_info").attr("data-incorrect","");
		$("#cards .right .popup .image").css("background-image", "unset");	

		$("#cards .left .popup .more_info").attr("data-title","")
		$("#cards .left .popup .more_info").attr("data-description","")
		$("#cards .left .popup .more_info").attr("data-name","");
		$("#cards .left .popup .more_info").attr("data-answer","");
		$("#cards .left .popup .more_info").attr("data-correct","");
		$("#cards .left .popup .more_info").attr("data-incorrect","");
		$("#cards .left .popup .image").css("background-image", "unset");	
		destroyLookCloserPopUp();

		return false;
	});

	$(document).on("click", "#cards .check_match", function() {
		
		var left_name = $("#cards .left .popup .more_info").attr("data-name");
		var right_name = $("#cards .right .popup .more_info").attr("data-name");


		var answer = $("#cards .left .popup .more_info").attr("data-answer");

		var correct = $("#cards .left .popup .more_info").attr("data-correct");
		var incorrect = $("#cards .left .popup .more_info").attr("data-incorrect");
		
		if(right_name == answer){
			
			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"success",
		        "Text":"Correct!"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		          "Type":"custom",
		          "HTML":'<button class="lookcloser_next" id="cmcorrect_close">Next</button>',
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + correct + '</div>'
		      }
		    };

		    $('#left_cards .card.' + left_name).removeClass("active")
		    $('#left_cards .card.' + left_name).addClass("disabled")
		    $('#right_cards .card.' + right_name).removeClass("active")
		    $('#right_cards .card.' + right_name).addClass("disabled")


	    
		} else {
			
			var options = {
		      "Header":{
		        "Style":"error",
		        "Text":"Try again."
		      },
		      "Scrollbar":false,
		      "Interaction":{
		          "Type":"custom",
		          "HTML":'<button class="lookcloser_tryagain" id="cmincorrect_close">Retry</button>',
		          "Close":false,
		          "Swipe":false
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + incorrect + '</div>'
		      }
		    };

		}

		createLookCloserPopUp(options);
		$("#cards .right").addClass("disabled")
		return false;
	});

	$(document).on("click", "#left_cards .card.active", function() {
		
		if(!$(this).hasClass("disabled")){
			if( $("#cards .left").hasClass("selected") ){
				$("#cards .left").removeClass("selected")
			} else {
				$("#cards .left").addClass("selected")
			}

			var title = $(this).data("title");
			var description = $(this).data("description");
			var image = $(this).data("image");
			
			var name = $(this).data("name");
			var answer = $(this).data("answer");
			var correct = $(this).data("correct");
			var incorrect = $(this).data("incorrect");


			$("#cards .left .popup .title").html(title)
			if(image == "" || image == undefined){
				$("#cards .left .popup .title").addClass("full_title")
			} else {
				$("#cards .left .popup .title").removeClass("full_title")
			}
			$("#cards .left .popup .image").css("background-image", "url(" + image + ")");

			$("#cards .left .popup .more_info").attr("data-name",name)
			$("#cards .left .popup .more_info").attr("data-answer",answer)
			$("#cards .left .popup .more_info").attr("data-correct",correct)
			$("#cards .left .popup .more_info").attr("data-incorrect",incorrect)

			if(description == undefined || description == ""){
				$("#cards .left .popup .more_info").hide()
			} else {
				$("#cards .left .popup .more_info").show()
				$("#cards .left .popup .more_info").attr("data-title",title)
				$("#cards .left .popup .more_info").attr("data-description",description)
				
			}

			if($("#cards .right").hasClass("selected")){
				$("#cards").addClass("check")
			} else {
				$("#cards").removeClass("check")
			}
		}

		$("#cards .right").removeClass("disabled")
			
			
	});

	$(document).on("click", "#right_cards .card.active", function() {
		
		if(!$(this).hasClass("disabled")){
			if( $("#cards .right").hasClass("selected") ){
				$("#cards .right").removeClass("selected")
			} else {
				$("#cards .right").addClass("selected")
			}
			
			var title = $(this).data("title");
			var description = $(this).data("description");
			var image = $(this).data("image");
			
			var name = $(this).data("name");
			var answer = $(this).data("answer");
			var correct = $(this).data("correct");
			var incorrect = $(this).data("incorrect");

			$("#cards .right .popup .title").html(title)
			if(image == "" || image == undefined){
				$("#cards .right .popup .title").addClass("full_title")
			} else {
				$("#cards .right .popup .title").removeClass("full_title")
			}
			
			$("#cards .right .popup .image").css("background-image", "url(" + image + ")");	

			$("#cards .right .popup .more_info").attr("data-name",name)
			$("#cards .right .popup .more_info").attr("data-answer",answer)
			$("#cards .right .popup .more_info").attr("data-correct",correct)
			$("#cards .right .popup .more_info").attr("data-incorrect",incorrect)
				
			if(description == undefined || description == ""){
				$("#cards .right .popup .more_info").hide()
			} else {
				$("#cards .right .popup .more_info").show()
				$("#cards .right .popup .more_info").attr("data-title",title)
				$("#cards .right .popup .more_info").attr("data-description",description)
				
			}

			if($("#cards .left").hasClass("selected")){
				$("#cards").addClass("check")
			} else {
				$("#cards").removeClass("check")
			}


		}
			

	});

	$(document).on("click", "#cards .right .popup .close", function() {
		
		$("#cards .right").removeClass("selected");
		$("#cards").removeClass("check")

		$("#cards .right .popup .more_info").attr("data-title","")
		$("#cards .right .popup .more_info").attr("data-description","")

		$("#cards .right .popup .more_info").attr("data-name","");
		$("#cards .right .popup .more_info").attr("data-answer","");
		$("#cards .right .popup .more_info").attr("data-correct","");
		$("#cards .right .popup .more_info").attr("data-incorrect","");

		$("#cards .right .popup .image").css("background-image", "unset");	

	});

	$(document).on("click", "#cards .left .popup .close", function() {
		
		$("#cards .left").removeClass("selected");
		$("#cards").removeClass("check")

		$("#cards .left .popup .more_info").attr("data-title","")
		$("#cards .left .popup .more_info").attr("data-description","")

		$("#cards .left .popup .more_info").attr("data-name","");
		$("#cards .left .popup .more_info").attr("data-answer","");
		$("#cards .left .popup .more_info").attr("data-correct","");
		$("#cards .left .popup .more_info").attr("data-incorrect","");

		$("#cards .left .popup .image").css("background-image", "unset");	

		


		$("#cards .right").addClass("disabled");
		$("#cards .right").removeClass("selected");
		$("#cards").removeClass("check")

		$("#cards .right .popup .more_info").attr("data-title","")
		$("#cards .right .popup .more_info").attr("data-description","")

		$("#cards .right .popup .more_info").attr("data-name","");
		$("#cards .right .popup .more_info").attr("data-answer","");
		$("#cards .right .popup .more_info").attr("data-correct","");
		$("#cards .right .popup .more_info").attr("data-incorrect","");

		$("#cards .right .popup .image").css("background-image", "unset");	

	});

	$(document).on("click", "#cards .popup .more_info", function() {
		
		var title = $(this).attr("data-title");
		var description = $(this).attr("data-description");

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":title
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cm_"
	      },
	      "Content":{
	        "Type":"html",
	        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
	      }
	    };
	    createLookCloserPopUp(options);

	    return false;

	});

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadCardMatch(program, content, container) {

	var showtitle_left = content.Configuration.ShowTitle["Left"];
	var showtitle_right = content.Configuration.ShowTitle["Right"];

	var prompt_one = content.Configuration.Prompt["First"];
	var prompt_two = content.Configuration.Prompt["Second"];
	
	var left_title = content.Title["Left"];
	
	if(left_title == "" || left_title == undefined){
		$("#cards .left").addClass("hide_title")
	} else {
		$("#cards .left .title").html(left_title)
	}
	
	$.each(content.Cards.Left, function(index, val) {

		var background = val["Background"];
		if(background == "" || background == undefined){
			var image_url = "";
		} else {
			var image_url = "./content/" + program + "/@Media/" + val["Background"]
		}
		
		var title = val["Title"];
		var content = val["Content"];

		var hide_info = ""
		if ( content == "" || content == undefined ){
			hide_info = "hide_info"
		}

		var hide_title = ""
		if ( title == "" || title == undefined || showtitle_left == false){
			hide_title = "hide_title"
		}

		$('#left_cards').append('<div data-name="' + index + '" data-image="' + image_url + '" data-title="' + title + '" data-description="' + val["Content"] + '"  data-side="left" class="active card ' + index + ' ' + hide_info + ' ' + hide_title + '"><div data-tippy-content="' + title + '" class="back" style="background-image: url(' + image_url + ')"><div class="title">' + title + '</div></div></div>')

	})

	var right_title = content.Title["Right"];
	if(right_title == "" || right_title == undefined){
		$("#cards .right").addClass("hide_title")
	} else {
		$("#cards .right .title").html(right_title)
	}

	$.each(content.Cards.Right, function(index, val) {
		var background = val["Background"];
		if(background == "" || background == undefined){
			var image_url = "";
		} else {
			var image_url = "./content/" + program + "/@Media/" + val["Background"]
		}
		var title = val["Title"];
		var content = val["Content"];
		
		var hide_info = ""
		if ( content == "" || content == undefined ){
			hide_info = "hide_info"
		}

		var hide_title = ""
		if ( title == "" || title == undefined || showtitle_right == false){
			hide_title = "hide_title"
		}

		$('#right_cards').append('<div data-name="' + index + '" data-image="' + image_url + '" data-title="' + title + '" data-description="' + val["Content"] + '"  data-side="right" class="active card ' + index + ' ' + hide_info + ' ' + hide_title + '"><div data-tippy-content="' + title + '" class="back" style="background-image: url(' + image_url + ')"><div class="title">' + title + '</div></div></div>')
	})

	$('#right_cards').shuffleChildren();
	$('#left_cards').shuffleChildren();

	initializeContainerScrollBar("#left_cards",false,false,true);
	initializeContainerScrollBar("#right_cards",false,false,true)

	$("#cards .right .popup .prompt").html(prompt_one);
	$("#cards .left .popup .prompt").html(prompt_two);

	$.each(content.Answer, function(index, val) {

		var correct = BBCodeParser.process(val["Correct"])

		var incorrect = BBCodeParser.process(val["Incorrect"])

		$('[data-name=' + val.Pair[0] + ']').attr("data-answer", val.Pair[1])
		$('[data-name=' + val.Pair[0] + ']').attr("data-correct", correct)
		$('[data-name=' + val.Pair[0] + ']').attr("data-incorrect", incorrect)

		$('[data-name=' + val.Pair[1] + ']').attr("data-answer", val.Pair[0])
		$('[data-name=' + val.Pair[1] + ']').attr("data-correct", correct)
		$('[data-name=' + val.Pair[1] + ']').attr("data-incorrect", incorrect)
	})

	tippy('[data-tippy-content]', {
	  trigger: 'mouseenter focus'
	});


};


