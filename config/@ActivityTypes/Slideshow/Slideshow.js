// Slideshow.js
// Used for the Slideshow activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Toolbar - Toggle overview button
	$(document).on("click", '#toggle_overview', function() {
		Reveal.toggleOverview();
	});

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadSlideshow(program, content, container) {
	

	var loop_slideshow = content.Configuration["Loop"];

	$.each(content.Slides, function(index, val) {

		var url = getCurrentURL();
		var program = getSetting("setting__default_program", "Default");

		var parent_slide = index;
		var parent_content = BBCodeParser.process(val.Content["Slide"]);

		var parent_background_color = val.Configuration["Background Color"];
		var parent_background_image = url['base'] + "/content/" + program + "/@Media/" + val.Configuration["Background Image"];
		var parent_font_color = ""
		var parent_title = val.Content["Title"];

		var parent_title_y = val.Configuration.Title["Y"]
		var parent_title_x = val.Configuration.Title["X"]
		var parent_title_size = val.Configuration.Title["Size"] + "em"

		if (val.Configuration["Font Color"] != undefined) {

			parent_font_color = val.Configuration["Font Color"];

		}
		
		if (val.Content["Sub Slides"] == undefined) {

			section_data.push("background-image")

			$('#slideshow .slides').append('<section class="slide' + parent_slide + '"  data-background-image="' + parent_background_image + '" data-background-color="' + parent_background_color + '" class="slide' + parent_slide + '"><div class="slide_content" style="color: ' + parent_font_color + '"><div class="slide_title" position: fixed; margin-top: ' + parent_title_y + '; left: ' + parent_title_x + '; font-size: ' + parent_title_size + '">' + parent_title + '</div>' + parent_content + '</div></section>');
		}

		else if (val.Content["Sub Slides"] != undefined) {
			$('#slideshow .slides').append('<section class="slide' + parent_slide + '"></section>')
			$('#slideshow .slides .slide' + parent_slide).append('<section data-background-image="' + parent_background_image + '" data-background-color="' + parent_background_color + '" ><div class="slide_content"  style="color: ' + parent_font_color + '"><div class="slide_title" style="position: fixed; top: ' + parent_title_y + '; left: ' + parent_title_x + '; font-size: ' + parent_title_size + '">' + parent_title + '</div>' + parent_content + '</div></section>')

			$.each(val.Content["Sub Slides"], function(index, val) {

				var slide_background = "";
				var slide_image = "";
				var slide_font = "";

				if (val.Configuration["Background Color"] != undefined) {

					slide_background = val.Configuration["Background Color"];

				} else {

					slide_background = parent_background_color;

				}

				if (val.Configuration["Background Image"] != undefined) {

					slide_image = url['base'] + "/content/" + program + "/@Media/" + val.Configuration["Background Image"];
				}

				if (val.Configuration["Font Color"] != undefined) {

					slide_font = val.Configuration["Font Color"]

				} else if (parent_font_color != "") {

					slide_font = parent_font_color;
				}

				var content = BBCodeParser.process(val["Content"])
				$('#slideshow .slides .slide' + parent_slide).append('<section class="slide' + parent_slide + 'b' + index + '"  data-background-image="' + slide_image + '" data-background-color="' + slide_background + '"  ><div class="slide_content" style="color: ' + slide_font + '">' + content + '</div></section>')
			})

		}

	})

	Reveal.initialize({
		width: 800,
		height: 420,
		loop: loop_slideshow,
		transition: content.Configuration["Transition"],
		backgroundTransition: content.Configuration["Background Transition"]
	});

	Reveal.slide(0, 0, 0);

	if (Reveal.isOverview() == true) {
		Reveal.toggleOverview();
	}

};


