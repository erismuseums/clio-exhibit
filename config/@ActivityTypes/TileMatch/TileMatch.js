// TileMatch.js
// Used for the Tile Match activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Left pane card click
	$(document).on("click", "#left_cards .card", function() {

		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$("#left_cards").removeClass('selected');
		} else {
			$('#left_cards .card').each(function() {
				$(this).removeClass('selected')
			})
			$("#left_cards").addClass('selected');
			$(this).addClass('selected')
		}

		var right_selected = $('#right_cards .card.selected').attr("data-title");
		var right_match = $('#right_cards .card.selected').attr("data-answer");
		var right_disabled = $('#right_cards .card.selected').hasClass("disabled")

		var left_selected = $('#left_cards .card.selected').attr("data-title");
		var left_match = $('#left_cards .card.selected').attr("data-answer");
		var left_disabled = $('#left_cards .card.selected').hasClass("disabled")

		if (right_selected == left_match && right_match == left_selected && right_selected != undefined && left_selected != undefined && left_disabled == false && right_disabled == false) {

			$('#left_cards .card.selected').addClass('disabled');


			$('#right_cards .card.selected').addClass("disabled");


			var total_cards = $('#cards .card').length
			var available_cards = $('#cards .disabled').length

			var response = $('#left_cards .card.selected').attr("data-correct");
			var toolbar = ""

			$('#left_cards .card.selected').removeClass('selected');
			$('#right_cards .card.selected').removeClass('selected');

			if (available_cards == total_cards) {
				toolbar = '<button class="lookcloser_done" id="tilematch_done" onclick="$.fancybox.destroy();">Done</button>'
			} else {
				toolbar = '<button class="lookcloser_next" id="tilematch_next">Next</button>'
			}

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"success",
		        "Text":"Correct!"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		        "Type":"custom",
		        "HTML":toolbar,
		        "Swipe":false,
		        "Close:":false,
		        "Prefix":"tilematch_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);


		}

		else if (right_selected != left_match && right_match != left_selected && right_selected != undefined && left_selected != undefined && left_disabled == false && right_disabled == false) {

			var response = $('#right_cards .card.selected').attr("data-incorrect");
			var toolbar = ""

			$('#left_cards .card.selected').removeClass('selected');

			$('#right_cards .card.selected').removeClass('selected');

			var total_cards = $('#cards .card').length
			var available_cards = $('#cards .disabled').length



			if (available_cards == total_cards) {
				toolbar = '<button class="lookcloser_done" id="tilematch_done" onclick="$.fancybox.destroy();">Done</button>'
			} else {
				toolbar = '<button class="lookcloser_tryagain" id="tilematch_retry">Retry</button>'
			}

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"error",
		        "Text":"Try Again"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		        "Type":"custom",
		        "HTML":toolbar,
		        "Swipe":false,
		        "Close:":false,
		        "Prefix":"tilematch_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);

		}
	})

	//Right pane card click
	$(document).on("click", "#right_cards .card", function() {

		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$("#right_cards").removeClass('selected');
		} else {
			$('#right_cards .card').each(function() {
				$(this).removeClass('selected')
			})
			$("#right_cards").addClass('selected');
			$(this).addClass('selected')
		}

		var left_selected = $('#left_cards .card.selected').attr("data-title")
		var left_match = $('#left_cards .card.selected').attr("data-answer")
		var left_disabled = $('#left_cards .card.selected').hasClass("disabled")

		var right_selected = $('#right_cards .card.selected').attr("data-title")
		var right_match = $('#right_cards .card.selected').attr("data-answer")
		var right_disabled = $('#right_cards .card.selected').hasClass("disabled")



		if (left_selected == right_match && left_match == right_selected && right_selected != undefined && left_selected != undefined && left_disabled == false && right_disabled == false) {

			$('#left_cards .card.selected').addClass('disabled');


			$('#right_cards .card.selected').addClass("disabled");


			var total_cards = $('#cards .card').length
			var available_cards = $('#cards .disabled').length

			var response = $('#left_cards .card.selected').attr("data-correct");
			var toolbar = ""

			$('#left_cards .card.selected').removeClass('selected');
			$('#right_cards .card.selected').removeClass('selected');

			if (available_cards == total_cards) {
				toolbar = '<button class="lookcloser_done" id="tilematch_done" onclick="$.fancybox.destroy();">Done</button>'
			} else {
				toolbar = '<button class="lookcloser_next" id="tilematch_next">Next</button>'
			}

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"success",
		        "Text":"Correct!"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		        "Type":"custom",
		        "HTML":toolbar,
		        "Swipe":false,
		        "Close:":false,
		        "Prefix":"tilematch_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);




		} else

		if (left_selected != right_match && left_match != right_selected && right_selected != undefined && left_selected != undefined && left_disabled == false && right_disabled == false) {



			var total_cards = $('#cards .card').length
			var available_cards = $('#cards .disabled').length

			var response = $('#right_cards .card.selected').attr("data-incorrect");
			var toolbar = "";

			$('#left_cards .card.selected').removeClass('selected');

			$('#right_cards .card.selected').removeClass('selected');

			if (available_cards == total_cards) {
				toolbar = '<button class="lookcloser_done" id="tilematch_done" onclick="$.fancybox.destroy();">Done</button>'
			} else {
				toolbar = '<button class="lookcloser_tryagain" id="tilematch_retry">Retry</button>'
			}

			//Create Popup 
		    var options = {
		      "Header":{
		        "Style":"error",
		        "Text":"Try Again"
		      },
		      "Scrollbar":false,
		      "Interaction":{
		        "Type":"custom",
		        "HTML":toolbar,
		        "Swipe":false,
		        "Close:":false,
		        "Prefix":"tilematch_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + response + '</div>'
		      }
		    };
		    createLookCloserPopUp(options);


		}

		console.log("Available Cards: " + $('#cards .disabled').length)
		console.log("Total Cards: " + $('#cards .card').length)

	})

	//Clear the selected cards when a user picks a pair.
	$(document).on("click", '#tilematch_retry, #tilematch_next', function() {
		$.fancybox.destroy();
		$('.card_section').each(function() {
			$(this).removeClass('selected')
		})
	});
	

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadTileMatch(program, content, container) {

	$.each(content.Cards.Left, function(index, val) {
		var image_url = "./content/" + program + "/@Media/" + val["Background"]
		var title = ""
		if (val["Title"] == "") {
			title = "hide_title"
		}
		$('#left_cards').append('<div data-title="' + index + '" class="card" style="background-image: url(' + image_url + ')"><div class="card_info"><span>' + val["Content"] + '</span></div><div class="card_title ' + title + '">' + val["Title"] + '</div></div>')

	})

	$.each(content.Cards.Right, function(index, val) {
		var image_url = "./content/" + program + "/@Media/" + val["Background"]
		var title = ""
		if (val["Title"] == "") {
			title = "hide_title"
		}

		$('#right_cards').append('<div data-title="' + index + '" class="card" style="background-image: url(' + image_url + ')"><div class="card_info"><span>' + val["Content"] + '</span></div><div class="card_title ' + title + '">' + val["Title"] + '</div></div>')
	})

	$('#right_cards').shuffleChildren();
	$('#left_cards').shuffleChildren();

	$.each(content.Answer, function(index, val) {
		var correct = BBCodeParser.process(val["Correct"])

		var incorrect = BBCodeParser.process(val["Incorrect"])

		$('[data-title=' + val.Pair[0] + ']').attr("data-answer", val.Pair[1])
		$('[data-title=' + val.Pair[0] + ']').attr("data-correct", correct)
		$('[data-title=' + val.Pair[0] + ']').attr("data-incorrect", incorrect)

		$('[data-title=' + val.Pair[1] + ']').attr("data-answer", val.Pair[0])
		$('[data-title=' + val.Pair[1] + ']').attr("data-correct", correct)
		$('[data-title=' + val.Pair[1] + ']').attr("data-incorrect", incorrect)
	})


};


