// FlashCards.js
// Used for the Flashcards activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	

	
})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadTileBuckets(program, content) {
	var program_details = loadProgramDetails(program);


	var buckets = content["Buckets"];
	var tiles = content["Tiles"];

	$.each(buckets,function(index,val){
		$("#buckets .buckets_wrapper").append('<div class="bucket" id="' + index + '"><div class="title">' + val["Name"] + '</div><div class="tile_slots"></div></div>')
	})

	$.each(tiles,function(index,val){
		
		var tile_name = val["Name"];
		var tile_index = index;
		$("#tiles .tiles_wrapper").append('<div class="tile_slot bank dropzone" id="' + tile_index + '"></div>')
		var fillsBuckets = val["FillsBucket"];
		
		$.each(fillsBuckets,function(index,val){
			console.log(tile_index)
			$("#buckets #" + val + " .tile_slots").append('<div class="dropzone ' + tile_index + ' buckets tile_slot"></div>');
			$("#" + tile_index).append('<div class="tile enabled ' + tile_index + '" ><div class="name">' + tile_name + '</div><div class="return" style="display: none;"></div></div>')
		})

		
	})

	initializeScrollBar("tilebuckets_tiles");
	initializeScrollBar("tilebuckets_buckets");

	const droppable1 = new Draggable.Droppable(document.querySelectorAll('#tile_buckets'), {
	  draggable: '.trait1',
	  dropzone: '.trait1',
	  distance: 100
	});

}


