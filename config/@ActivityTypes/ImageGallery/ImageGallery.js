// ImageGallery.js
// Used for the Image Gallery activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Open the lightbox and jumped to the clicked image.
    $(document).on("click", ".gallery_image", function() {

		var gallery = $(this).data("gallery");
		var clicked_image = $(this).index();
		var images = []

		$('.gallery_image[data-gallery="' + gallery + '"]').each(function(index, val) {

			var description = BBCodeParser.process($(this).data("caption"));
			images.push({
				src: $(this).data('link'),
				caption: description
			})
		})

		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"cg_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": images,
	        "LoadIndex":clicked_image
	      }
	      
	    };
	    createLookCloserPopUp(options);

		

	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadImageGallery(program, content) {

	
	var program_details = loadProgramDetails(program)
	

	//Autohide the subheader
	$('#image_gallery').append('<div id="images"></div>')
	var size = content.Configuration["Size"];
	$.each(content["Images"], function(index, val) {
		var image = val["Image"];
		var description = BBCodeParser.process(val["Description"]);
		var image_url = 'content/' + program + '/@Media/' + image;

		$('#images').append('<div class="gallery_image" data-gallery="image_gallery" data-caption="' + description + '" data-link="' + image_url + '" style="background-image: url(' + image_url + '); width: ' + size + 'px; height: ' + size + 'px;"><div class="ig_expand"></div></div>')
	})
	initializeContainerScrollBar('#image_gallery',true,false,true)

}


