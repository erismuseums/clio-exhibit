// VisualThinking.js
// Used for the Visual Thinking activity type.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//Toggle bullet points.
    $(document).on("click", "#visual_thinking .bullet .header", function() {
		if ($(this).parent('.bullet').hasClass('open') == true) {

			$(this).parent('.bullet').removeClass('open')

		} else {
			$(this).parent('.bullet').addClass('open')

		}

	})

    //Open the visual thinking image gallery.
	$(document).on("click", ".vt_image", function() {

		var gallery = $(this).data("gallery");

		var images = []

		$('.vt_gallery').each(function(index, val) {

			var description = BBCodeParser.process($(this).data("caption"));
			images.push({
				src: $(this).data('image'),
				caption: description
			})
		})

		
		//Create Popup 
	    var options = {
	      "Header":{
	        "Style":"default",
	        "Text":"Gallery"
	      },
	      "Scrollbar":false,
	      "Interaction":{
	        "Type":"acknowledge",
	        "Prefix":"vt_"
	      },
	      "Content":{
	        "Type":"gallery",
	        "Data": images	      }
	      
	    };
	    createLookCloserPopUp(options);

	})

})

/* ==========================================================================
   Activity Function
   ========================================================================== */

function loadVisualThinking(program, content) {


	var first = Object.keys(content.Media)[0]
	var first_image = content.Media[first]
	var image_url = "./content/" + program + "/@Media/" + first_image["Image"];

	$('#visual_thinking').append('<div class="vt_image" data-link="' + image_url + '" style="background-image:url(' + image_url + ')"><div class="vt_expand"></div></div>');
	$('#visual_thinking').append('<div class="details"></div>');

	var final_images = [];

	$.each(content.Media, function(index, val) {

		var image_url = './content/' + program + "/@Media/" + val["Image"]
		var content = BBCodeParser.process(val["Caption"])

		final_images.push('<a style="display: none" class="vt_gallery" data-image="' + image_url + '" data-gallery="vt' + index + '" data-caption="' + content + '" ></a>')


	})

	$('#visual_thinking .vt_image').append(final_images.join(""))
	$('#visual_thinking .details').append('<div class="title"><div class="header"><span>' + content['Query'] + '</span></div></div>');

	$.each(content.Observations, function(index, val) {
		var content = BBCodeParser.process(val["Content"])

		$('#visual_thinking .details').append('<div class="bullet"><div class="header"><div class="button"></div><span>' + val['Header'] + '</span></div><div class="description">' + content + '</div></div>');
	})

	initializeContainerScrollBar('#visual_thinking .details',true,false,true)

}



