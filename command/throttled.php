<?php

$raw_output = shell_exec("sudo vcgencmd get_throttled 2>&1; echo $?");

if(strpos($raw_output, "throttled=") !== false){
    $status = str_replace("\n0\n","",str_replace("throttled=","",$raw_output));
    if($status == "0x50005"){
    	$output = "Currently throttled";
    } else if($status == "0x50000"){
    	$output = "Throttling has occured";
    } else if($status == "0x0"){
    	$output = "Not throttled";
    } else {
    	$output = "Not Available";
    }

} else {
	$output = "Not Available";
}

echo $output;

?>