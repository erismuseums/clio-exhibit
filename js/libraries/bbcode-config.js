/* 
 * Javascript BBCode Parser Config Options
 * @author Philip Nicolcev
 * @license MIT License
 */

var parserColors = [ 'gray', 'silver', 'white', 'yellow', 'orange', 'red', 'fuchsia', 'blue', 'green', 'black', '#cd38d9' ];

var parserTags = {
	'b': {
		openTag: function(params,content) {
			return '<b>';
		},
		closeTag: function(params,content) {
			return '</b>';
		}
	},
	'i': {
		openTag: function(params,content) {
			return '<i>';
		},
		closeTag: function(params,content) {
			return '</i>';
		}
	},
	's': {
		openTag: function(params,content) {
			return '<s>';
		},
		closeTag: function(params,content) {
			return '</s>';
		}
	},
	'u': {
		openTag: function(params,content) {
			return '<span style="text-decoration:underline">';
		},
		closeTag: function(params,content) {
			return '</span>';
		}
	},
	'image': {
		openTag: function(params,content) {

			var tags = processBBCodeParams(params);
			var url = getCurrentURL();
			var image = "";

			if (!tags['program'] && !tags['section'] && !tags['exhibit'] && !tags['object']){
				tags['program'] = url['program'];
				tags['section'] = url['section'];
				tags['exhibit'] = url['exhibit'];
				tags['object'] = url['object'];
			}

			if(!tags['gallery']) {
				tags['gallery'] = content
			}

			var directory = "./content/" + program + "/@Media/" + content + "/"
			
			image = "./content/" + tags['program'] + "/@Sections/" + tags['section'] + "/@Exhibits/" + tags['exhibit'] + "/@Objects/" + tags['object'] + "/@Media/" + content;
			
			return '<div class="imageset_wrapper" data-link="'+ image + '"><div style="background-image:url(' + image + ')" class="description_image"><div class="expand"></div></div>/div></div>';
		},
		closeTag: function(params,content) {
			return '';
		},
		content: function(params,content) {
			return '';
		}
	},
	'imageset': {
		openTag: function(params,content) {
			
			var tags = processBBCodeParams(params);
			var url = getCurrentURL();
			var gallery = '<div class="imageset_wrapper">';

			var program = ""
			
			if(tags['program'] != undefined){
				program = tags['program'];
			} else {
				program = getSetting("setting__default_program","NaturesNetworks");
			}

			var directory = "./content/" + program + "/@Media/" + content + "/"

			var images = parseApacheDirectory(directory);

			$.each(images,function(index,value){
				var image = directory + value;

				gallery = gallery + '<div class="description_image_wrapper" data-link="'+ image + '" ><div style="background-image:url(' + image + ')" class="description_image"><div class="expand"></div></div></div>'
			})			

			gallery = gallery + '</div>'

			return gallery;
		},
		closeTag: function(params,content) {
			return '';
		},
		content: function(params,content) {
			return '';
		}
	},
	'list': {
		openTag: function(params,content) {
			var classes = ""
			var tags = processBBCodeParams(params);

			if (tags['style']){
				classes = "style=\'list-style-type:" + tags['style'] + "\'";
			}

			return '<ul ' + classes + '>';
		},
		closeTag: function(params,content) {
			return '</ul>';
		},
		restrictChildrenTo: ["*", "li"]
	},
	'li': {
		openTag: function(params,content) {
			return '<li>';
		},
		closeTag: function(params,content) {
			return '</li>';
		},
		restrictChildrenTo: ["*", "li"]
	},
	'*': {
		openTag: function(params,content) {
			return '<li>';
		},
		closeTag: function(params,content) {
			return '</li>';
		},
		restrictChildrenTo: ["*", "li"]
	},
	'noparse': {
		openTag: function(params,content) {
			return '';
		},
		closeTag: function(params,content) {
			return '';
		},
		noParse: true
	},
	'quote': {
		openTag: function(params,content) {
			return '<blockquote>';
		},
		closeTag: function(params,content) {
			return '</blockquote>';
		}
	},
	'p': {
		openTag: function(params,content) {
			return '<p>';
		},
		closeTag: function(params,content) {
			return '</p>';
		}
	},
	'h1': {
		openTag: function(params,content) {
			return '<h1>';
		},
		closeTag: function(params,content) {
			return '</h1>';
		}
	}	,
	'h2': {
		openTag: function(params,content) {
			return '<h2>';
		},
		closeTag: function(params,content) {
			return '</h2>';
		}
	},
	'h3': {
		openTag: function(params,content) {
			return '<h3>';
		},
		closeTag: function(params,content) {
			return '</h3>';
		}
	},
	'h4': {
		openTag: function(params,content) {
			return '<h4>';
		},
		closeTag: function(params,content) {
			return '</h4>';
		}
	},
	'h5': {
		openTag: function(params,content) {
			return '<h5>';
		},
		closeTag: function(params,content) {
			return '</h5>';
		}
	},
	'object': {
		openTag: function(params,content) {
			
			var tags = processBBCodeParams(params);
			var url = getCurrentURL();
			var content = "";

			if (!tags['program'] && !tags['section'] && !tags['exhibit']){
				tags['program'] = url['program'];
				tags['section'] = url['section'];
				tags['exhibit'] = url['exhibit'];
			}


			var content = loadObjectContent(tags['program'], tags['section'], tags['exhibit'], tags['object']);

			
			var link = './content/' + tags['program'] + '/@Sections/' + tags['section'] + '/@Exhibits/' + tags['exhibit']  + '/@Objects/' + tags['object'] + '/@Media/' + content.Content.Media['Image'];

			var template = '<div class="object"><div class="object_image object_button" data-box="' + tags['program'] + '" data-section="' + tags['section'] + '" data-exhibit="' + tags['exhibit'] + '" data-object="' + tags['object'] + '" style="background-image:url(' + link + ')"></div><div class="object_content"><div class="object_header object_button" data-box="' + tags['program'] + '" data-section="' + tags['section'] + '" data-exhibit="' + tags['exhibit']+ '" data-object="' + tags['object'] + '">' + content.Content['Name'] +  '</div>' + content.Content.Description['Brief'] + '</div><div class="object_link object_button" data-box="' + tags['program'] + '" data-section="' + tags['section'] + '" data-exhibit="' + tags['exhibit'] + '" data-object="' + tags['object'] + '"></div></div>'

			return template;
		},
		closeTag: function(params,content) {
			
			return '';
		},
		content: function(params,content) {
			
			return '';
		}
	}
};
