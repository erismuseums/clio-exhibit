(function() {
  const currentScript = document.currentScript;
  window.addEventListener(
    "load",
    function() {
      const id = `exhibit_screensaver`;
      const el = document.body.appendChild(document.createElement("div"));
      el.id = id;
      el.style.position = "fixed";

      el.className = "Screensaver";
      el.innerHTML = `<div><div style="background-image:url(${(currentScript &&
        currentScript.getAttribute("image")) ||
        'img/white/interact/touch.svg'})"><span>${(currentScript &&
        currentScript.getAttribute("message")) ||
        window.location.hostname}</span></div></div>`;

      const width = el.offsetWidth;
      const height = el.offsetHeight;

      const style = document.body.appendChild(document.createElement("style"));

      style.textContent = `
#${id} {
  left: 0; top: 0; right: 0; bottom: 0;
  z-index: 100000;
  color: #eee;
  background-color: #000;
}
#${id} div {
  width: ${width}px;
  height: ${height}px;
  line-height: 1;
}
#${id} > div {
  animation: x${id} 13s linear infinite alternate;
}
#${id} > div > div {
  animation: y${id} 7s linear infinite alternate;
  width: 300px;
  height:230px;
  font-family: "Open Sans";
  font-size: 2em;
  background-image: url('img/white/interact/touch.svg');
  background-repeat: no-repeat;
  background-position: top center;
  background-size: 200px;
  display: flex;
  justify-content: center;
  align-items: flex-end;
}
#${id} > div > div > span {
  
}
@keyframes x${id} {
  100% {
    transform: translateX(calc(100vw - 300px));
  }
}

@keyframes y${id} {
  100% {
    transform: translateY(calc(100vh - 230px));
  }
}
`;
      let timeoutId = null;
      let timeout =
        (currentScript && Number(currentScript.getAttribute("timeout"))) ||
        180000;

      function disable() {
        
        var device_brightness = getLocal("device_brightness", 4)
        if(!$('body').hasClass("noscreensaver")){
          setScreenBrightness(device_brightness);
        }
        
        el.style.display = "none";

        timeoutId && clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
          if(!$('body').hasClass("noscreensaver")){
                  
            setScreenBrightness(1)
          };
          
          el.style.display = "block";

          
        }, timeout);
        event.preventDefault();
      }
      disable();
      document.addEventListener("mousedown", disable);
      document.addEventListener("keydown", disable);
      document.addEventListener("scroll", disable);
    },
    { once: true }
  );
})();
