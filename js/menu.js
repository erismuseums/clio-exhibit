// Menu.js
// This file contains event scripts used by the menu bar system.
$(document).ready(function() {

  /* ==========================================================================
     General Menu
     ========================================================================== */

  //Menu button 'selected' script
  //Go to this menu when a menu button is clicked.
  $('.menu_button.clickable').click(function() {

    destroyContainerScrollBar("#menu_content_wrapper .menu_content");
    

    //Get the 'clicked' button id.
    var section = {};
    section['menu'] = $(this).attr('id').replace('_button', '')

    if ($('body #menu').hasClass('collapsed')) {
      $('#menu').removeClass('collapsed');
      $('#menu').addClass('minimal');
      $('#menu').addClass('full_menu');
    }

    loadMenuSection(section);
    openMenuSection(section);

  });

  //The Exhibit Menu is the 'homescreen'.
  //when we click on the Exhibit Menu button, we are really closing the menu system.
  $(document).on("click",'#home_button',function(){
    var url = getCurrentURL();
    closeMenu();
    $('#menu').addClass('closed')
    $('#menu .bumper .selected').removeClass('selected')
    setURLParameters(url["mode"]);
  });

  /* ==========================================================================
     Submenu
     ========================================================================== */

  //Button to go back to the main menu from a sub menu.
  $('#menu_breadcrumbs .button').click(function() {

    //Get the current menu through the URL, split it into an array with forward slash as the delimiter, and remove the submenu.
    var url = getCurrentURL();
    var menu_location = url["menu"].split("/")
    var menu_new = menu_location.splice(0, menu_location.length - 1)

    //Define the menu section to navigate to.
    //Use the menu above the current submenu.
    var section = {};

    if (menu_new[0]) {
      section['menu'] = menu_new[0];
    }

    if (menu_new[1]) {
      section['tier1'] = menu_new[1];
    }

    //Load the menu section.
    loadMenuSection(section)

    //Update the URL query string.
    setURLParameters(url["mode"],section)

  })

  /* ==========================================================================
     Exhibit Menu (homescreen)
     ========================================================================== */

  //Exhibit Menu button to open the Activity menu.
  $(document).on("click", '#add_activity', function() {

    //Define the menu section to navigate to.
    var section = {};
    section['menu'] = "program"

    //Load the menu section and open it.
    loadMenuSection(section);
    openMenuSection(section);

  });

  /* ==========================================================================
     Facilitation Menu
     ========================================================================== */

  //Facilitation link script
  //This is used within a discussion to navigate back to the facilitation menu.
  $(document).on("click", '.facilitation_link', function() {

    var url = getCurrentURL();

    //Define the menu section to navigate to.
    var section = {};
    section['menu'] = "facilitation";

    //Load the menu section.
    loadMenuSection(section)

    //Update the URL query string.
    setURLParameters(url["mode"],section)

  })

  //Discussion link script
  //This is used by the facilitation menu to load a discussion.
  $(document).on("click", '.discussion_button', function() {
    var url = getCurrentURL();
    //Get the program and discussion to load from the button the user clicked.
    var program = $(this).data("program");
    var discussion = $(this).data("discussion");

    //Define the menu section.
    //This is the facilitation menu, with the discussion clicked by the user.
    var section = {};
    section['menu'] = "facilitation";
    section['tier1'] = discussion;

    //Update the URL query string.
    setURLParameters(url["mode"],section)

    //Load the discussion into the menu container.
    loadMenuSection(section);

  })

  //Opens and collapses the Facilitation menu subsections
  $(document).on("click", "#facilitation_menu .facilitation_header", function() {

    var section = $(this).attr("id");
    var content_section = section.replace("facilitation_", "")

    $(this).toggleClass('open')

    if ($("#" + content_section + ":visible").length == 0) {
      $("#" + content_section).show()
    } else {
      $("#" + content_section).hide()
    }

    $("#menu_content_wrapper .menu_content").mCustomScrollbar("update");

  });

  /* ==========================================================================
     Help Menu
     ========================================================================== */

  //Opens and collapses the Help menu subsections
  $(document).on("click", "#info_menu .help_header", function() {

    var section = $(this).attr("id");
    var content_section = section.replace("section_", "content_")

    $(this).toggleClass('open')

    if ($("#" + content_section + ":visible").length == 0) {
      $("#" + content_section).show()
    } else {
      $("#" + content_section).hide()
    }

    $("#menu_content_wrapper .menu_content").mCustomScrollbar("update");

  });

  //Loads the Information menu from the Help menu.
  $(document).on("click", '.clio_button', function() {
    
    
    var url = getCurrentURL();
    //Define the menu section to load.
    var section = {};
    section['menu'] = "about";
    section['tier1'] = "clio";

    //Update the URL query string.
    setURLParameters(url["mode"],section)

    //Load the submenu into the menu container.
    loadMenuSection(section);


  })

  //Loads the Getting Started menu from the Help menu.
  $(document).on("click", '.setup_button', function() {
    
    var url = getCurrentURL();

    //Define the menu section to load.
    var section = {};
    section['menu'] = "about";
    section['tier1'] = "setup";

    //Update the URL query string.
    setURLParameters(url["mode"],section)

    //Load the submenu into the menu container.
    loadMenuSection(section);

  })

  /* ==========================================================================
     Settings Menu
     ========================================================================== */

  //Clears LocalStorage, prompts the user the settings are being reset and reloads the page.
  $(document).on("click", '#clear_caches', function() {
    
    $("#loading").show();
    
    clearSettings();
    clearSession();

    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Settings are being reset.', 'error', 2, function() {});
    
    clearBrowserCache();
    window.location.reload(true);
    
  })

  $(document).on("click", '#kiosk_poweroff', function() {
    
    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Power Off Kiosk?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_poweroff">Power Off</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to power off the kiosk?</p></div>'
        }
      };
      createLookCloserPopUp(options);
    
  })

  $(document).on("click", '#confirm_poweroff', function() {
    console.log("powering down")
    $.ajax({
      url: "command/power.php?action=power_off",
    success: function(data) {
      
    }
  })
    
  })

  

  $(document).on("click", '#kiosk_restart', function() {
    
    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Reboot Kiosk?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_restart">Restart</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to restart the kiosk?</p></div>'
        }
      };
      createLookCloserPopUp(options);
    
  })

  $(document).on("click", '#confirm_restart', function() {
    console.log("restarting")
    $.ajax({
      url: "command/power.php?action=power_restart",
      success: function(data) {
        
      }
    })

  })

  $(document).on("click", '#kiosk_refresh', function() {
    clearBrowserCache();
    window.location.reload(true);
  })

  $(document).on("click", '#temperature_update', function() {
    
    $.ajax({
      url: "command/temperature.php",
      success: function(data) {
        
        if(data != "Not Available"){
          $("#kiosk_temperature span").html(data + "&deg;C")

          if(data > 75){
            $("#kiosk_temperature span").css("color","red");
          } else if(data > 75 && data < 60){
            $("#kiosk_temperature span").css("color","orange");
          } else {
            $("#kiosk_temperature span").css("color","green");
          }

        } else {
          $("#kiosk_temperature span").html(data)
        }

        
      }
    });
  })

  $(document).on("click", '#throttle_update', function() {
    
    $.ajax({
      url: "command/throttled.php",
      success: function(data) {
        
        if(data != "Not Available"){
          $("#kiosk_throttle span").html(data)


        } else {
          $("#kiosk_throttle span").html(data)
        }

        
      }
    });
  })

 
  $(document).on("click", '#kiosk_development', function() {
    
    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Enable Development Mode?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="cancel_development" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_development">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to put the kiosk into Development Mode?</p></div>'
        }
      };
      createLookCloserPopUp(options);
    
  })

  $(document).on("click", '#confirm_development', function() {
    
    $.ajax({
      url: "command/development.php?mode=development",
      success: function(data) { 
        
      }
    });

    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Reboot Kiosk?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_restart">Restart</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>This change requires restarting the kiosk to take effect.</p><p>Would you like to do this now?</p></div>'
        }
      };
      createLookCloserPopUp(options);
  })

  $(document).on("click", '#kiosk_production', function() {
    
    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Enable Production Mode?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="cancel_development" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_production">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to put the kiosk into Production Mode?</p></div>'
        }
      };
      createLookCloserPopUp(options);
    
  })

  $(document).on("click", '#confirm_production', function() {
    
    $.ajax({
      url: "command/development.php?mode=production",
      success: function(data) { 
        
      }
    });

    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Reboot Kiosk?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_restart">Restart</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>This change requires restarting the kiosk to take effect.</p><p>Would you like to do this now?</p></div>'
        }
      };
      createLookCloserPopUp(options);
  })

  $(document).on("click", '#kiosk_rotate', function() {

    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Rotate Kiosk Display?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_rotate">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you would like to rotate the kiosk display by 180&deg;?</p></div>'
        }
      };
      createLookCloserPopUp(options);
  })

  $(document).on("click", '#confirm_rotate', function() {
    
    $.ajax({
      url: "command/rotate.php",
      success: function(data) { 
        
      }
    });

    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Reboot Kiosk?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="power_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_restart">Restart</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>This change requires restarting the kiosk to take effect.</p><p>Would you like to do this now?</p></div>'
        }
      };
      createLookCloserPopUp(options);
  })


  
  //Facilitator Screen brightness
  //When in Facilitator Mode, the facilitator can change the screen brightness through settings.
  //These scripts use AJAX to run a PHP script, which in turn runs a Python script to change the the screen brightness through the operating system.
  //This brightness setting is saved to LocalStorage and used whenever the kiosk is restarted.
  //Users will be able to change the screen brightness from the accessibility settings, but it will always revert to the Facilitator's setting.
  $(document).on("click", '#settings_menu .brightness_control', function() {
    
    var facilitator_brightness = $(this).attr("id").replace("brightness","")
    
    setScreenBrightness(facilitator_brightness,"device")

    var configuration = formatConfigurationSettings();
    saveConfigurationSettings(configuration);

    $('.brightness_control').each(function(index, val) {

      if (index < facilitator_brightness) {
        $(this).removeClass('unselected')
        $(this).addClass('selected')
      } else {
        $(this).removeClass('selected')
        $(this).addClass('unselected')
      }

    })
  })

  //Program - Setting to change the Default program.
  $(document).on("change", '#setting__default_program', function() {

    
    //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Change Program?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="cancel_program">Cancel</button><button id="confirm_program">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"power_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to load a new program?</p></div>'
        }
      };
      createLookCloserPopUp(options);
    

    

  })

  $(document).on("click", '#confirm_program', function() {
      //Show the loading screen.
    $("#loading").show()

    //Alert the user that the new Program is loading.
    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Loading Program...', 'success', 2, function() {});
    
    //Get the selected Program and set the localstorage.
    var setting__default_program = $('#setting__default_program').val()
    console.log(setting__default_program)
    setLocal("setting__default_program", setting__default_program)
    
    var configuration = formatConfigurationSettings();

    saveConfigurationSettings(configuration);

    //Remove any selected activities from both session and local storage.
    sessionStorage.removeItem("selected_activities")
    localStorage.removeItem("selected_activities")
    
    //Reload the page.
    setTimeout(
      function() 
      {
        clearBrowserCache();
        window.location.reload(true);
      }, 500
    )

    

  });

  $(document).on("click", '#cancel_program', function() {
      var default_program = getLocal("setting__default_program", "Default");
      console.log(default_program)
      $('#setting__default_program').val(default_program).trigger('change');
      $.fancybox.destroy();
  });

  //Activity Timeout - Setting to change how long before returning home while in exhibit mode.
  $(document).on("change", '#setting__activity_timeout', function() {

    var setting__activity_timeout = $('#setting__activity_timeout').val()
    setLocal("setting__activity_timeout", setting__activity_timeout)

    var configuration = formatConfigurationSettings();
    console.log(configuration)
    saveConfigurationSettings(configuration);

  })

  //Theme - Setting to override the interface colors and themes.
  $(document).on("change", '#setting__contrast_override', function() {
    destroyContainerScrollBar("#menu_content_wrapper .menu_content")
    
    //Because Accessibility contrast modes will override Settings contrast modes, we need to get the current accessibility contrast mode setting.
    var setting__contrast_mode = getAccessibility("setting__contrast_mode", "default")

    //Get the contrast mode theme that the facilitator just selected, and set it in settings.
    var setting__contrast_override = $('#setting__contrast_override').val()
    setLocal("setting__contrast_override", setting__contrast_override)

    var configuration = formatConfigurationSettings();
    saveConfigurationSettings(configuration);

    //We need to check to make sure that accessibility settings will always override facilitator settings.
    //This gives individual users of a kiosk the ability to temporarily change the interface to their needs.

    //If both the Settings Theme and Accessibility Contrast mode are disabled, remove all themes.
    if (setting__contrast_override == "disabled" && setting__contrast_mode == "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')

    } 

    //If Setting Theme is disabled but Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override == "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is disabled, use the Theme setting.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode == "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_override + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_override);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    }

    initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true)
  })

  //Slide Duration - Setting to change how long to display each slide in Exhibit Mode.
  $(document).on("change", '#setting__slide_duration', function() {

    var setting__slide_duration = $('#setting__slide_duration').val()
    setLocal("setting__slide_duration", setting__slide_duration)

    var configuration = formatConfigurationSettings();
    saveConfigurationSettings(configuration);

  })

  //Slide Duration - Setting to change how long to display each slide in Exhibit Mode.
  $(document).on("change", '#setting__screensaver', function() {

    var setting__screensaver = $('#setting__screensaver').val()

    setLocal("setting__screensaver", setting__screensaver);

    var configuration = formatConfigurationSettings();
    saveConfigurationSettings(configuration);

    location.reload();

  })

  /* ==========================================================================
     Accessibility Menu
     ========================================================================== */

  //Reset accessibility options and notifies the user.
  $(document).on("click", '#reset_accessibility', function() {

    clearAccessibility();
    
    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Accessibility options have been set to their defaults.', 'error', 2, function() {});

  })

  //Facilitator Screen brightness
  //When in Exhibit Mode, the user can change the screen brightness through settings.
  //These scripts use AJAX to run a PHP script, which in turn runs a Python script to change the the screen brightness through the operating system.
  //This brightness setting is saved to SessionStorage and is reset whenever the kiosk returns to the homescreen.
  //Users will be able to change the screen brightness from the accessibility settings, but it will always revert to the Facilitator's setting.
  $(document).on("click", '#user_brightness .brightness_control', function() {
    
    var user_brightness = $(this).attr("id").replace("accessibility_brightness","")
    
    setScreenBrightness(user_brightness)

    $('#user_brightness .brightness_control').each(function(index, val) {

      if (index < user_brightness) {
        $(this).removeClass('unselected')
        $(this).addClass('selected')
      } else {
        $(this).removeClass('selected')
        $(this).addClass('unselected')
      }

    })
  })

  //Contrast Mode - Setting to change the default theme that is used for the entire interface.
  $(document).on("change", '#setting__contrast_mode', function() {

    destroyContainerScrollBar("#menu_content_wrapper .menu_content")

    //Get the user selected theme from the dropdown.
    var setting__contrast_mode = $('#setting__contrast_mode').val()

    //Get the currently loaded theme.
    var setting__contrast_override = getLocal("setting__contrast_override", "disabled")

    //Set the accessibility option in sessionstorage
    setAccessibility("setting__contrast_mode", setting__contrast_mode)

    //If both the Settings Theme and Accessibility Contrast mode are disabled, remove all themes.
    if (setting__contrast_override == "disabled" && setting__contrast_mode == "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')

    } 

    //If Setting Theme is disabled but Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override == "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is disabled, use the Theme setting.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode == "default") {
      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_override + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_override);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    }

    initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true)

  })

  //Font Size - Options to change the font size.
  $(document).on("change", '#setting__font_size', function() {

    var setting__font_size = $('#setting__font_size').val()
    setAccessibility("setting__font_size", setting__font_size)

    removeBodyClassByPrefix('accessibility_font_size_')
    $('body').addClass("accessibility_font_size_" + setting__font_size );
   
  })

  //Font Face - Options to override the default font family.
  $(document).on("change", '#setting__font_face', function() {

    var setting__font_face = $('#setting__font_face').val()
    setAccessibility("setting__font_face", setting__font_face)

    removeBodyClassByPrefix('accessibility_font_face_')
    $('body').addClass("accessibility_font_face_" + setting__font_face);

  })

  //Paragraph Alignment - Options to change the paragraph alignment
  $(document).on("change", '#setting__paragraph_alignment', function() {

    var setting__paragraph_alignment = $('#setting__paragraph_alignment').val()
    setAccessibility("setting__paragraph_alignment", setting__paragraph_alignment)

    removeBodyClassByPrefix('accessibility_paragraph_alignment_')
    $('body').addClass("accessibility_paragraph_alignment_" + setting__paragraph_alignment);
    
  })

  //Paragraph Spacing - Option to change the spacing between the lines of each paragraph.
  $(document).on("change", '#setting__paragraph_spacing', function() {

    var setting__paragraph_spacing = $('#setting__paragraph_spacing').val()
    setAccessibility("setting__paragraph_spacing", setting__paragraph_spacing)

    removeBodyClassByPrefix('accessibility_paragraph_spacing_')
    $('body').addClass("accessibility_paragraph_spacing_" + setting__paragraph_spacing);

  })

  //Media Autoplay
  $(document).on("change", '#setting__autoplay', function() {

    var setting__autoplay = $('#setting__autoplay').val()
    setAccessibility("setting__autoplay", setting__autoplay)

  })

  //Media Autoplay
  $(document).on("change", '#setting__playback_speed', function() {

    var setting__playback_speed = $('#setting__playback_speed').val()
    setAccessibility("setting__playback_speed", setting__playback_speed)

  })

  /* ==========================================================================
     Activities Menu
     ========================================================================== */

  //Grid or Details View - Buttons to toggle different views.
  $(document).on("click", "#activity_sort .activity_view .sort_button", function() {
    var url = getCurrentURL();
    //Remove all selected classes from sort buttons.
    $('#activity_sort .sort_button').each(function() {
      $(this).removeClass('selected');
    })

    //Select the clicked button.
    $(this).addClass('selected');

    //Get which view was clicked.
    var view = $(this).attr("id").replace("activity_", "");
    
    //Define our current position for the URL.
    var section = {};
    section['menu'] = "program";

    //Update the URL parameters to include our view preference.
    setURLParameters(url["mode"],section, "", "", view, "", "");
    
    //Remove the current view class from the Activity menu and add the new one.
    removeSelectorClassByPrefix("#program_menu", "view_");
    $("#program_menu").addClass("view_" + view);
    $("#menu_content_wrapper .menu_content").mCustomScrollbar("update");
  })

  //Sort Bar - Audience Dropdown search
  $(document).on("change", "#sort__audience", function() {

    //Get the current criteria options for both audience and activity type.
    var sort_audience = $('#sort__audience').val();
    var sort_type = $('#sort__activity_type').val();

    //Go through each activity and hide the ones that do not meet these criteria.
    $('#program_menu .activity').each(function(index, val) {

      //Get the audience and activity type for this individual activity.
      var this_audience = $(this).data("audience")
      var this_type = $(this).data("type")

      //Apply logic about whether to show or hide it, based on both criteria.
      if (sort_audience == 'disabled' && sort_type == 'disabled') {
        $(this).show()
      }

      else if (this_audience != sort_audience && sort_type == 'disabled') {
        $(this).hide()
      } 

      else if (this_type != sort_type && sort_audience == 'disabled') {
        $(this).hide()
      } 

      else if (sort_audience == 'disabled' && this_type == sort_type) {
        $(this).show()
      } 

      else if (sort_type == 'disabled' && this_audience == sort_audience) {
        $(this).show()
      } 

      else if (sort_audience != this_audience && this_type == sort_type) {
        $(this).hide()
      } 

      else if (sort_audience != this_audience && this_type == sort_type) {
        $(this).hide()
      } 

      else if (sort_audience != this_audience && sort_type != this_type) {
        $(this).hide()
      } 

      else if (sort_audience == this_audience && sort_type == this_type) {
        $(this).show()
      } 

      else {
        $(this).hide()
      }

    })

    //If there are no visible activities, display a no results warning.
    if ($('#program_menu').children('.activity:visible').length == 0) {
      $("#no_results").show()
    } else {
      $("#no_results").hide()
    }
      $("#menu_content_wrapper .menu_content").mCustomScrollbar("update");
  })

  //Sort Bar - Activity Type Dropdown search
  $(document).on("change", "#sort__activity_type", function() {

    //Get the current criteria options for both audience and activity type.
    var sort_audience = $('#sort__audience').val();
    var sort_type = $('#sort__activity_type').val();

    //Go through each activity and hide the ones that do not meet these criteria.
    $('#program_menu .activity').each(function(index, val) {

      //Get the audience and activity type for this individual activity.
      var this_audience = $(this).data("audience")
      var this_type = $(this).data("type")

      //Apply logic about whether to show or hide it, based on both criteria.
      if (sort_audience == 'disabled' && sort_type == 'disabled') {
        $(this).show()
      } 

      else if (this_audience != sort_audience && sort_type == 'disabled') {
        $(this).hide()
      } 

      else if (this_type != sort_type && sort_audience == 'disabled') {
        $(this).hide()
      } 

      else if (sort_audience == 'disabled' && this_type == sort_type) {
        $(this).show()
      } 

      else if (sort_type == 'disabled' && this_audience == sort_audience) {
        $(this).show()
      } 

      else if (sort_audience != this_audience && this_type == sort_type) {
        $(this).hide()
      } 

      else if (sort_audience != this_audience && this_type == sort_type) {
        $(this).hide()
      } 

      else if (sort_audience != this_audience && sort_type != this_type) {
        $(this).hide()
      } 

      else if (sort_audience == this_audience && sort_type == this_type) {
        $(this).show()
      } 

      else {
        $(this).hide()
      }

    })

    //If there are no visible activities, display a no results warning.
    if ($('#program_menu').children('.activity:visible').length == 0) {
      $("#no_results").show()
    } else {
      $("#no_results").hide()
    }
      $("#menu_content_wrapper .menu_content").mCustomScrollbar("update");
  })

  //Checkmark beside each activity
  $(document).on("click", '.checkbox', function() {

    //Get the object containing selected activities.
    var selected_activities = getSessionObject("selected_activities", "")
    
    //If it doesn't exist, create an object.
    if (selected_activities == "") {
      selected_activities = {};
    }

    //Get the activity and the program containing it.
    var program = $(this).parents('.activity').data("program");
    var activity = $(this).parents('.activity').data("activity");

    //Create an object to contain formatted information about the activity that was just selected.
    //We will be creating an object containing the program and activity, using an index name that is the relative location of the activity.
    var selected_activity = {};
    var selected_activity_url = "content/" + program + "/@Activities/" + activity + ".json"

    //Get more information aboout the audiences in this program and the activity types available to CLIO.
    var audiences = getAudiences(program);
    var activity_types = getActivityTypes();

    //If this activity was not yet selected, do this.
    if ($(this).hasClass('unselected')) {

      //Switch between the unselected and selected class.  This toggles between the plus icon and the checkmark icon.
      $(this).removeClass('unselected');
      $(this).addClass('selected')

      //This takes the information we previously captured about the program and activity that was selected, and assigns it within the object we created earlier.
      selected_activity["Program"] = program;
      selected_activity["Activity"] = activity;

      //Using the relative url we created earlier as the index, we can push this new activity into the full list of selected activities.
      selected_activities[selected_activity_url] = selected_activity

      //Assign this new object, with the old and new activities, with the session.  This allows the interface to remember which activities are selected between menus and refreshes.
      setSessionObject("selected_activities", selected_activities)

      //Get information about the activity.
      var activity_details = loadActivityDetails(program, activity)

      //Pull the relevant information about the activity type.
      var type = activity_details.Activity["Type"];
      var type_details = activity_types[type]

      //Pull the relevant information about the audience.
      var audience = activity_details.Activity["Audience"];
      var audience_details = audiences[audience]

      //Process the descriptions as rich text.
      var type_description = BBCodeParser.process(type_details["Description"]);
      var audience_description = BBCodeParser.process(audience_details["Description"]);

      //Add this activity to the Exhibit menu list.
      $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + selected_activity_url + '" data-program="' + program + '" data-activity="' + activity + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help" data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');

      //Make sure that the Exhibit menu list of selected activities knows its populated.  Start the scrollbar.
      if ($('#desktop .setup_container').hasClass('empty') == true) {
        $('#desktop .setup_container').removeClass('empty')
        $('#desktop .setup_container').addClass('populated')
        
        initializeContainerScrollBar("#activities",true,false,true)
      }

      //Notify the user that they added an activity.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Added <i>' + activity_details.Activity.Info["Title"] + '</i> to Exhibit Mode.', 'success', 2, function() {});

    } 

    //If this activity was already selected, do this.
    else if ($(this).hasClass('selected')) {

      //Get details about the activity.
      var activity_details = loadActivityDetails(program, activity)

      //Toggle between the selected and unselected class.
      $(this).removeClass('selected');
      $(this).addClass('unselected')

      //Delete this activity from the object of selected activities.
      delete selected_activities[selected_activity_url]

      //Update the session object containing the selected activities.
      setSessionObject("selected_activities", selected_activities)
      
      //Find this activity on the Exhibit menu selected activities list and remove it.
      $('#activities .selected_activities').find("[data-url='" + selected_activity_url + "']").remove()

      //IF there are no more activities listed in the Exhibit menu, then change the container from populated to empty and remove the scrollbar.
      if ($('#activities .selected_activities').find('.activity').length == 0) {
        $('#desktop .setup_container').removeClass('populated')
        $('#desktop .setup_container').addClass('empty')
        destroyContainerScrollBar("#activities")
        $("#exhibit_presets").val("default").trigger('change');

      }

      
      //Alert the user that they've removed the activity.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Removed <i>' + activity_details.Activity.Info["Title"] + '</i> from Exhibit Mode.', 'error', 2, function() {});

    }

  });

  //Open an activity when a user clicks on activity button, on the activity menu or exhibit menu.
  $(document).on("click", '#menu_content_wrapper .activity .content, #activities .activity .content', function() {

    //Get the program and activity of the button that was clicked.
    var program = $(this).parents('.activity').data("program");
    var activity = $(this).parents('.activity').data("activity");

    //This will check whether we are opening the link from the exhibit menu or program menu, so the return home button knows where to bring facilitators.
    if ($('#menu').hasClass('full_menu')) {
      setSession("desktop_link", "true")
    }

    //Load the activity content.
    loadActivity(program, activity);

    //Switch from the homescreen view to the activity view.
    $('body').removeClass('homescreen');
    $('body').addClass('activity');

  })

  //Opens a pop-up containing more information about the activity type or the audience.
  $(document).on("click", ".view_details .activity .content .info .help", function(event) {

    //Get metadata from the button that was clicked.
    var header = $(this).attr("data-title");
    var description = $(this).attr("data-description");

    //Create Popup 
    var options = {
      "Header":{
        "Style":"default",
        "Text":header
      },
      "Scrollbar":"auto",
      "Interaction":{
        "Type":"acknowledge",
        "Prefix":"activityhelp_"
      },
      "Content":{
        "Type":"html",
        "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
      }
    };
    createLookCloserPopUp(options);

    //End the function without propogating up.  This stops the activity button script from triggering.
    return false;
  })

  /* ==========================================================================
     Activity Page
     ========================================================================== */

  //Return Home from an Activity.
  $(document).on("click", '#return_home', function() {
    returnHome();
  })

  //Return Home from an Activity.
  $(document).on("click", '#activity_menu_button', function() {
    
    if($('#activity_menu_button').hasClass('closed')){

      $('#activity_menu_button').removeClass('closed');
      $('#activity_menu_button').addClass('open')

      $('#activity_menu').removeClass('closed');
      $('#activity_menu').addClass('open');
      initializeContainerScrollBar("#activity_menu_list",false,false,true);

    } else {

      $("#activity_menu .activity_menu_header").html("Activity Menu");

      $('#activity_menu_button').removeClass('open');
      $('#activity_menu_button').addClass('closed')

      $('#activity_menu').removeClass('open');
      $('#activity_menu').addClass('closed');

      $('#activity_menu_list').removeClass('closed');
      $('#activity_menu_list').addClass('open');

      $('#activity_menu_subcontent').removeClass('open');
      $('#activity_menu_subcontent').addClass('closed');

      $('#activity_menu_back').addClass('closed');

      if(videojs.getPlayers()["activity_menu_intro"]) {
        delete videojs.getPlayers()["activity_menu_intro"];
      }

      $("#activity_video").html("");



    }

      

  })

  $(document).on("click", '#activity_menu_list .activity_menu_item', function() {
    var section = $(this).attr("data-section");
    var title = $(this).attr("data-section-title");

    $("#activity_menu .activity_menu_header").html(title);
    loadActivitySubMenu(section);
    $('#activity_menu_back').removeClass('closed')
  });

  $(document).on("click", '#activity_menu_back', function() {
    loadActivityMainMenu();

  });


  //Facilitator Mode - Button on activity to add to exhibit mode.
  $(document).on("click", '#facilitator_add', function() {
    
    //Get the list of activities that have been selected during this session.
    var selected_activities = getSessionObject("selected_activities", "")

    //If the object doesn't exist, create it.
    if (selected_activities == "") {
      selected_activities = {}
    }

    //Get information about what program and activity is loaded from the URL.
    var url = getCurrentURL();
    var program = url["program"];
    var activity = url["activity"];

    //Create an object to contain formatted information about the activity that was just selected.
    //We will be creating an object containing the program and activity, using an index name that is the relative location of the activity.
    var selected_activity = {};
    var selected_activity_url = "content/" + program + "/@Activities/" + activity + ".json";

    //Get information about the audiences defined in the program and the activities available to CLIO.
    var audiences = getAudiences(program);
    var activity_types = getActivityTypes();

    //If this activity was not yet selected, do this.
    if ($(this).hasClass('unselected')) {

      //Switch between the unselected and selected class.  This toggles between the plus icon and the checkmark icon.
      $(this).removeClass('unselected');
      $(this).addClass('selected');

      //This takes the information we previously captured about the program and activity that was selected, and assigns it within the object we created earlier.
      selected_activity["Program"] = program;
      selected_activity["Activity"] = activity;

      //Using the relative url we created earlier as the index, we can push this new activity into the full list of selected activities.
      selected_activities[selected_activity_url] = selected_activity

      //Assign this new object, with the old and new activities, with the session.  This allows the interface to remember which activities are selected between menus and refreshes.
      setSessionObject("selected_activities", selected_activities)

      //Get information about the activity that was selected.
      var activity_details = loadActivityDetails(program, activity)

      //Pull the relevant information about the activity type.
      var type = activity_details.Activity["Type"];
      var type_details = activity_types[type]

      //Pull the relevant information about the audience.
      var audience = activity_details.Activity["Audience"];
      var audience_details = audiences[audience]

      //Process the descriptions as rich text.
      var type_description = BBCodeParser.process(type_details["Description"]);
      var audience_description = BBCodeParser.process(audience_details["Description"]);

      //Add this activity to the Exhibit menu list.
      $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + selected_activity_url + '" data-program="' + program + '" data-activity="' + activity + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help" data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');

      //Make sure the Exhibit menu activities container knows if it's empty and start the scrollbar.
      if ($('#desktop .setup_container').hasClass('empty') == true) {
        $('#desktop .setup_container').removeClass('empty')
        $('#desktop .setup_container').addClass('populated')

        initializeContainerScrollBar("#activities",true,false,true)
      }

      //Alert the user that an activity was added.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Added <i>' + activity_details.Activity.Info["Title"] + '</i> to Exhibit Mode.', 'success', 2, function() {});

    } 

    //If this activity was already selected, do this.
    else if ($(this).hasClass('selected')) {

      //Switch between the unselected and selected class.  This toggles between the plus icon and the checkmark icon.
      $(this).removeClass('selected');
      $(this).addClass('unselected')

      //Delete this activity from the object of selected activities.
      delete selected_activities[selected_activity_url]

      //Update the session object containing the selected activities.
      setSessionObject("selected_activities", selected_activities)
      
      //Find this activity on the Exhibit menu selected activities list and remove it.
      $('#activities .selected_activities').find("[data-url='" + selected_activity_url + "']").remove()

      //IF there are no more activities listed in the Exhibit menu, then change the container from populated to empty and remove the scrollbar.
      if ($('#activities .selected_activities').find('.activity').length == 0) {
        $('#desktop .setup_container').removeClass('populated')
        $('#desktop .setup_container').addClass('empty')
        destroyContainerScrollBar("#activities")
            $("#exhibit_presets").val("default").trigger('change');

      }

      //Get details about the activity.
      var activity_details = loadActivityDetails(program, activity)

      //Alert the user that they've removed the activity.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Removed <i>' + activity_details.Activity.Info["Title"] + '</i> from Exhibit Mode.', 'error', 2, function() {});

    }

  });

});