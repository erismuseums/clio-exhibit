// Activities.js
// Used to configure, load and interact with all of the individual activies.

/* ==========================================================================
   Activity Events
   ========================================================================== */

$(document).ready(function() {

	//This library is used to keep track of time idle on an activity while in Exhibit Mode.
	//After a configurable amount of idle time while in exhibit mode, the activity will close and return the interface to the homescreen.
	TimeMe.initialize({
		currentPageName: "Activity", // current page
	});

	/* ==========================================================================
       General Activity
       ========================================================================== */

    $(document).on("click", "#activityinfo_details", function(){
    	

    	
    	if($("#activityinfo_container").hasClass("video")){
    		
    		$("#activityinfo_container").removeClass("video")
    		$("#activityinfo_container").addClass("details")
	    	$("#activityinfo_details").html("Details");

    	} else if($("#activityinfo_container").hasClass("details")){
    		

    		$("#activityinfo_container").removeClass("details")
    		$("#activityinfo_container").addClass("video")
	    	$("#activityinfo_details").html("Intro");

    	}
    	
    })

    //Activity information icon that is shown in the menu bar while in Exhibit mode.
    $(document).on("click", "#activity_info", function() {

		var url = getCurrentURL();
		var program = $("#activity_info").attr("data-program");
		var title = $("#activity_info").attr("data-title");
		var description = $("#activity_info").attr("data-description");
		var video_file = $("#activity_info").attr("data-video");
		var video_container = "";

		var subtitles = $("#activity_info").attr("data-subtitles");
		if( subtitles != "" && subtitles != undefined){
			subtitles = JSON.parse(subtitles)
		}
		var subtitles_container = [];
		var subtitles_join = ""; 

		if(video_file != "" && video_file != undefined){

			if( subtitles != "" && subtitles != undefined){
				$.each(subtitles,function(index,val){
					var file = "content/" + program + "/@Media/" + val["File"];
					var subtitle_wrapper = '<track label="' + val["Title"] + '" kind="subtitles" srclang="' + val["Language"] + '" src="' + file + '" default="">'
					
					subtitles_container.push(subtitle_wrapper)
				})	

				subtitles_join = subtitles_container.join("");
			}

			
    		var video_wrapper = '<source src="' + video_file + '" type="video/ogg"></source>';
    		var video_container = '<video id="activity_intro" class="video-js" data-setup=\'{"playbackRates": [0.5, 1, 1.5, 2], "fluid": true, "muted": false, "loop": false,  "controls": true, "autoplay": false, "preload": "auto"}\'>' + video_wrapper + "" + subtitles_join + '</video>';
    		console.log(video_container)
    		


    		var options = {
		      "Header":{
		        "Style":"default",
		        "Text":title
		      },
		      "Scrollbar":"false",
		      "Interaction":{
		        "Type":"custom",
		        "HTML":'<button class="lookcloser_info" id="activityinfo_details">Details</button><button class="lookcloser_acknowledge" id="activityinfo_close">Okay</button>',
		        "Prefix":"activityinfo_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div id="activityinfo_container" class="video"><div id="activity_video" style="display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + video_container + '</div><div id="activity_details" style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div></div>'
		      }
		    };

		} else {

			var options = {
		      "Header":{
		        "Style":"default",
		        "Text":title
		      },
		      "Scrollbar":"auto",
		      "Interaction":{
		        "Type":"custom",
		        "HTML":'<button class="lookcloser_acknowledge" id="activityinfo_close">Okay</button>',
		        "Prefix":"activityinfo_"
		      },
		      "Content":{
		        "Type":"html",
		        "Data":'<div id="activityinfo_container" class="details"><div id="activity_details" style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div></div>'
		      }
		    };

		}

		

		
		
		//Create Popup 
	    createLookCloserPopUp(options);
	    if(video_container != ""){
	    	videojs('activity_intro');
	    }
	    
	})

    //Carousel - Next button.
	$(document).on("click", ".owl-next-button", function() {
		$('.owl-carousel').trigger('next.owl.carousel');
	});

	$(document).on("click", '#activity_accessibility .brightness_control', function() {
    console.log($(this).attr("id"))
    var facilitator_brightness = $(this).attr("id").replace("user_brightness","")
    
    setScreenBrightness(facilitator_brightness)


    $('#activity_accessibility .brightness_control').each(function(index, val) {

      if (index < facilitator_brightness) {
        $(this).removeClass('unselected')
        $(this).addClass('selected')
      } else {
        $(this).removeClass('selected')
        $(this).addClass('unselected')
      }

    })
  })

  $(document).on("click", '#reset_user_accessibility', function() {

    clearAccessibility();
    
    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Accessibility options have been set to their defaults.', 'error', 2, function() {});

  })



  //Contrast Mode - Setting to change the default theme that is used for the entire interface.
  $(document).on("change", '#user__contrast_mode', function() {

    //Get the user selected theme from the dropdown.
    var setting__contrast_mode = $('#user__contrast_mode').val()

    //Get the currently loaded theme.
    var setting__contrast_override = getLocal("setting__contrast_override", "disabled")

    //Set the accessibility option in sessionstorage
    setAccessibility("setting__contrast_mode", setting__contrast_mode)

    //If both the Settings Theme and Accessibility Contrast mode are disabled, remove all themes.
    if (setting__contrast_override == "disabled" && setting__contrast_mode == "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')

    } 

    //If Setting Theme is disabled but Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override == "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is disabled, use the Theme setting.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode == "default") {
      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_override + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_override);

    } 

    //If Setting Theme is enabled and Accessibility Contrast mode is enabled, use the Accessibility contrast mode.
    else if (setting__contrast_override != "disabled" && setting__contrast_mode != "default") {

      removeBodyClassByPrefix('accessibility_contrast_mode_')
      $('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
      $('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);

    }


  })

  //Font Size - Options to change the font size.
  $(document).on("change", '#user__font_size', function() {

    var setting__font_size = $('#user__font_size').val()
    setAccessibility("setting__font_size", setting__font_size)

    removeBodyClassByPrefix('accessibility_font_size_')
    $('body').addClass("accessibility_font_size_" + setting__font_size );
   
  })

  //Font Face - Options to override the default font family.
  $(document).on("change", '#user__font_face', function() {

    var setting__font_face = $('#user__font_face').val()
    setAccessibility("setting__font_face", setting__font_face)

    removeBodyClassByPrefix('accessibility_font_face_')
    $('body').addClass("accessibility_font_face_" + setting__font_face);

  })

  //Paragraph Alignment - Options to change the paragraph alignment
  $(document).on("change", '#user__paragraph_alignment', function() {

    var setting__paragraph_alignment = $('#user__paragraph_alignment').val()
    setAccessibility("setting__paragraph_alignment", setting__paragraph_alignment)

    removeBodyClassByPrefix('accessibility_paragraph_alignment_')
    $('body').addClass("accessibility_paragraph_alignment_" + setting__paragraph_alignment);
    
  })

  //Paragraph Spacing - Option to change the spacing between the lines of each paragraph.
  $(document).on("change", '#user__paragraph_spacing', function() {

    var setting__paragraph_spacing = $('#user__paragraph_spacing').val()
    setAccessibility("setting__paragraph_spacing", setting__paragraph_spacing)

    removeBodyClassByPrefix('accessibility_paragraph_spacing_')
    $('body').addClass("accessibility_paragraph_spacing_" + setting__paragraph_spacing);

  })

	
	
})

/* ==========================================================================
    General Activity Functions
    ========================================================================== */


function loadActivity(program, activity) {

	var url = getCurrentURL();
	var menu_section = {};

	setURLParameters(url["mode"],menu_section, program, activity)
	

	var current_activity_url = "content/" + program + "/@Activities/" + activity + ".json"
	var selected_activities = getSessionObject("selected_activities", "")

	if (selected_activities.hasOwnProperty(current_activity_url)) {
		$('#facilitator_add').removeClass('unselected')
		$('#facilitator_add').addClass('selected')
	} else {
		$('#facilitator_add').removeClass('selected')
		$('#facilitator_add').addClass('unselected')
	}
	collapseMenu();

	$('#desktop').hide()
	$('#exhibit').hide()
	$('#content').show()
	$('#activity_content').html('')
	
	
	$('#menu').removeClass('minimal')

	var activity_details = loadActivityDetails(program, activity);

	var acknowledgements = JSON.stringify(activity_details.Activity["Acknowledgements"]);
	if(acknowledgements == "" || acknowledgements == undefined){
		$('#activity_menu_acknowledgement').hide();
	} else {
		$('#activity_menu_acknowledgement').show();
		$('#activity_menu_acknowledgement').attr("data-acknowlegements",acknowledgements);
	}

	var attributions = JSON.stringify(activity_details.Activity["Attributions"]);
	if(attributions == "" || attributions == undefined){
		$('#activity_menu_attribution').hide();
	} else {
		$('#activity_menu_attribution').show();
		$('#activity_menu_attribution').attr("data-attributions",attributions)
	}

	var description = activity_details.Activity.Info["Description"];
	if(description == "" || description == undefined){
		$('#activity_menu_information').hide();
	} else {
		$('#activity_menu_information').show();
		$('#activity_menu_information').attr("data-description", description)
	}

	var video = activity_details.Activity.Info["Video"]
	if(video == "" || video == undefined){
		$('#activity_menu_introduction').hide();
	} else {
		$('#activity_menu_introduction').show();
		$('#activity_menu_introduction').attr("data-video", './content/'+ program + '/@Media/' + video)
		$('#activity_menu_introduction').attr("data-poster", './content/'+ program + '/@Media/' + activity_details.Activity.Info["Poster"])
	}


	

	

	var type = activity_details.Activity["Type"];

	var file = "config/@ActivityTypes/" + type + "/@About.json";
	var activitytype_content = parseJSONContent(file);
	console.log(activitytype_content)
	var activitytype_help = activitytype_content.Configuration["Help"];

	if( activitytype_help == "" || activitytype_help == undefined){
		$('#activity_menu_help').hide();
	} else {
		$('#activity_menu_help').show();
		$('#activity_menu_help').attr("data-help", activitytype_help)
	}

	$('#header .title_bar .title span').html(activity_details.Activity.Info["Title"])
	

	
	var subtitles = activity_details.Activity.Info["Subtitles"];


	

		

	if (activity_details.Activity.Configuration["Subheader"] == "false") {
		$('#content_subheader').hide()
	} else {
		$('#content_subheader').html(activity_details.Activity.Info["Description"])
	}

	if (activity_details.Activity.Configuration["Accessibility"] == "false") {
		$('body').addClass('hide_accessibility')
	}

	if (activity_details.Activity.Configuration["Theme"]) {
		removeBodyClassByPrefix('theme_')
		var theme = activity_details.Activity.Configuration["Theme"]
		applyTheme(theme)
	}

	$('body').addClass('type_' + type)

	if ($("body").hasClass("user") == true) {
		var activity_timeout = getLocal("setting__activity_timeout", "60");
		TimeMe.setIdleDurationInSeconds(activity_timeout);

		TimeMe.callWhenUserLeaves(function() {
			returnHome();
		}, 1);
	}

	var type = activity_details.Activity["Type"]

	var content = activity_details.Content
	loadActivityType(program, type, content)

}

function loadActivityType(program, type, content) {

	var url = getCurrentURL();

	//Load the program details.
	var program_details = loadProgramDetails(program)

	var loaded_types = $("#activity_content").attr("data-types")
	
	var types_JSON = {}
	
	if (loaded_types != "" && loaded_types != undefined){
		types_JSON = JSON.parse(loaded_types);
	} 

	//Get Information about the Activity Types.
	var file = "config/@ActivityTypes/" + type + "/@About.json";
	var activitytype_content = parseJSONContent(file);

	var initScript = activitytype_content.Initialize["Script"];
	var initFunction = activitytype_content.Initialize["Function"];

	if(!types_JSON.hasOwnProperty(type)){
		//Get the CSS to add.
		var stylesheet_directory = "../config/@ActivityTypes/" + type + "/@Stylesheets/";
		var available_stylesheets = parseDirectory(stylesheet_directory);

		$.each(available_stylesheets,function(index,val){
			//Apply any custom stylesheets.
	    	$('head').append('<link rel="stylesheet" href="' + url['base'] + "config/@ActivityTypes/" + type + "/@Stylesheets/" + val + '" type="text/css" />')
		})

	} else {
		console.log("Already loaded styles.")
	}
		
	//Creates the HTML template to load.
	var template_file = url['base'] + "config/@ActivityTypes/" + type + "/@Templates/" + activitytype_content.Initialize["Template"] + ".html";

	//Loads the HTML file into a variable.
	var activity_template = loadHTML(template_file);

	var container = "#activity_content";
	
	//Append the template to the activity container.
	$(container).append(activity_template)

	if(!types_JSON.hasOwnProperty(type)){ 
		//Get the JS libraries to add.
		var library_directory = "../config/@ActivityTypes/" + type + "/@Libraries/";
		var available_libraries = parseDirectory(library_directory);
		
		$.each(available_libraries,function(index,val){
			//Apply any custom JavaScript.
	    	
	    	var script = url['base'] + "config/@ActivityTypes/" + type + "/@Libraries/" + val;

			$.getScript(script, function() {
			    console.log(script + " script loaded and executed.");

			});


		})

		$.getScript(url['base'] + "config/@ActivityTypes/" + type + "/" + initScript, function() {
		    setTimeout(
			  function() 
			  {
			    window[initFunction](program, content);
		    	// here you can use anything you defined in the loaded script
			  }, 500
			)
		    
		});

	} else {

		$.getScript(url['base'] + "config/@ActivityTypes/" + type + "/" + initScript, function() {
		    setTimeout(
			  function() 
			  {
			    window[initFunction](program, content);
		    	// here you can use anything you defined in the loaded script
			  }, 500
			)
		    
		});
		
	}
		
	types_JSON[type] = { "loaded":"true"}
	var types_encode = JSON.stringify(types_JSON);

	$("#activity_content").attr("data-types",types_encode)
	
}

function loadActivityMainMenu(){
	$("#activity_menu .activity_menu_header").html("Activity Menu");
	$('#activity_menu_back').addClass('closed')

	$('#activity_menu_list').removeClass('closed')
	$('#activity_menu_list').addClass('open')

	$('#activity_menu_subcontent').removeClass('open')
	$('#activity_menu_subcontent').addClass('closed')

	if(videojs.getPlayers()["activity_menu_intro"]) {
	    delete videojs.getPlayers()["activity_menu_intro"];
	}
	$("#activity_video").html("");
	$('#activity_menu_subcontent').html("")
	initializeContainerScrollBar("#activity_menu_list",false,false,true);
	console.log('done')

}

function loadActivitySubMenu(section){
	var url = getCurrentURL();
	
	$('#activity_menu_list').removeClass('open')
	$('#activity_menu_list').addClass('closed')

	$('#activity_menu_subcontent').removeClass('closed')
	$('#activity_menu_subcontent').addClass('open')

	var menu_content = loadHTML('./config/@ActivityMenu/' + section + '.html');
	$('#activity_menu_subcontent').html(menu_content)
	console.log(section)

	switch(section){
      case "accessibility":
      	
      	if (url["mode"] == "kiosk") {

					$("#activity_brightness").show()
					var device_brightness = getLocal("device_brightness", 4)

					$('#activity_brightness .brightness_control').each(function(index, val) {

						if (index < device_brightness) {
							$(this).removeClass('unselected')
							$(this).addClass('selected')
						} else {
							$(this).removeClass('selected')
							$(this).addClass('unselected')
						}

					})
				} else {
					$("#activity_brightness").hide()
				}


				$('#user__contrast_mode').val(getAccessibility("setting__contrast_mode", "default"))
				$('#user__font_face').val(getAccessibility("setting__font_face", "default"))
				$('#user__font_size').val(getAccessibility("setting__font_size", "default"))
				$('#user__paragraph_alignment').val(getAccessibility("setting__paragraph_alignment", "left"))
				$('#user__paragraph_spacing').val(getAccessibility("setting__paragraph_spacing", "default"))

      	$('.dropdown').select2({
					minimumResultsForSearch: Infinity,
					width: '400px'
				})
      	initializeContainerScrollBar("#activity_accessibility",true,false,true)

      	

        break;

      case "introduction":
      	var video = $("#activity_menu_introduction").attr("data-video");
      	var poster = $("#activity_menu_introduction").attr("data-poster");
      	      	
      	$('#activity_video').append('<video id="activity_menu_intro" class="video-js"  poster="' + poster + '" data-setup=\'{"playbackRates": [0.5, 1, 1.5, 2], "fluid": false, "muted": false, "loop": false,  "controls": true, "autoplay": false, "preload": "auto"}\'><track label="" kind="subtitles" srclang="" src="" default=""><source src="' + video + '" type="video/mp4"></source></video>')


      	if(video != ""){
		    	videojs('activity_menu_intro');
		    }
        break;

      case "information":
      	var description = $("#activity_menu_information").attr("data-description");
      	$("#activity_menu_description .description_wrapper").html(description)
      	initializeContainerScrollBar("#activity_menu_description",true,false,true)
        break;

      case "acknowledgements":
      	var acknowledgements = JSON.parse($('#activity_menu_acknowledgement').attr("data-acknowlegements"))
      	$.each(acknowledgements,function(index,value){
      		$('#acknowledgement_list .acknowledgement_container').append('<div class="acknowledgement">' + value + '</div>')
      	})
      	initializeContainerScrollBar("#acknowledgement_list",true,false,true)
        break;

      case "attribution":
      	var attributions = JSON.parse($('#activity_menu_attribution').attr("data-attributions"))
      	$.each(attributions,function(index,value){
      		$('#attribution_list .attribution_container').append('<div class="attribution">' + value + '</div>')
      	})
      	initializeContainerScrollBar("#attribution_list",true,false,true)
        break;

      case "help":
      	var help = $('#activity_menu_help').attr("data-help")
      	$("#activity_help_content .description_wrapper").html(help)
      	initializeContainerScrollBar("#activity_help_content",true,false,true)
        break;

    }
}