//Homescreen.js
//Main page load scripts for the CLIO Web application.
//Configures the homescreen based on URL parameters.
$(document).ready(function() {


  /* ==========================================================================
   Facilitator Mode
   ========================================================================== */

  //When there are no URL parameters set, the web application will default to a full Facilitator Mode experience.
  //This allows a faciliator to select activities before starting Exhibit Mode.

  //Get the last used brightness setting for the display and set the device brightness to this.
  var device_brightness = getLocal("device_brightness", "4")
  $.ajax({
    url: "command/brightness/" + device_brightness + ".php",
    success: function() {
      console.log('Set brightness to ' + device_brightness)
    }
  });

  //Create current URL
  var url = getCurrentURL();

  //Get the last used program, or load the default program.  Get details about the program.
  var default_program = getSetting("setting__default_program", "Default");
  var program_details = loadProgramDetails(default_program)

console.log(loadDiscussionDetails('Default','1_Hardware'));

  //Configure for kiosk
  if(url["kiosk"] != "true"){
    $("#facilitation_button").hide();
  }

  //Set the default theme.
  $('head').append('<link rel="stylesheet" href="./css/themes/theme_default.css" type="text/css" />');
  $('body').addClass("theme_default");

  //If there is a branding CSS file present within the Program, apply it.
  var exhibit_branding = doesFileExist(url['base'] + "content/" + default_program + "/branding.css")
  if (exhibit_branding == true) {
    $('head').append('<link rel="stylesheet" href="' + url['base'] + 'content/' + default_program + '/branding.css" type="text/css" />');
  }

  //Set the title of the Program for Facilitator Mode.
  $('#setup_header').html(program_details['Program'])

  //Get the audiences used by this program, and the activity types available to CLIO.
  var audiences = getAudiences(default_program);
  var activity_types = getActivityTypes();

  //See if there are any activities that have already been selected during this session.
  var selected_activities = getSessionObject("selected_activities", "")

  //If there are previously selected activities, do this.
  if (!jQuery.isEmptyObject(selected_activities)) {

    //Go through each activity and add it to the Facilitator Mode Exhibit menu.
    $.each(selected_activities, function(index, val) {

      //Get details for the activity.
      var activity_details = loadActivityDetails(val["Program"], val["Activity"])

      //Get information about the activity type.
      var type_details = activity_types[activity_details.Activity["Type"]]

      //Get information about the audience.
      var audience_details = audiences[activity_details.Activity["Audience"]]

      //Process the activity type and audience descriptions as rich text.
      var type_description = BBCodeParser.process(type_details["Description"]);
      var audience_description = BBCodeParser.process(audience_details["Description"]);

      //Add a button for this activity to the Exhibit menu list of currently selected activities.
      $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '" data-program="' + val["Program"] + '" data-activity="' + val["Activity"] + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help"  data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');
    });

    //Change the Exhibit Menu list of selected activities from empty to populated.
    $('#desktop .setup_container').removeClass('empty')
    $('#desktop .setup_container').addClass('populated')

    //Initialize the scroll bar on the Exhibit Menu list of selected activities.
    initializeScrollBar("exhibit")
  }

  //Check if there is a menu parameter, then opens that menu.
  if (url['menu']) {

    //Split the menu url parameter by slash.
    var menu_navigation = url['menu'].split('/');

    //Create an array of menu navigation.
    var menu_section = {};
    menu_section['menu'] = menu_navigation[0];
    menu_section['tier1'] = menu_navigation[1];
    menu_section['tier2'] = menu_navigation[2];
    menu_section['tier3'] = menu_navigation[3];

    //Load the menu section, then display it.
    loadMenuSection(menu_section)
    openMenuSection(menu_section)

  } 

  //Check if there is a program and activity specified, and then open it.
  if (url['program'] != undefined && url['activity'] != undefined) {   

    //If 'standalone' is set to true, configure the interface to show an information button instead of a home button.
    if (url["standalone"] == "true") {
      $('body').addClass('standalone')
      setLocal("setting__default_program", url['program'])
    }

    //Load the activity.
    loadActivity(url['program'], url['activity'])
   
    //Change from the homescreen to the activity screen.
    $('body').removeClass('homescreen');
    $('body').addClass('activity');
  }

  /* ==========================================================================
     Embedded Exhibit Mode
     ========================================================================== */

  //If 'embedded' is set to true and a 'program' is defined, automatically start in exhibit mode.
  //This will use all of the Activities in that Program that are not marked as 'hidden' in their configuration files.
  if (url["embedded"] == "true" && url['program'] != undefined) {
    
    //Add the embedded class to the body
    $('body').addClass('embedded');
    
    //Set the default program as the one defined within the URL.
    setLocal("setting__default_program", url['program'])

    //Create the object for the activities we want to use in Exhibit Mode.
    var selected_activities = {};
    
    //Request a list of the activities in the prrogram.
    var activities = requestActivities(url['program']);

    //Apply any custom branding used within the Program's CSS file.
    $('head').append('<link rel="stylesheet" href="' + url['base'] + 'content/' + url['program'] + '/branding.css" type="text/css" />')

    //Change from Facilitator Mode to User Mode.
    $('body').removeClass('facilitator');
    $('body').addClass('user')

    //Set the title of the Exhibit Mode homescreen.
    var program_details = loadProgramDetails(url['program'])
    $("#exhibit .exhibit_header").html(program_details["Program"])

    //Create the container for the activity carousel.
    $("#exhibit").append('<div class="owl-carousel owl-theme"></div>');

    //For each activity, load it's details and add it to the carousel if it is not marked as hidden.
    $.each(activities, function(index, val) {

      var activity_details = loadActivityDetails(url["program"], val)

      var program = url["program"];
      var activity = val;

      if (activity_details.Activity.Configuration['Hidden'] == "false") {
        var selected_activity = {};
        var selected_activity_url = "content/" + program + "/@Activities/" + activity + ".json"

        selected_activity["Program"] = program;
        selected_activity["Activity"] = activity;

        selected_activities[selected_activity_url] = selected_activity

        var preview_url = "content/" + program + "/@Media/" + activity_details.Activity.Info["Preview"]
        $("#exhibit .owl-carousel").append('<div class="activity_card activity_button" data-program="' + val["Program"] + '" data-audience="' + activity_details.Activity["Audience"] + '" data-activity="' + val["Activity"] + '"><div style="background-image: url(' + preview_url + ')" class="image closed"><div class="overlay"><span>' + activity_details.Activity.Info["Description"] + '</span></div></div><div class="details"><div class="title">' + activity_details.Activity.Info['Title'] + '</div><div class="info"></div></div></div>');
      
      }

    })

    //Start the Exhibit Mode homescreen carousel.
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 0,
      nav: true,
      dots: true,
      items: 1,
      autoplay: true,
      autoplayTimeout: 10000,
      onChange: function() {
        $('.owl-carousel').trigger('stop.owl.autoplay');

        $('.activity_card .image').removeClass('open');
        $('.activity_card .image').addClass('closed');
        $('.owl-carousel').trigger('play.owl.autoplay');
      }
    })
  }

  /* ==========================================================================
     Facilitator Mode Events
     ========================================================================== */

  //Exhibit menu button to load the activities that were used the last time the kiosk entered Exhibit Mode..
  $('#reload_exhibit').click(function() {

    //Get the activities used the last time CLIO entered exhibit mode.
    var selected_activities = getLocalObject("selected_activities", "")
    
    //Get the program that was used the last time CLIO entered exhibit mode.
    var program = getSetting("setting__default_program", "Default");
   
    //Get information about that programs audiences and the activity types available to CLIO.
    var audiences = getAudiences(program);
    var activity_types = getActivityTypes();

    //If there were previously selected activities, do this.
    if (selected_activities != "") {
      
      //Clear any activities that are loaded in the Exhibit menu.
      $("#activities .selected_activities").html("")
      
      //For each activity, we need to add a button to the Exhibit menu list of selected activities.
      $.each(selected_activities, function(index, val) {

        //Get details about that activity.
        var activity_details = loadActivityDetails(val["Program"], val["Activity"])

        //Get more information about the activity type.
        var type_details = activity_types[activity_details.Activity["Type"]]

        //Get more information about the audience.
        var audience_details = audiences[activity_details.Activity["Audience"]]

        //Process the audience and activity type descriptions as rich text.
        var type_description = BBCodeParser.process(type_details["Description"]);
        var audience_description = BBCodeParser.process(audience_details["Description"]);

        //Add the activity button to the Exhibit menu list of selected activities.
        $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '" data-box="' + val["Program"] + '" data-activity="' + val["Activity"] + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type"  data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help" data-help_type="Audience"  data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');

      });

      //Change the Exhibit Menu list of selected activities from empty to populated.
      $('#desktop .setup_container').removeClass('empty')
      $('#desktop .setup_container').addClass('populated')
      
      //Set this session's selected activities to the previously  selected activities.
      setSessionObject("selected_activities", selected_activities)
      
      //Initialize the scroll bar on the Exhibit Menu list of selected activities.
      initializeScrollBar("exhibit")

      //Notify the user that they were loaded.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Reloaded Activities from Previous Exhibit.', 'success', 2, function() {});
    } 

    //If there were no previously selected activities, change the warning text.
    else {
      
      //Notify the user that they couldn't be loaded.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('There were no activities to resume.', 'error', 2, function() {});

    }

  })

  //Exhibit menu button to clear all of the selected activities.
  $('#clear_exhibit').click(function() {
    
    //Create an empty object and set the selected_activities session object.
    var selected_activities = {};
    setSessionObject("selected_activities", selected_activities)

    //Empty the Exhibit menu list of selected activities.
    $('#activities .selected_activities').html("")

    //Change the Exhibit Menu list of selected activities from populated to empty.
    $('#desktop .setup_container').addClass('empty')
    $('#desktop .setup_container').removeClass('populated')
    
    //Destroy the scroll bar for the Exhibit menu list.
    destroyScrollBar("exhibit")

    //Alert the user with a pop up notification.
    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Cleared Activities from Current Exhibit.', 'success', 2, function() {});
  })

  //Exhibit menu button to enter Exhibit Mode.  
  //When the user first clicks 'Enter Exhibit Mode', they will be prompted that they will need to restart the kiosk to return to Facilitator Mode.
  $('#exhibit_disclaimer').click(function() {
    if ($('#desktop .setup_container').hasClass('populated')) {
      
      //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Exhibit Mode"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="exhibit_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_exhibit">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"exhibit_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to enter Exhibit Mode?</p><p><i>Returning to Facilitator Mode will require a restart of the kiosk.</i></p></div>'
        }
      };
      createLookCloserPopUp(options);
      
    }
  })

  //When the user selects 'Confirm' on the disclaimer pop-up, this will run.
  $(document).on("click", '#confirm_exhibit', function() {
    
    //Close the current lightbox window.
    $.fancybox.destroy();

    //Reset any accessibility options that may have been set by the facilitator.
    clearAccessibility();

    //Get the program that is loaded and get more information about it.
    var default_program = getSetting("setting__default_program", "Default");
    
    //Set the title of the Exhibit Mode homescreen.
    var program_details = loadProgramDetails(default_program)
    $("#exhibit .exhibit_header").html(program_details["Program"])

    //Get the list of activities selected for Exhibit Mode.
    var selected_activities = getSessionObject("selected_activities", "")
    
    //Get the audiences configured in this program and the activity types available to CLIO.  
    var audiences = getAudiences(default_program);
    var activity_types = getActivityTypes();

    //Use the activities from the current session to set the local object.
    //This allows the activities to be resumed when a user opens the CLIO interface.
    setLocalObject("selected_activities", selected_activities)

    //Change from Facilitator Mode to Exhibit Mode.
    $('body').removeClass('facilitator');
    $('body').addClass('user')

    //Create the container for the Exhibit Mode activity carousel.
    $("#exhibit").append('<div class="owl-carousel owl-theme"></div>');

    //For each activity, get information about it and add a card to the carousel.
    $.each(selected_activities, function(index, val) {

      var activity_details = loadActivityDetails(val["Program"], val["Activity"])

      var preview_url = "content/" + default_program + "/@Media/" + activity_details.Activity.Info["Preview"]

      $("#exhibit .owl-carousel").append('<div class="activity_card activity_button" data-program="' + val["Program"] + '" data-audience="' + activity_details.Activity["Audience"] + '" data-activity="' + val["Activity"] + '"><div style="background-image: url(' + preview_url + ')" class="image closed"><div class="overlay"><span>' + activity_details.Activity.Info["Description"] + '</span></div><div class="touch heartbeat"></div></div><div class="details"><div class="title">' + activity_details.Activity.Info['Title'] + '</div><div class="info"></div></div></div>');

    });

    //Get the user setting for Slide duration, or use the default.
    var slide_duration = getSetting("setting__slide_duration", "10000");

    //Start the Exhibit Mode activity carousel.
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 0,
      nav: true,
      dots: true,
      items: 1,
      autoplay: true,
      autoplayTimeout: slide_duration,
      onChange: function(event) {

        //When the slide changes, close the description if it is open.
        $('.owl-carousel').trigger('stop.owl.autoplay');
        $('.activity_card .image').removeClass('open');
        $('.activity_card .image').addClass('closed');
        $('.owl-carousel').trigger('play.owl.autoplay');

      }
    })

  })

  /* ==========================================================================
    Exhibit Mode Events
    ========================================================================== */
  
  //Load the activity when a user clicks on an Exhibit Mode card.
  $(document).on("click", '.activity_card', function() {

    var url = getCurrentURL();

    var program = $(this).data("program");
    var activity = $(this).data("activity");

    loadActivity(program, activity);

    $('body').removeClass('homescreen');
    $('body').addClass('activity');

  })

  //Show the activity description when a user clicks the info button.
  $(document).on("click", '.activity_card .info', function(e) {
    
    $('.owl-carousel').trigger('stop.owl.autoplay');
    $(this).parents('.activity_card').find('.image').toggleClass('open closed')
    $('.owl-carousel').trigger('play.owl.autoplay');

    e.stopPropagation();
    
  });

});