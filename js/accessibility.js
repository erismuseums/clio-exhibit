// Accessibility.js
// Loads on every page to apply accessibility options, as set through the accessibility menu.

$(document).ready(function(){
	var url = getCurrentURL();

	//Get accessibility settings.
	var setting__font_size = getAccessibility("setting__font_size","default");
	var setting__font_face = getAccessibility("setting__font_face","default");
	var setting__paragraph_alignment = getAccessibility("setting__paragraph_alignment","left");
	var setting__paragraph_spacing = getAccessibility("setting__paragraph_spacing","default");
	var setting__contrast_mode = getAccessibility("setting__contrast_mode","default");

	var setting__contrast_override = getLocal("setting__contrast_override","disabled");

	//Remove all possible font size options before adding class.
	removeBodyClassByPrefix('accessibility_font_size_');
	$('body').addClass("accessibility_font_size_" + setting__font_size);

	//Remove all possible font face options before adding class.
	removeBodyClassByPrefix('accessibility_font_face_');
	$('body').addClass("accessibility_font_face_" + setting__font_face);

	//Remove all possible paragraph alignment options before adding class.
	removeBodyClassByPrefix('accessibility_paragraph_alignment_');
	$('body').addClass("accessibility_paragraph_alignment_" + setting__paragraph_alignment);

	//Remove all possible paragraph spacing options before adding class.
	removeBodyClassByPrefix('accessibility_paragraph_spacing_');
	$('body').addClass("accessibility_paragraph_spacing_" + setting__paragraph_spacing);

	//Checks if contrast mode is enabled.
	if (setting__contrast_mode != "default"){
		
		$('head').append('<link rel="stylesheet" href="' + url["base"] + '/css/accessibility/accessibility_contrast_mode_' + setting__contrast_mode + '.css" type="text/css" />');
		$('body').addClass("accessibility_contrast_mode_" + setting__contrast_mode);
	} else if (setting__contrast_override != "disabled"){
		$('head').append('<link rel="stylesheet" href="' + url["base"] + '/css/accessibility/accessibility_contrast_mode_' + setting__contrast_override + '.css" type="text/css" />');
		$('body').addClass("accessibility_contrast_mode_" + setting__contrast_override);
	}


})