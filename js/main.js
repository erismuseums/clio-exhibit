// Main.js
// This file contains all custom functions.

$(document).ready(function() {

	$(document).on("click", ".lookcloser_acknowledge", function() {
		destroyLookCloserPopUp();
	});

	$(document).on("click", ".lookcloser_deny", function() {
		destroyLookCloserPopUp();
	});

});

/* ==========================================================================
   URL Functions
   ========================================================================== */

//Returns an array with various information about the current URL.
function getCurrentURL(param) {

	//Constructs a full URL.
	var self = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
	var parent = window.parent.location.protocol + "//" + window.parent.location.host + "/" + window.parent.location.pathname + window.parent.location.search;
	var hash = window.location.hash;

	var url = "";

	//Check if param is set to "self" or "parent".
	//Useful for getting the URL of the parent window while in a menu frame.
	if (param == undefined || param == "self") {
		url = self;
	} else if (param == "parent") {
		url = parent;
	}


	var getUrl = window.location;
	var baseURL = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname;

	//Intializt the return array.
	var urlArray = {
		"url": url,
		"base": baseURL,
		"hash": hash
	};

	//Creates an array with any parameters that are found in the URL
	var request = {};
	var pairs = url.substring(url.indexOf('?') + 1).split('&');
	for (var i = 0; i < pairs.length; i++) {
		if (!pairs[i])
			continue;
		var pair = pairs[i].split('=');
		request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	}

	//Combines the request array with urlArray.
	$.each(request, function(index, value) {
		urlArray[index] = value;
	})


	return urlArray;
}

//This function manages the URL parameters that are used in the interface.  
//It updates the URL to reflect the current location of the user without reloading the page.
function setURLParameters(mode, menu_section, program, activity, view) {

	var url = getCurrentURL();

	var new_params = {};
	var menu_query = [];

	//Check if we are updating the program in the url.
	if (program != "" && program != undefined) {
		new_params['program'] = program;

	}

	//Check if we are updating the activity in the url.
	if (activity != "" && activity != undefined) {
		new_params['activity'] = activity;
	}

	//Check if we are in development mode.
	if (url["development"] == "true") {
		new_params['development'] = "true";
	}

	//Check if there is a desired mode. 
	if (mode != "" && mode != undefined ) {
		new_params['mode'] = mode;
	} else if (url["mode"] != "") {
		new_params['mode'] = url["mode"];
	} else if (url["mode"] == ""){
		new_params['mode'] = "facilitator";
	}


	//If the view option is defined, use that one.
	if (view != "" && view != undefined) {
		new_params['view'] = view;
	}

	//If it is not defined, check to see if it is defined in the url from the user.
	else if (url["view"] != "" && url["view"] != undefined) {
		new_params['view'] = url["view"];
	}

	//Else, just don't add view.
	else {

	}


	//Check to see if the menu section is defined.
	if (menu_section != undefined && menu_section != "") {

		//If menu is defined, but tier1 is not defined, we will just set the new parameters to the menu section.  
		if (menu_section['menu'] != undefined && menu_section['tier1'] == undefined && menu_section['tier2'] == undefined && menu_section['tier3'] == undefined && menu_section['tier4'] == undefined && menu_section['tier5'] == undefined) {

			new_params['menu'] = menu_section['menu'];

		}

		//If menu and tier1 are both defined, we will merge them with a forward slash before setting the new parameters.
		else if (menu_section['menu'] != undefined && menu_section['tier1'] != undefined && menu_section['tier2'] == undefined && menu_section['tier3'] == undefined && menu_section['tier4'] == undefined && menu_section['tier5'] == undefined) {

			new_params['menu'] = menu_section['menu'] + '/' + menu_section['tier1'];

		}

	}

	//Take this new parameters and format them for use in a URL by joining each parameter with an equal sign.
	$.each(new_params, function(index, value) {
		menu_query.push(index + '=' + value);
	});

	//Join all of the parameters together using an ampersand.
	var final_query = menu_query.join('&');

	//Push the final URL to the window without reloading.
	var final_url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + final_query;
	window.history.pushState({
		path: final_url
	}, '', final_url);

}

/* ==========================================================================
   PHP Functions
   ========================================================================== */

function parseDirectory(directory) {
	var directory = './php/directory.php?directory=' + directory;
	var results = [];
	$.ajax({
		url: directory,
		async: false,
		success: function(data) {

			results = JSON.parse(data);

		}
	});
	return results;
}

/* ==========================================================================
   Apache Functions
   ========================================================================== */

//Parses an Apache's directory autoindex to return an array of file paths.
//This has been replaced in favor of PHP directory parsing.
function parseApacheDirectory(directory) {
	var sub_directories = [];

	//Ajax request for Apache index.
	$.ajax({
		url: directory,
		async: false,
		success: function(data) {

			//Create a list of all programss.
			$(data).find("td > a").each(function() {
				sub_directories.push($(this).attr("href"));
			});

			//Removes parent directory.
			sub_directories.shift();
		}
	});

	return sub_directories;
}

/* ==========================================================================
   File Functions
   ========================================================================== */

function doesFileExist(urlToFile) {
	var xhr = new XMLHttpRequest();
	xhr.open('HEAD', urlToFile, false);
	xhr.send();

	if (xhr.status == "404") {
		return false;
	} else {
		return true;
	}
}

/* ==========================================================================
   Script Functions
   ========================================================================== */

function loadScript(url)
{
    document.body.appendChild(document.createElement("script")).src = url;
}

/* ==========================================================================
   DropDown Functions
   ========================================================================== */

//Settings Menu
//This function is provided an object of arrays, each array with a unique ID (eg: default) and a friendly name (eg: Getting Started).  
//This is used by the settings menu to append available Programs to the Default Program dropdown menu.
function appendToDropdownbyTitleID(id, array) {

	//Add Options to list.
	$.each(array, function(index, val) {
		$(id).append('"<option class="query" value="' + val[0] + '">' + val[1] + '</option>"');
	});
}

/* ==========================================================================
   Look Closer Pop Up Functions
   ========================================================================== */

function createLookCloserPopUp(options) {

	//Get metadata from the button that was clicked.
    var header_text = options.Header['Text'];
    var header_style = options.Header['Style'];

    var scrollbar = options['Scrollbar'];

    var interaction_type = options.Interaction['Type'];
    var interaction_prefix = options.Interaction['Prefix'];
    
    if(interaction_prefix == undefined) {
    	interaction_prefix = 'lookcloser_'
    }

    var interaction_buttons = "";
    var interaction_swipe = true;
    var interaction_close = true;
    var display_buttons = undefined;
       
    var content_type = options.Content['Type'];
    var content_data = options.Content['Data'];

    var onInit = options['onInit'];

    console.log(options)
    switch(interaction_type){
    	case "":
    	case undefined:
    	case "acknowledge":
    		interaction_buttons = '<button class="lookcloser_acknowledge" id="' + interaction_prefix + 'close">Okay</button>'
    		break;

    	case "accept_cancel":
    		interaction_buttons = '<button class="lookcloser_deny" id="' + interaction_prefix + 'deny">Cancel</button><button class="lookcloser_confirm" id="' + interaction_prefix + '_confirm">Accept</button>'
    		interaction_swipe = false;
    		interaction_close = false;
    		break;

    	case "confirm_deny":
    		interaction_buttons = '<button class="lookcloser_deny" id="' + interaction_prefix + 'deny";">Deny</button><button class="lookcloser_confirm" id="' + interaction_prefix + '_confirm">Confirm</button>'
    		interaction_swipe = false;
    		interaction_close = false;
    		break;

		case "custom":
			interaction_buttons = options.Interaction['HTML'];
    		interaction_swipe = options.Interaction['Swipe'];
    		interaction_close = options.Interaction['Close'];
    		
    		if(interaction_close == false){
    			display_buttons = [];
    		}
			break;
    }


    switch(content_type){
    	case "richtext":
    		//Open a lightbox, formatted with information from the button pressed.
		    
		    var richtext = BBCodeParser.process(content_data);

		    $.fancybox.open({
		      src: '<div class="lookcloser"><div class="response"><div class="header"><div class="header_style ' + header_style + '">' + header_text + '</div></div><div id="lookcloser_content" class="text">' + richtext + '</div><div class="interaction">' + interaction_buttons + '</div></div></div>',
		      type: 'html',
		      opts: {
		          idleTime: false,
		          animationDuration: 0,
		          toolbar: true,
		          clickSlide: interaction_swipe,
		          touch: interaction_swipe,
		          modal: true,
		          buttons: display_buttons
		        }
		    });

		    //Start the scrollbar on the popup.
		    if(scrollbar == "auto" || scrollbar == undefined){
		    	initializeContainerScrollBar("#lookcloser_content",false,false,true);
		    } else 

		    if (scrollbar == true ){
		    	initializeContainerScrollBar("#lookcloser_content",true,false,true);
		    }

		    
    		break;

		case "html":
			//Open a lightbox, formatted with information from the button pressed.
		    $.fancybox.open({
		      src: '<div class="lookcloser"><div class="response"><div class="header"><div class="header_style ' + header_style + '">' + header_text + '</div></div><div id="lookcloser_content" class="text">' + content_data + '</div><div class="interaction">' + interaction_buttons + '</div></div></div>',
		      type: 'html',
		      opts: {
		          idleTime: false,
		          animationDuration: 0,
		          toolbar: true,
		          clickSlide: interaction_swipe,
		          touch: interaction_swipe,
		          buttons: display_buttons
		        }
		    });

		    //Start the scrollbar on the popup.
		    if(scrollbar == "auto" || scrollbar == undefined){
		    	initializeContainerScrollBar("#lookcloser_content",false,false,true);
		    } else 

		    if (scrollbar == true ){
		    	initializeContainerScrollBar("#lookcloser_content",true,false,true);
		    }

		    
    		break;

    		

		case "gallery":
    		

		    $.fancybox.open(
				content_data, {
					idleTime: false,
			        animationDuration: 0,
			        toolbar: true,
			        clickSlide: interaction_swipe,
			        touch: interaction_swipe,
			        onInit: function(instance, current) {
						if(options.Content["LoadIndex"] != undefined){
							var instance = $.fancybox.getInstance();
							instance.jumpTo(options.Content["LoadIndex"])
						}
					}
				}

			);

		   

    		break;

    	case "video":
    		
    		var setting__default_program = getSetting("setting__default_program", "Default")

    		var video_file = "content/" + setting__default_program + "/@Media/" + content_data.Media["Video"];
    		var video_wrapper = '<source src="' + video_file + '" type="video/mp4">';
    		var subtitles = content_data["Subtitles"];
    		
    		var subtitles_container = [];

			$.each(subtitles,function(index,val){
				var file = "content/" + program + "/@Media/" + val["File"];
				var subtitle_wrapper = '<track label="' + val["Title"] + '" kind="subtitles" srclang="' + val["Language"] + '" src="' + file + '" default="">'
				
				subtitles_container.push(subtitle_wrapper)
			})	

			var subtitles_join = subtitles_container.join("");
    		
    		var video_container = '<video class="video-js" data-setup=\'{"playbackRates": [0.5, 1, 1.5, 2], "fluid": ' + content_data["Fluid"] + ', "muted": ' + content_data["Mute"] + ', "loop": ' + content_data["Loop"] + ',  "controls": true, "autoplay": ' + content_data["Autoplay"] + ', "preload": "auto"}\'>' + video_wrapper + '</video>';

    		//Open a lightbox, formatted with information from the button pressed.
		    $.fancybox.open({
		      src: '<div class="lookcloser"><div class="response"><div class="header"><div class="header_style ' + header_style + '">' + header_text + '</div></div><div id="lookcloser_content" class="text">' + video_container + '</div><div class="interaction">' + interaction_buttons + '</div></div></div>',
		      type: 'html',
		      opts: {
		          idleTime: false,
		          animationDuration: 0,
		          toolbar: true,
		          clickSlide: interaction_swipe,
		          touch: interaction_swipe
		        }
		    });

		    
    		break;
    }
    
}

function destroyLookCloserPopUp(){
	if(document.getElementById('activity_intro')){
		videojs(document.getElementById('activity_intro')).dispose();
	}
	
	$.fancybox.destroy();
}


/* ==========================================================================
   Scrollbar Functions
   ========================================================================== */

function initializeContainerScrollBar(container,show_scrollbar,auto_dragger_length,show_scroll_buttons) {
	if(container != null || container != undefined || container != ""){

		if(show_scrollbar == undefined){
			show_scrollbar = true;
		}

		if(auto_dragger_length == undefined){
			auto_dragger_length = false;
		}

		if(show_scroll_buttons == true){
			show_scroll_buttons = 1;
		} else {
			show_scroll_buttons = 0;
		}

		$(container).mCustomScrollbar({
			theme: "light",
			scrollInteria: 0,
			alwaysShowScrollbar: show_scrollbar,
			autoDraggerLength: auto_dragger_length,
			scrollButtons: {
				enable: show_scroll_buttons
			},
			callbacks: {
				onScrollStart: function() {
					$(".dropdown").select2('close');
				}
			}
		});

	}
}

function destroyContainerScrollBar(container) {
	var target = "";

	
	$(container).mCustomScrollbar("destroy");
	
}



//Destroy the scrollbar on the selected container.
//Options: activity, menu
function destroyScrollBar(container) {
	var target = "";

	//Menu is the menu container.
	if (container == "menu") {
		$("#menu_content_wrapper .menu_content").mCustomScrollbar("destroy");
	} else

	if (container == "exhibit") {
		$("#activities").mCustomScrollbar("destroy");
	} else

	if (container == "activities") {
		$("#activities").mCustomScrollbar("destroy");
	}
}

/* ==========================================================================
   BBCode Functions
   ========================================================================== */

//Progess BBcode params to return them as an object.
function processBBCodeParams(params) {

	//Split the params by space.
	var object = params.split(' ');
	var parsedObject = [];

	//Goes over each param, splits by equal sign, then creates an object.
	$.each(object, function(index, value) {
		var param = value.split('=');

		var key = param[0];
		var value = param[1];

		parsedObject[key] = value;
	});

	return parsedObject;
}

/* ==========================================================================
   Cache Functions
   ========================================================================== */

//Clears settings by emptying the localStorage cache.
function clearSettings() {
	localStorage.clear();

	saveConfigurationSettings({
	  "Screensaver": {
	    "Text": "Touch to Begin",
	    "Image": ""
	  },
	  "Device": {
	    "Screen Brightness": "4"
	  },
	  "Settings": {
	    "Default Program": "Default",
	    "Screensaver": "enabled",
	    "Contrast Mode": "disabled",
	    "Activity Timeout": "60",
	    "Slide Duration": "10000"
	  }
	})
}

//Clears accessibility options by emptying the sessionStorage cache.
function clearSession() {
	sessionStorage.clear();
}

//Manually resets accessibility options by returning them to defaults.
//This is used by the Accessibility menu "Reset Accessibility button" to reset options without reloading the page.
function clearAccessibility() {

	//Set the Accessibility menu drop down menus to their default options.
	$("#setting__contrast_mode").val("default").trigger('change.select2');
	$("#setting__font_size").val("default").trigger('change.select2');
	$("#setting__font_family").val("default").trigger('change.select2');
	$("#setting__paragraph_alignment").val("left").trigger('change.select2');
	$("#setting__paragraph_spacing").val("default").trigger('change.select2');

	//Ensure that the Contrast Mode style is removed from the body and manually sets the default sessionStorage accessibility option.
	removeBodyClassByPrefix('accessibility_contrast_mode_');
	setAccessibility("setting__contrast_mode", "default");

	//Ensure that the font size style is removed from the body and manually sets the default sessionStorage accessibility option.  
	//Adds the default font size class to the body.
	removeBodyClassByPrefix('accessibility_font_size_');
	setAccessibility("setting__font_size", "default");
	$('body').addClass("accessibility_font_size_default");

	//Ensure that the font face style is removed from the body and manually sets the default sessionStorage accessibility option.  
	//Adds the default font face class to the body.
	removeBodyClassByPrefix('accessibility_font_face_');
	setAccessibility("setting__font_face", "default");
	$('body').addClass("accessibility_font_face_default");

	//Ensure that the paragraph alignment style is removed from the body and manually sets the default sessionStorage accessibility option.  
	//Adds the default paragraph alignment class to the body.
	removeBodyClassByPrefix('accessibility_paragraph_alignment_');
	setAccessibility("setting__paragraph_alignment", "left");
	$('body').addClass("accessibility_paragraph_alignment_left");

	//Ensure that the paragraph spacing style is removed from the body and manually sets the default sessionStorage accessibility option.  
	//Adds the default paragraph spacing class to the body.
	removeBodyClassByPrefix('accessibility_paragraph_spacing');
	setAccessibility("setting__paragraph_spacing", "default");
	$('body').addClass("accessibility_paragraph_spacing_default");

	//Get the current URL.
	var url = getCurrentURL();

	//If an activity is loaded, apply that activity's theme.
	if (url["program"] != undefined && url["activity"] != undefined) {

		//Get the details for the loaded activity.
		var activity_details = loadActivityDetails(url["program"], url["activity"]);

		//If there is a theme default, use it.
		if (activity_details.Activity.Configuration["Theme"]) {
			var theme = activity_details.Activity.Configuration["Theme"];
			applyTheme(theme);
		}

	} else {

		//Get the factilitator contrast theme setting.
		var theme_override = getLocal("setting__contrast_override", "disabled");

		//If it is diabled, apply the default theme.  
		if (theme_override == 'disabled') {

			var theme = "default";
			applyTheme(theme);

			//If it is enabled, apply that accessibility theme.
		} else {

			$('head').append('<link rel="stylesheet" href="' + url["base"] + '/css/accessibility/accessibility_contrast_mode_' + theme_override + '.css" type="text/css" />');
			$('body').addClass("accessibility_contrast_mode_" + theme_override);

		}
	}
}

/* ==========================================================================
   JSON Functions
   ========================================================================== */

//Get a JSON file with AJAX and returns an object.
function parseJSONContent(file) {
	//Use Ajax to load the Object's content settings JSON file.
	var fileJSON = $.ajax({
		url: file,
		async: false
	}).responseText;

	//Use jQuery to parse JSON into a Content array.
	var file = jQuery.parseJSON(fileJSON);

	return file;

}

/* ==========================================================================
   HTML Functions
   ========================================================================== */

//Get an HTML file with AJAX and return its content.
function loadHTML(file) {

	//Use Ajax to load HTML content.
	var fileHTML = $.ajax({
		url: file,
		async: false
	}).responseText;

	return fileHTML;

}

/* ==========================================================================
   Menu Functions
   ========================================================================== */

//Closes the menu content window
function closeMenu() {
	$('#menu').removeClass('open');
	$('#menu').removeClass('full_menu');
	$('#open_menu').removeClass('open');
	$('#open_menu').addClass('close');

	$('#exhibit_presets').select2({
    minimumResultsForSearch: Infinity,
    width: '330px'
  })
}

//Collapses the menu to the right side of the screen, for use within Activities.
function collapseMenu() {
	$('#menu').removeClass('open');
	$('#menu').removeClass('full_menu');
	$('#open_menu').removeClass('open');

	$('#open_menu').addClass('close');

	$('#menu').addClass('collapsed');
}

//Loads the menu section iframe before opening it.
//section is an object with the menu, tier 1 and tier 2.
//This allows you to open nested menu pages using a search parameter in the URL.
function loadMenuSection(section) {

	var url = getCurrentURL();

	//Load the default program.
	var default_program = getSetting("setting__default_program", "Default");

	//Destroys the current menu scrollbar to ensure it displays correctly and scrolls to the top of the page.
	destroyContainerScrollBar("#menu_content_wrapper .menu_content");

	//Creates the HTML template to load.
	var section_html = url['base'] + "templates/menu/" + section['menu'] + ".html";

	//Loads the HTML file into a variable.
	var menu_content = loadHTML(section_html);

	//Places HTML content in proper container.
	$('#menu_' + section['menu']).html(menu_content)

	//When this function is run, it is given a pre-formatted array of which menu setion to load.
	//This array consists of: menu and tier1.
	//This allows the function to load both top tier menus, such as Settings, as well as submenus, such as About > Acknowledgements.

	//If only menu is defined, we will only be loading a top tier menu.
	if (section['menu'] != undefined && section['tier1'] == undefined) {

		//Menu specific JS
		switch (section['menu']) {

			//Load the Activities menu and fill it with the activities from the Program selected under the Settings menu.
			case "program":
				loadProgramMenu(default_program);
				break;

				//Load the Facilitation menu and fill it with facilitator information from the Program selected under the Settings menu.
			case "facilitation":
				loadFacilitationMenu(default_program);
				break;

				//Load the Settings menu.
			case "settings":
				loadSettingsMenu();
				break;

				//Load the Accessibility menu.
			case "accessibility":
				loadAccessibilityMenu();
				break;

				//Load the Help menu.
			case "about":
				loadAboutMenu();
				break;
		}

	} else

		//If menu and tier1 are defined.  We will be loading a submenu.
		if (section['menu'] != undefined && section['tier1'] != undefined) {

			//We will see what the menu above the submenu.  
			//This is pre-formatted by the script through the 'section' variable passed into the 'loadMenuSection' (this) function.
			switch (section['menu']) {

				//If we are loading the Facilitation menu, and a tier1 is present, we will automatically be loading the Discussion menu based on the value within 'tier1'.
				//Tier1 is the unique id for the discussion that will be loaded.
				case "facilitation":
					loadDiscussionMenu(default_program, section['tier1']);
					break;

					//There are multiple submenus that can be loaded under the Help menu.
				case "about":

					//We will check which menu is specified under tier1, and load that menu.
					switch (section['tier1']) {

						//Load the Acknowledgements submenu.
						case "clio":
							loadCLIOMenu();
							break;

						//Load the Getting Started submenu.
						case "setup":
							loadHowToMenu();
							break;
					}

					break;

			}

		}

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);

}

//Opens the selected menu section.
//'section' is an object with the menu and tier 1.
function openMenuSection(section) {

	var url = getCurrentURL();

	//If an activity isn't loaded, then make sure to update the URL to reflect which menu section is open.
	if (!$('body').hasClass('activity')) {

		//Just pass the 'section' variable that was used to initiate the parent function.
		setURLParameters(url["mode"],section);
	}

	//Set the menu and button id using the menu section defined within the 'section' object.
	var menuID = "menu_" + section['menu'];
	var buttonID = section['menu'] + "_button";

	//Select the clicked button.
	if (!$('#' + buttonID).hasClass('selected')) {
		$('#' + buttonID).addClass('selected');
	} else

		//If the menu has the class minimal, that means the user is in an activity and the accessibility menu is open.  This removes the selected class that displays a close button instead of the accessibility button.
		if ($('#menu').hasClass('minimal')) {
			$('#' + buttonID).removeClass('selected');
		}

	//Check if there are other siblings with the 'selected' class.
	//If there aren't, open the menu content.
	if ($('#' + buttonID).siblings('.selected').length == 0) {
		$('#menu_content_wrapper').addClass('open')
		$('#menu').removeClass('closed')
		$('#menu').addClass('full_menu')
	}

	//Remove selected class from all siblings.
	$('#' + buttonID).siblings().removeClass('selected');
	$('#' + menuID).siblings().hide();

	//If there are no children  with 'selected' class, then close the menu content.
	if ($('#menu .bumper').children('.menu_button.selected').length == 0) {

		//If an activity is loaded, we don't want to show the full menu, just a collapsed menu on the right side..
		if ($('body').hasClass('activity')) {
			$('#menu').removeClass('minimal')
			$('#menu').addClass('collapsed')
		}

		$('#menu_content_wrapper').removeClass('open')
		$('.menu_button').addClass('close');
		$('#menu').removeClass('full_menu')
		$('#menu').addClass('closed')
		$('#menu_content_wrapper .menu_section').each(function() {
			$(this).html('')
			$(this).hide()
		})
		if (!$('body').hasClass('activity')) {
			var menu_section = [];
			setURLParameters(url["mode"],menu_section);
		}


	} else {
		$('#menu_content_wrapper').addClass('open');
	}

	$('#' + menuID).show();
}

//This returns a user home (to facilitator mode and exhibit mode) from an activity.
function returnHome() {
	//Closes any lightbox windows.
	$.fancybox.destroy();
	
	//If in exhibit mode, clear the accessibility options when returning home.
	if ($('body').hasClass('user') == true) {
		$('.owl-carousel').removeClass('owl-hidden')
		clearAccessibility();
	}
	
	//Removes the meta activity style class from the home button.
	$('#return_home').removeClass('meta')

	//Hides activity content and displays both the exhibit and facilitator mode containers.
	$('#content').hide();
	$('#exhibit').show();
	$('#desktop').show();
	
	//Removes the class that hides the accessibility button from an activity that has accessibility disabled.
	$('body').removeClass('hide_accessibility')

	$('body').removeClass("child_activity");
	
	var url = getCurrentURL();

	//Removes the activity class and adds the homescreen class.
	$('body').removeClass('activity');
	$('body').addClass('homescreen');

	//Removes all content from the previously loaded activity.
	$('#activity_content').html('');

	$("#activity_menu_description .description_wrapper").html('');
	$("#activity_video").html('');
	$('#acknowledgement_list .acknowledgement_container').html('');
	$('#attribution_list .attribution_container').html('')


	//Resets the content subheader
	$('#content_subheader').show();

	//Removes any theme class used by the previously loaded activity.
	removeBodyClassByPrefix('theme_')
	
	//Removes any activity type class used by the previously loaded activity.
	removeBodyClassByPrefix('type_')
	
	//Applies the default theme and checks for contrast mode options.
	applyTheme("default")
	var contrast_override = getLocal("setting__contrast_override", "default")
	if (contrast_override != "default") {
		removeBodyClassByPrefix('accessibility_contrast_mode_')
		$('head').append('<link rel="stylesheet" href="css/accessibility/accessibility_contrast_mode_' + contrast_override + '.css" type="text/css" />');
		$('body').addClass("accessibility_contrast_mode_" + contrast_override);
	}

	//Check if this was a link from the Exhibit menu or the Activities menu.
	var desktop_link = getSession("desktop_link", "false")
	
	//If it was the Activities menu, return there and reset the session setting.
	if (desktop_link == "true") {

		var menu_section = {};
		menu_section['menu'] = "program";

		loadMenuSection(menu_section)
		openMenuSection(menu_section)

		setSession("desktop_link", "false")
	} 
	//If it was the Exhibit menu, just return home.
	else {
		$('.menu_button.selected').removeClass('selected')
		setURLParameters(url["mode"])
	}

	//Restore the full menu.
	$('#menu').removeClass('collapsed')

	//Reset the screen brightness to the facilitator's last setting.
	var device_brightness = getLocal("device_brightness", "4")
	setScreenBrightness(device_brightness);

	//Closes the Activity Menu
	$("#activity_menu .activity_menu_header").html("Activity Menu");

   $('#activity_menu_button').removeClass('open');
   $('#activity_menu_button').addClass('closed')

   $('#activity_menu').removeClass('open');
   $('#activity_menu').addClass('closed');

   $('#activity_menu_list').removeClass('closed');
   $('#activity_menu_list').addClass('open');

   $('#activity_menu_subcontent').removeClass('open');
   $('#activity_menu_subcontent').addClass('closed');

   $('#activity_menu_back').addClass('closed');

   if(videojs.getPlayers()["activity_menu_intro"]) {
     delete videojs.getPlayers()["activity_menu_intro"];
   }

   $("#activity_video").html("");

}

/* ==========================================================================
   Facilitation Menu Functions
   ========================================================================== */

function loadFacilitationMenu(program) {

	destroyContainerScrollBar("#menu_content_wrapper .menu_content")

	var url = getCurrentURL();

	//Load the program details.
	var program_details = loadProgramDetails(program)

	//Append the title of the Program to the top of the Facilitator menu.
	$("#facilitation_title").append(program_details['Program'])

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_facilitation');

	//Load the Facilitation details from the @About.json file saved within the Program's @Facilitation folder.  Then parse it into an object.
	var file = url['base'] + "content/" + program + "/@Facilitation/@About.json";
	var content = parseJSONContent(file);

	//Append each Big Idea to the appropriate list.
	$.each(content["Big Ideas"], function(index, value) {
		$('#facilitation_content #big_ideas ul').append('<li>' + value + '</li>')
	})

	//Append each Objective to the appropriate list.
	$.each(content["Objectives"], function(index, value) {
		$('#facilitation_content #objectives ul').append('<li>' + value + '</li>')
	})

	//Append the introductions to the appropriate list.
	$.each(content["Introductions"], function(index, value) {

		//Parse this content as Rich Text.
		var parsed = BBCodeParser.process(value);
		$('#facilitation_content #introductions ol').append('<li>' + parsed + '</li>')
	})

	//Parse the instructions as Rich Text and append it to the Instructions section.
	var parsedInstructions = BBCodeParser.process(content["Instructions"]);
	$('#facilitation_content #instructions').html(parsedInstructions);

	//Get a list of the discussions available within this Program.
	var discussions = requestFacilitationDiscussions(program);

	//Creates a button for each discussion in the Program's facilitation folder.
	$.each(discussions, function(index, value) {

		//The unique id is the file name without .json
		var id = value.replace(".json", "")

		//Load the Discussion details from the appropriate json file saved within the Program's @Discussions folder.  Then parse it into an object. 
		var base_url = url['base'] + "content/" + program + "/@Facilitation/@Discussions/" + value;
		var content = parseJSONContent(base_url)

		//Process the description as Rich Text.
		var parsedDescription = BBCodeParser.process(content["Description"]);

		//Append the final button..
		$('#facilitation_content #discussions').append('<div class="discussion discussion_button" id="' + id + '" data-program="' + program + '" data-discussion="' + id + '"><div class="icon"><div class="image"></div></div><div class="content"><div class="discussion_header"><div class="title">' + content["Name"] + '</div></div><div class="discussion_description">' + parsedDescription + '</div></div><div class="discussion_link"></div></div>');
	})


	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
	
}

function loadDiscussionMenu(program, discussion) {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")

	var url = getCurrentURL();

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_discussion');

	//Creates the location for the Discussion menu template and then loads it into the Facilitation menu.
	var discussion_url = url['base'] + '/templates/menu/discussion.html'
	var discussion_html = loadHTML(discussion_url);
	$('#facilitation_menu').html(discussion_html);

	//Load the discussion details from a JSON file and parse into an object.
	var base_url = url['base'] + "content/" + program + "/@Facilitation/@Discussions/" + discussion + ".json";
	var content = parseJSONContent(base_url)

	//Set the Submenu header text to the name of the Discussion.
	$('#page_title').html(content['Name']);

	//Parse the description as Rich Text and append to the Discussion description section. 
	var parsedDescription = BBCodeParser.process(content["Description"]);
	$('#discussion_description').html(parsedDescription);

	//For each level of discussion, we will add a new bullet point section.
	var discussion = content['Discussion'];

	$.each(discussion, function(index, value) {

		var parsed = BBCodeParser.process(value);
		var content = '<div class="facilitation_subsection"><div class="icon"><div class="image" style="background-image: url(\'./img/black/settings/dot.svg\');"></div></div><div class="content"><div class="facilitation_subheader"><div class="title">' + index + '</div></div><div class="subsection_content">' + parsed + '</div></div></div>'
		$('#discussion_content .facilitation_section').append(content)

	})

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
		
}

//Request available Discussions within a Program, and then returns an array.
function requestFacilitationDiscussions(program) {

	var url = getCurrentURL();
	var directory = "../content/" + program + "/@Facilitation/@Discussions/";

	var available_discussions = parseDirectory(directory)

	//Removes the ending slash from the name.
	$.each(available_discussions, function(index, value) {
		available_discussions[index] = value.replace("/", "")
	})

	return available_discussions;
}

//Function to parse the @About.json file in the Activity directory.
function loadDiscussionDetails(program, discussion) {
	var url = getCurrentURL();

	//Create base url.
	var base_url = url['base'] + "content/" + program + "/@Facilitation/@Discussions/" + discussion + ".json";
	var content = parseJSONContent(base_url)

	return content;
}

/* ==========================================================================
   Activity Menu Functions
   ========================================================================== */

function loadProgramMenu(program) {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")

	var url = getCurrentURL();

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_program');

	//Load program details.
	var program_details = loadProgramDetails(program);
	
	//Get an object of any selected activities.
	var selected_activities = getSessionObject("selected_activities", "")

	//Change the view based on the URL.
	if (url['view']) {
		
		//Change the view type
		$('#program_menu').addClass('view_' + url['view'])
		
		//Make sure other views aren't selected.
		$('.sort_button').each(function() {
			$(this).removeClass('selected');
		})

		//Select the new view button.
		$('#activity_' + url['view']).addClass('selected');
	} 

	//Or just default to details view.
	else {
		
		$('#program_menu').addClass('view_details')
		$('#activity_details').addClass('selected');
	}

	//Initialize Select2 for the sort bar dropdown menus. 
	$('.dropdown').select2({
		minimumResultsForSearch: Infinity,
		width: '200px'
	})

	//Get a list of the activities contained within this Program.
	var activities = requestActivities(program);
	//Get a list of the audiences defined within this Program.
	var audiences = getAudiences(program);
	//Get a list of activity types included in the CLIO web application.
	var activity_types = getActivityTypes();

	//For each activity, we're going to need to create a button to access it.
	$.each(activities, function(index, val) {
		var activity = val;
		var image = "";

		//Load the details about the selected activity.
		var activity_details = loadActivityDetails(program, activity)
		
		//Define the type of the activity and get the approriate details about it.
		var type = activity_details.Activity["Type"];
		var type_details = activity_types[type]

		//Define the audience of the activity and get the approriate details about it.
		var audience = activity_details.Activity["Audience"];
		var audience_details = audiences[audience]

		//Process the Rich Text description for the audience and activity types.
		var type_description = BBCodeParser.process(type_details["Description"]);
		var audience_description = BBCodeParser.process(audience_details["Description"]);

		//Create the relative location for the activity json file.
		var activity_url = "content/" + program + "/@Activities/" + activity + ".json"
		
		//Check if this activity should be hidden.  If so, set hidden to "hidden".  This is used to add a hidden class.
		var hidden = "";
		if (activity_details.Activity.Configuration['Hidden'] == "true") {
			hidden = "hidden"
		}

		//Check if this activity has previously been selected this session.  If so, we will be changing the add button into a checkmark.
		var selected = "unselected"
		if (selected_activities.hasOwnProperty(activity_url) == true) {
			selected = "selected"
		}

		//Append the final button for this activity.
		$("#program_menu").append('<div id="activity_' + activity + '" class="activity ' + hidden + ' ' + activity_details.Activity["Type"] + ' activity activity_button"  data-program="' + program + '" data-type="' + activity_details.Activity["Type"] + '" data-activity="' + activity + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox ' + selected + '"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-description="' + type_description + '" data-title="' + type_details["Name"] + '" data-help_type="Activity Type"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help"  data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');


	})

	//Append each audience to the sort bar's audience dropdown menu.
	$.each(audiences, function(index, val) {
		$("#sort__audience").append('<option value="' + index + '">' + val["Name"] + '</option>')
	})

	//Append each activity type to the sort bar's activity type dropdown menu.
	$.each(activity_types, function(index, val) {
		$("#sort__activity_type").append('<option value="' + index + '">' + val["Name"] + '</option>')
	})

		

}

/* ==========================================================================
   Settings Menu Functions
   ========================================================================== */

function loadSettingsMenu() {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")

	var url = getCurrentURL();

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_settings');

	//Get the default program if it is set, otherwise default to "Default"
	var setting__default_program = getSetting("setting__default_program", "Default")

	//Get a list of the available Programs and append them to the default program dropdown menu.
	var program_list = requestProgramList();
	appendToDropdownbyTitleID('#setting__default_program', program_list);

	//Update settings dropdown menus based on current saved settings, or load the defaults.
	$('#setting__default_program').val(getSetting("setting__default_program", "Default"))
	$('#setting__activity_timeout').val(getSetting("setting__activity_timeout", "60"))
	$('#setting__slide_duration').val(getSetting("setting__slide_duration", "10000"))
	$('#setting__contrast_override').val(getSetting("setting__contrast_override", "disabled"))
	$('#setting__screensaver').val(getSetting("setting__screensaver", "60000"))


	$('#kiosk_pin span').html(getSetting("pin", "2546"))

	if(url["development"] == "true"){
		$("#kiosk_development").hide();
		$("#kiosk_production").show();

		$('#development_section .title').html("Production Environment");
		$('#development_section .description').html("Start the kiosk in a live user environment without access to the operating system.");
	}

	if(url["mode"] == "kiosk"){
		var device_brightness = getLocal("device_brightness", 4)
		$('.brightness_control').each(function(index, val) {

			if (index < device_brightness) {
				$(this).removeClass('unselected')
				$(this).addClass('selected')
			} else {
				$(this).removeClass('selected')
				$(this).addClass('unselected')
			}

		})

		$.ajax({
	      url: "command/temperature.php",
			success: function(data) {
				
				if(data != "Not Available"){
					$("#kiosk_temperature span").html(data + "&deg;C")

					if(data > 75){
						$("#kiosk_temperature span").css("color","red");
					} else if(data > 75 && data > 60){
						$("#kiosk_temperature span").css("color","orange");
					} else {
						$("#kiosk_temperature span").css("color","green");
					}

				} else {
					$("#kiosk_temperature span").html(data)
				}
				
			}
		});

		$.ajax({
	      url: "command/throttled.php",
			success: function(data) {
				
				if(data != "Not Available"){
					$("#kiosk_throttle span").html(data)

				} else {
					$("#kiosk_throttle span").html(data)
				}
				
			}
		});

		$.ajax({
	      url: "command/model.php",
			success: function(data) {
				
				if(data != "Not Available"){
					$("#kiosk_model span").html(data)

				} else {
					$("#kiosk_model span").html(data)
				}
				
			}
		});

	} else {
		$("#settings_menu .setting_kiosk").hide()

	}
	//For the kiosk, we will see what the default screen brightness has been set to.  Then we will update the brightness control section to display the currently setting.
		

	//Initialize Select2 to style the dropdown menus.
	$('.dropdown').select2({
		minimumResultsForSearch: Infinity,
		width: '400px'
	})

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);

		
}

/* ==========================================================================
   Accessibility Menu Functions
   ========================================================================== */

function loadAccessibilityMenu() {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")
	var url = getCurrentURL();

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_accessibility');

	//If we are in Facilitator mode, hide the user brightness control panel from the accessibility menu.
	//If we are in Exhibit mode, get the previously used Facilitator settings for brightness and update the panel.
	if ($("body").hasClass("facilitator") == true) {
		
		$("#user_brightness").hide()
	
	} else {
		$("#user_brightness").hide()
	}

	//Update forms based on current saved accessibility settings, otherwise load the defaults.
	$('#setting__contrast_mode').val(getAccessibility("setting__contrast_mode", "default"))
	$('#setting__font_face').val(getAccessibility("setting__font_face", "default"))
	$('#setting__font_size').val(getAccessibility("setting__font_size", "default"))
	$('#setting__paragraph_alignment').val(getAccessibility("setting__paragraph_alignment", "left"))
	$('#setting__paragraph_spacing').val(getAccessibility("setting__paragraph_spacing", "default"))
	$('#setting__autoplay').val(getAccessibility("setting__autoplay", "true"))
	$('#setting__playback_speed').val(getAccessibility("setting__playback_speed", 1))

	//Style dropdown menu elements.
	$('.dropdown').select2({
		minimumResultsForSearch: Infinity,
		width: '400px'
	})

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
		
}

/* ==========================================================================
   Help Menu Functions
   ========================================================================== */

function loadAboutMenu() {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_about');
	
	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
		
}

//Getting Started Guide submenu
function loadHowToMenu() {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")
	var url = getCurrentURL();
	
	//Get the currently selected program.
	var program = getSetting("setting__default_program", "Default")

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_setup');

	//Loads the HTML template into the proper menu.
	var section_html = url['base'] + "templates/menu/setup.html";
	var menu_content = loadHTML(section_html);
	$('#menu_about').html(menu_content)
	
	//Sets the Submenu header title.
	$('#page_title').html("Getting Started");

	//Get the audiences defined by the program.
	var audiences = getAudiences(program);

	
	//Append the audiences and their descriptions to the audience section.
	$.each(audiences, function(index, val) {
		var content = BBCodeParser.process(val["Description"])
		$('#available_audiences').append('<p><b>' + val["Name"] + '</b></p>' + content + '<p>&nbsp;</p>')
	})

	//Get the activity types defined by the CLIO web application.
	var types = getActivityTypes();



	//Append the activity types and their descriptions to the activity type section.
	$.each(types, function(index,val) {
		
		var content = BBCodeParser.process(val["Description"])
		
		$('#available_types').append('<p><b>' + val["Name"] + '</b></p>' + content + '<p>&nbsp;</p>')
	})

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
		
}

//Information submenu 
function loadCLIOMenu() {
	destroyContainerScrollBar("#menu_content_wrapper .menu_content")
	var url = getCurrentURL();
	console.log("hello")
	//Loads the HTML template into the proper menu.
	var section_html = url['base'] + "templates/menu/information.html";
	var menu_content = loadHTML(section_html);
	$('#menu_about').html(menu_content)

	//Make sure that none of the other menus are marked as selected before selecting this one.
	removeSelectorClassByPrefix("#menu_content_wrapper", "menu_")
	$('#menu_content_wrapper').addClass('menu_information');

	//Sets the Submenu header title.
	$('#page_title').html("Information");

	initializeContainerScrollBar("#menu_content_wrapper .menu_content",true,false,true);
		
}

/* ==========================================================================
   Program Functions
   ========================================================================== */

//Request available programs and then returns an array.
function requestPrograms() {
	var url = getCurrentURL();
	var directory = '../content/'

	var available_programs = parseDirectory(directory)
	//Removes the ending slash from the name.
	$.each(available_programs, function(index, value) {
		available_programs[index] = value.replace("/", "")
	})

	return available_programs;
}

//Creates a formatted array of Programs for the Settings menu with an id and a friendly name.
function requestProgramList() {

	var available_programs = requestPrograms();

	var IDandTitle = [];

	//Removes the ending slash from the name.
	$.each(available_programs, function(index, value) {

		var program_details = loadProgramDetails(value);

		var TitleID = [value, program_details['Program']]

		IDandTitle.push(TitleID)
	})

	return IDandTitle;
}

//Function to parse the @About.json file in the Program directory.
function loadProgramDetails(program) {

	var url = getCurrentURL();

	//Parse the content of JSON file.
	var base_url = url['base'] + "content/" + program + '/@About.json';
	var content = parseJSONContent(base_url)
	return content;
}

/* ==========================================================================
   Activity Functions
   ========================================================================== */

//Request available activities within the Program directory.
function requestActivities(program) {

	var url = getCurrentURL();
	var directory = "../content/" + program + "/@Activities/";

	var activities = parseDirectory(directory)

	var available_activities = [];

	$.each(activities, function(index, val) {
		var safe_name = val.replace(".json", "")
		available_activities.push(safe_name)
	})

	return available_activities;
}

//Loads the content of the activities JSON file and returns an object.
function loadActivityDetails(program, activity) {
	var url = getCurrentURL();

	//Create base url.
	var base_url = url['base'] + "content/" + program + "/@Activities/" + activity + ".json";

	var content = parseJSONContent(base_url)

	return content;
}

//This is used to shuffle the subcontents of a container.
jQuery.fn.shuffleChildren = function() {
	var p = this[0];
	for (var i = p.children.length; i >= 0; i--) {
		p.appendChild(p.children[Math.random() * i | 0]);
	}
};

/* ==========================================================================
   Theme Functions
   ========================================================================== */

//Applies a theme, if a contrast mode is not enabled.
function applyTheme(theme) {
	var setting__contrast_mode = getAccessibility("setting__contrast_mode", "default");

	//Checks if contrast mode is disabled before applying theme.
	if (setting__contrast_mode == "default") {
		//Add the theme that was loaded from the layout settings file.

		if (theme != 'default') {
			$('head').append('<link rel="stylesheet" href="./css/themes/theme_' + theme + '.css" type="text/css" />');
		}

		$('body').addClass("theme_" + theme);

	}
}

/* ==========================================================================
   Class Functions
   ========================================================================== */

//Accepts a prefix and removes all classes from body that fit.
function removeBodyClassByPrefix(prefix) {
	var regex = new RegExp("(^|\\s)" + prefix + "\\S+", 'g')

	$('body').removeClass(function(index, className) {
		return (className.match(regex) || []).join(' ');
	});
}

//Accepts a prefix and removes all classes from the selector that fit.
function removeSelectorClassByPrefix(selector, prefix) {
	var regex = new RegExp("(^|\\s)" + prefix + "\\S+", 'g')

	$(selector).removeClass(function(index, className) {
		return (className.match(regex) || []).join(' ');
	});

}

/* ==========================================================================
   Settings Functions
   ========================================================================== */

//Gets a setting from localStorage.
function getSetting(setting_name, setting_default) {

	var value = localStorage.getItem(setting_name);

	if (value == null) {
		value = setting_default;
	}

	return value;

}

//Sets a setting in localStorage.
function setSetting(setting_name, user_setting) {
	localStorage.setItem(setting_name, user_setting)
}

/* ==========================================================================
   Accessibility Functions
   ========================================================================== */

//Gets an accessibility option from sessionStorage.
function getAccessibility(setting_name, setting_default) {

	var value = sessionStorage.getItem(setting_name);

	if (value == null) {
		value = setting_default;
	}

	return value;
}

//Sets an accessibility option in sessionStorage.
function setAccessibility(setting_name, user_setting) {
	sessionStorage.setItem(setting_name, user_setting)
}

/* ==========================================================================
   LocalStorage Functions
   ========================================================================== */

//Gets a string from localStorage.
function getLocal(name, setting_default) {

	var value = localStorage.getItem(name);

	if (value == null) {
		value = setting_default;
	}

	return value;

}

//Gets an object from localStorage
function getLocalObject(name, setting_default) {

	var value = localStorage.getItem(name);
	var deStrify = JSON.parse(value)

	if (deStrify == null) {
		deStrify = setting_default;
	}

	return deStrify;

}

//Sets a string in localStorage.
function setLocal(name, user_setting) {
	localStorage.setItem(name, user_setting)
}

//Sets an object from localStorage
function setLocalObject(name, user_setting) {
	var JSONStringify = JSON.stringify(user_setting);
	localStorage.setItem(name, JSONStringify)
}

/* ==========================================================================
   SessionStorage Functions
   ========================================================================== */

//Gets a string from sessionStorage.
function getSession(name, setting_default) {

	var value = sessionStorage.getItem(name);

	if (value == null) {
		value = setting_default;
	}

	return value;
}

//Gets an object from sessionStorage.
function getSessionObject(name, setting_default) {

	var value = sessionStorage.getItem(name);
	var deStrify = JSON.parse(value)

	if (deStrify == null) {
		deStrify = setting_default;
	}

	return deStrify;
}

//Sets a string in sessionStorage.
function setSession(name, user_setting) {
	sessionStorage.setItem(name, user_setting)
}

//Sets an object in sessionStorage.
function setSessionObject(name, user_setting) {
	var JSONStringify = JSON.stringify(user_setting);
	sessionStorage.setItem(name, JSONStringify)
}

/* ==========================================================================
   Audience Functions
   ========================================================================== */

//Gets a list of the audiences defined within the Program.
function getAudiences(program) {
	var url = getCurrentURL();
	var file = url['base'] + 'content/' + program + '/@Audiences.json';
	var audiences_from_file = parseJSONContent(file)
	return audiences_from_file;
}

/* ==========================================================================
   Activity Type Functions
   ========================================================================== */

//Gets a list of the Activity Types defined within the CLIO web application.
function getActivityTypes() {
	
	var url = getCurrentURL();

	//Get the Activity types.
	var activityconfig_directory = "../config/@ActivityTypes/";
	var activity_types = parseDirectory(activityconfig_directory);
	var types = {}
	$.each(activity_types,function(index,val){


		//Get Information about the Activity Types.
		var file = "config/@ActivityTypes/" + val + "/@About.json";
		var activitytype_content = parseJSONContent(file);

		var current_type = {};
		
		types[val] = {
			"Name":activitytype_content["Name"],
			"Description":activitytype_content.Configuration["Description"]
		};
		
	})

	return types;

}

//Adds the activity types from the program to the web application for preloading.
function initializeActivityTypes(program){

	var program_activities = requestActivities(program);
	var program_types = [];

	var types_JSON = {};

	$.each(program_activities,function(index,val){
		
		var activity_details = loadActivityDetails(program, val);
		var activity_type = activity_details.Activity["Type"];

		var found = jQuery.inArray(activity_type, program_types);
		if (found == -1) {
		    // Element was not found, add it.
		    program_types.push(activity_type);
		}

	})



	$.each(program_types,function(index,type){
		
		var url = getCurrentURL();
		var file = "config/@ActivityTypes/" + type + "/@About.json";
		var activitytype_content = parseJSONContent(file);


		var stylesheet_directory = "../config/@ActivityTypes/" + type + "/@Stylesheets/";
		var available_stylesheets = parseDirectory(stylesheet_directory);

		//Load style sheets
		$.each(available_stylesheets,function(index,val){
			//Apply any custom stylesheets.
	    	$('head').append('<link rel="stylesheet" href="config/@ActivityTypes/' + type + "/@Stylesheets/" + val + '" type="text/css" />')
		})

		//Get the JS libraries to add.
		var library_directory = "../config/@ActivityTypes/" + type + "/@Libraries/";
		var available_libraries = parseDirectory(library_directory);

		$.each(available_libraries,function(index,val){
			//Apply any custom JavaScript.
	    	
	    	var script = "config/@ActivityTypes/" + type + "/@Libraries/" + val;

			$.getScript(script, function() {
			    console.log(script + " script loaded and executed.");

			});


		})

		types_JSON[type] = { "loaded":"true"};



	})

	
	var types_encode = JSON.stringify(types_JSON);

	$("#activity_content").attr("data-types",types_encode)

	console.log(program_types)
}


function clearBrowserCache(){
	var url = getCurrentURL();
	if(url["mode"] == "kiosk"){
		$.ajax({
	      url: "command/cache.php",
			success: function(data) {
				console.log(data)
			}
		});
	}
		
}

function setScreenBrightness(level,mode) {
	$.ajax({
      url: "command/brightness.php?level=" + level,
		success: function(data) {
			console.log('Set brightness to ' + level)
			console.log(data)
		}
	});

	if(mode == "device"){
		setLocal("device_brightness", level)
	}
}


function getConfigurationSettings(){
	var settings = parseJSONContent("config/@Configuration.json");
	return settings;
}

function updateStorageSettings(settings){
	

	setLocal("device_brightness", settings.Device["Screen Brightness"]);

	setLocal("setting__default_program", settings.Settings["Default Program"]);
	setLocal("setting__screensaver", settings.Settings["Screensaver"]);
	setLocal("setting__contrast_override", settings.Settings["Contrast Mode"]);
	setLocal("setting__slide_duration", settings.Settings["Slide Duration"]);
	setLocal("setting__activity_timeout", settings.Settings["Activity Timeout"]);

	setLocal("pin", settings.Settings["Pin"]);




}

function formatConfigurationSettings(){
	var device_brightness = getLocal("device_brightness","4");
	var default_program = getLocal("setting__default_program","Default");
	var screensaver = getLocal("setting__screensaver","enabled");
	var contrast_override = getLocal("setting__contrast_override","disabled");
	var activity_timeout = getLocal("setting__activity_timeout","60");
	var slide_duration = getLocal("setting__slide_duration","10000");
	console.log(slide_duration)
	var pin = getLocal("pin","2546");

	var data = {
	  "Screensaver": {
	    "Text": "Touch to Begin",
	    "Image": ""
	  },
	  "Device": {
	    "Screen Brightness": device_brightness
	  },
	  "Settings": {
	    "Default Program": default_program,
	    "Screensaver": screensaver,
	    "Contrast Mode": contrast_override,
	    "Activity Timeout": activity_timeout,
	    "Slide Duration": slide_duration,
	    "Pin":pin
	  }
	}
	return data;
}

function saveConfigurationSettings(settings){
	
	var settings = JSON.stringify(settings);
	$.ajax({
      url: "php/configuration.php?data=" + encodeURIComponent(settings),
		success: function(data) {
		}
	});

}

function loadExhibitPresets(program){
	var program_details = loadProgramDetails(program);
	var presets = program_details["Presets"];

	var IDandTitle = [];

	//Removes the ending slash from the name.
	$.each(presets, function(index, value) {
		var activities = JSON.stringify(value["Activities"]);
		$("#exhibit_presets").append('"<option data-name="' + value["Name"] + '" data-activities=\'' + activities + '\' class="query" value="' + index + '">' + value["Name"] + '</option>"');
	})

}