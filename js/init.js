//Init.js
//Main page load scripts for the CLIO Web application.
//Configures the homescreen based on URL parameters.




$(document).ready(function() {


  /* ==========================================================================
   Initializations
   ========================================================================== */

  //When there are no URL parameters set, the web application will default to a full Facilitator Mode experience.
  //This allows a faciliator to select activities before starting Exhibit Mode.

  var configuration = getConfigurationSettings();
  updateStorageSettings(configuration);
  
  //Create current URL
  var url = getCurrentURL();

  //Set the default mode if one isn't selected
  if(url["mode"] == undefined){
    setURLParameters("facilitator");
  }

  //Get the last used program, or load the default program.  Get details about the program.
  var default_program = getSetting("setting__default_program", "Default");

  

  

  if(url["program"] == "" || url["program"] == undefined){
    program = default_program;
  } else {
    program = url["program"];
  }

  
  
  //Set the default theme.
  $('head').append('<link rel="stylesheet" href="./css/themes/theme_default.css" type="text/css" />');
  $('body').addClass("theme_default");

  //If there is a branding CSS file present within the Program, apply it.
  var exhibit_branding = doesFileExist(url['base'] + "content/" + program + "/branding.css")
  if (exhibit_branding == true) {
    $('head').append('<link rel="stylesheet" href="' + url['base'] + 'content/' + program + '/branding.css" type="text/css" />');
  }
  
  //Temporarily disable previously selected activities.
  //setSessionObject("selected_activities",{})

  $('#exhibit_presets').select2({
    minimumResultsForSearch: Infinity,
    width: '330px'
  })
  
  loadExhibitPresets(program);




  /* ==========================================================================
     Usage Modes
     ========================================================================== */

  switch(url["mode"]) {
    case "kiosk":
      
      initializeActivityTypes(program);
      loadKioskMode(program);
      break;

    case "facilitator":
      initializeActivityTypes(program);
      loadFacilitatorMode(program);
      break;

    case "exhibit":
      //If program is defined in the URL, set it as the default program.
      if(url["program"] != undefined){
        setSetting("setting__default_program",url["program"])
      }

      if(url["program"] != "" && url["program"] != undefined){
        loadExhibitMode(url['program']);
      } else {
        loadFacilitatorMode(default_program);
      }
      
      break;

    case "activity":
      //If program is defined in the URL, set it as the default program.
      if(url["program"] != undefined){
        setSetting("setting__default_program",url["program"])
      }

      if(url["program"] != "" && url["program"] != undefined && url["activity"] != "" && url["activity"] != undefined){
        loadActivityMode(url["program"],url["activity_details"]);
      } else {
        loadFacilitatorMode(default_program)
      }
      
      break;

    case "chromeless":
      //If program is defined in the URL, set it as the default program.
      if(url["program"] != undefined){
        setSetting("setting__default_program",url["program"])
      }

      if(url["program"] != "" && url["program"] != undefined && url["activity"] != "" && url["activity"] != undefined){
        loadChromelessMode(url["program"],url["activity_details"]);
      } else {
        loadFacilitatorMode(default_program)
      }
      

      break;

    
    

  }




  /* ==========================================================================
     Load Activity
     ========================================================================== */

  //Check if there is a program and activity specified, and then open it.
  if (url['program'] != undefined && url['activity'] != undefined) {   

    //Load the activity.
    loadActivity(url['program'], url['activity'])
   
    //Change from the homescreen to the activity screen.
    $('body').removeClass('homescreen');
    $('body').addClass('activity');
  }


  /* ==========================================================================
     Load Menu
     ========================================================================== */

    //Check if there is a menu parameter, then opens that menu.
  if (url['menu'] && url['mode'] != "activity" && url["mode"] != "exhibit" && url["mode"] != "chromeless") {

    //Split the menu url parameter by slash.
    var menu_navigation = url['menu'].split('/');

    //Create an array of menu navigation.
    var menu_section = {};
    menu_section['menu'] = menu_navigation[0];
    menu_section['tier1'] = menu_navigation[1];
    menu_section['tier2'] = menu_navigation[2];
    menu_section['tier3'] = menu_navigation[3];

    //Load the menu section, then display it.
    loadMenuSection(menu_section)
    openMenuSection(menu_section)

  } 



  /* ==========================================================================
     Facilitator Mode Events
     ========================================================================== */

  //Exhibit menu button to load the activities that were used the last time the kiosk entered Exhibit Mode..
  $('#reload_exhibit').click(function() {

    //Get the activities used the last time CLIO entered exhibit mode.
    var selected_activities = getLocalObject("selected_activities", "")
    
    //Get the program that was used the last time CLIO entered exhibit mode.
    var program = getSetting("setting__default_program", "Default");
   
    //Get information about that programs audiences and the activity types available to CLIO.
    var audiences = getAudiences(program);
    var activity_types = getActivityTypes();

    //If there were previously selected activities, do this.
    if (selected_activities != "") {
      
      //Clear any activities that are loaded in the Exhibit menu.
      $("#activities .selected_activities").html("")
      
      //For each activity, we need to add a button to the Exhibit menu list of selected activities.
      $.each(selected_activities, function(index, val) {

        //Get details about that activity.
        var activity_details = loadActivityDetails(val["Program"], val["Activity"])

        //Get more information about the activity type.
        var type_details = activity_types[activity_details.Activity["Type"]]

        //Get more information about the audience.
        var audience_details = audiences[activity_details.Activity["Audience"]]

        //Process the audience and activity type descriptions as rich text.
        var type_description = BBCodeParser.process(type_details["Description"]);
        var audience_description = BBCodeParser.process(audience_details["Description"]);

        //Add the activity button to the Exhibit menu list of selected activities.
        $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '" data-title="' + activity_details.Activity.Info["Title"] + '" data-description="' + activity_details.Activity.Info["Description"] + '" data-program="' + val["Program"] + '" data-activity="' + val["Activity"] + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type"  data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help" data-help_type="Audience"  data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');

      });

      //Change the Exhibit Menu list of selected activities from empty to populated.
      $('#desktop .setup_container').removeClass('empty')
      $('#desktop .setup_container').addClass('populated')
      
      //Set this session's selected activities to the previously  selected activities.
      setSessionObject("selected_activities", selected_activities)
      
      //Initialize the scroll bar on the Exhibit Menu list of selected activities.
      initializeContainerScrollBar("#activities",true,false,true)

      //Notify the user that they were loaded.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('Reloaded Activities from Previous Exhibit.', 'success', 2, function() {});
    } 

    //If there were no previously selected activities, change the warning text.
    else {
      
      //Notify the user that they couldn't be loaded.
      alertify.set('notifier', 'position', 'bottom-center');
      var notification = alertify.notify('There were no activities to resume.', 'error', 2, function() {});

    }

  })

  //Exhibit menu button to clear all of the selected activities.
  $('#clear_exhibit').click(function() {
    
    //Create an empty object and set the selected_activities session object.
    var selected_activities = {};
    setSessionObject("selected_activities", selected_activities)

    //Empty the Exhibit menu list of selected activities.
    $('#activities .selected_activities').html("")

    //Change the Exhibit Menu list of selected activities from populated to empty.
    $('#desktop .setup_container').addClass('empty')
    $('#desktop .setup_container').removeClass('populated')
    
    //Destroy the scroll bar for the Exhibit menu list.
    destroyContainerScrollBar("#activities")

    $("#exhibit_presets").val("default").trigger('change');

    //Alert the user with a pop up notification.
    alertify.set('notifier', 'position', 'bottom-center');
    var notification = alertify.notify('Cleared Activities from Current Exhibit.', 'success', 2, function() {});
  })

  //Exhibit menu button to enter Exhibit Mode.  
  //When the user first clicks 'Enter Exhibit Mode', they will be prompted that they will need to restart the kiosk to return to Facilitator Mode.
  $('#exhibit_disclaimer').click(function() {
    if ($('#desktop .setup_container').hasClass('populated')) {
      
      //Create Popup 
      var options = {
        "Header":{
          "Style":"warning",
          "Text":"Enter into Exhibit Mode?"
        },
        "Scrollbar":false,
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="exhibit_cancel" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_exhibit">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"exhibit_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><p>Are you sure you want to enter Exhibit Mode?</p><p><i>Return to Facilitator Mode by tapping the bottom right corner of the screen three times.</i></p><p><i>Clicking this button will allow you to return to Facilitator Mode.</i></p></div>'
        }
      };
      createLookCloserPopUp(options);
      
    }
  })

  //When the user selects 'Confirm' on the disclaimer pop-up, this will run.
  $(document).on("click", '#confirm_exhibit', function() {
    
    //Close the current lightbox window.
    $.fancybox.destroy();

    //Reset any accessibility options that may have been set by the facilitator.
    clearAccessibility();

    //Get the program that is loaded and get more information about it.
    var default_program = getSetting("setting__default_program", "Default");
    
    //Set the title of the Exhibit Mode homescreen.
    var program_details = loadProgramDetails(default_program)
    $("#exhibit .exhibit_header").html(program_details["Program"])

    //Get the list of activities selected for Exhibit Mode.
    var selected_activities = getSessionObject("selected_activities", "")
    
    //Get the audiences configured in this program and the activity types available to CLIO.  
    var audiences = getAudiences(default_program);
    var activity_types = getActivityTypes();

    //Use the activities from the current session to set the local object.
    //This allows the activities to be resumed when a user opens the CLIO interface.
    setLocalObject("selected_activities", selected_activities)

    //Change from Facilitator Mode to Exhibit Mode.
    $('body').removeClass('facilitator');
    $('body').addClass('user')

    //Create the container for the Exhibit Mode activity carousel.
    $("#exhibit").append('<div class="owl-carousel owl-theme"></div>');

    //For each activity, get information about it and add a card to the carousel.
    $.each(selected_activities, function(index, val) {

      var activity_details = loadActivityDetails(val["Program"], val["Activity"])

      var preview_url = "content/" + default_program + "/@Media/" + activity_details.Activity.Info["Preview"]

      $("#exhibit .owl-carousel").append('<div class="activity_card activity_button" data-title="' + activity_details.Activity.Info['Title'] + '" data-description="' + activity_details.Activity.Info["Description"] + '" data-program="' + val["Program"] + '" data-audience="' + activity_details.Activity["Audience"] + '" data-activity="' + val["Activity"] + '"><div style="background-image: url(' + preview_url + ')" class="image closed"><div class="overlay"><span>' + activity_details.Activity.Info["Description"] + '</span></div></div><div class="details"><div class="title">' + activity_details.Activity.Info['Title'] + '</div><div class="info"></div></div></div>');

    });

    //Get the user setting for Slide duration, or use the default.
    var slide_duration = getSetting("setting__slide_duration", "10000");

    //Start the Exhibit Mode activity carousel.
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 0,
      nav: true,
      dots: true,
      items: 1,
      animateIn: "animate__backInLeft",
      animateOut: "animate__backOutRight",
      autoplay: true,
      autoplayTimeout: slide_duration,
      onChange: function(event) {
        
        setTimeout(
          function() 
          {
            $('#touch_overlay .touch').addClass('heartbeat')
            setTimeout(
              function() 
              {
                $('#touch_overlay .touch').removeClass('heartbeat')
              }, 4500
            )
          }, 1000
        )

      }
    })

  })

  /* ==========================================================================
    Exhibit Mode Events
    ========================================================================== */
  
  //Load the activity when a user clicks on an Exhibit Mode card.
  $(document).on("click", '.activity_card', function() {

    var url = getCurrentURL();

    var program = $(this).data("program");
    var activity = $(this).data("activity");

    loadActivity(program, activity);

    $('body').removeClass('homescreen');
    $('body').addClass('activity');

  })

  //Show the activity description when a user clicks the info button.
  $(document).on("click", '.activity_card .info', function(e) {
    
    $('.owl-carousel').trigger('stop.owl.autoplay');

    var title = $(this).parents('.activity_card').attr("data-title");
    var description = $(this).parents('.activity_card').attr("data-description");

    

    //Create Popup 
      var options = {
        "Header":{
          "Style":"default",
          "Text": title
        },
        "Scrollbar":"auto",
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="info_cancel" class="lookcloser_acknowledge" onclick="$.fancybox.destroy(); $(\'.owl-carousel\').trigger(\'play.owl.autoplay\');">Okay</button>',
          "Close":true,
          "Swipe":true,
          "Prefix":"info_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">' + description + '</div>'
        }
      };
      createLookCloserPopUp(options);

      TimeMe.setIdleDurationInSeconds(30);

      TimeMe.callWhenUserLeaves(function() {
        
        if($('body').hasClass("homescreen")){
          $.fancybox.destroy(); 
          $('.owl-carousel').trigger('play.owl.autoplay');
        }
          
      }, 1);

    

    e.stopPropagation();
    
  });


  $(document).on("change", '#exhibit_presets', function() {

    var desired_preset = $("#exhibit_presets").val();

    if(desired_preset != "default"){
      var name = $(this).find(':selected').attr('data-name')
      var activities = JSON.parse($(this).find(':selected').attr('data-activities'))
      $("#activities .selected_activities").html("");

      var program = getSetting("setting__default_program", "Default")
      
      //Get information about the audiences defined in the program and the activities available to CLIO.
      var audiences = getAudiences(program);
      var activity_types = getActivityTypes();
      var selected_activities = {}

      $.each(activities,function(index,val){

        
        

        //Get information about what program and activity is loaded from the URL.
        var url = getCurrentURL();
        var activity = val;

        //Create an object to contain formatted information about the activity that was just selected.
        //We will be creating an object containing the program and activity, using an index name that is the relative location of the activity.
        var selected_activity = {};
        var selected_activity_url = "content/" + program + "/@Activities/" + activity + ".json";

        

          //This takes the information we previously captured about the program and activity that was selected, and assigns it within the object we created earlier.
          selected_activity["Program"] = program;
          selected_activity["Activity"] = activity;

          //Using the relative url we created earlier as the index, we can push this new activity into the full list of selected activities.
          selected_activities[selected_activity_url] = selected_activity

          //Assign this new object, with the old and new activities, with the session.  This allows the interface to remember which activities are selected between menus and refreshes.
          setSessionObject("selected_activities", selected_activities)

          //Get information about the activity that was selected.
          var activity_details = loadActivityDetails(program, activity)

          //Pull the relevant information about the activity type.
          var type = activity_details.Activity["Type"];
          var type_details = activity_types[type]

          //Pull the relevant information about the audience.
          var audience = activity_details.Activity["Audience"];
          var audience_details = audiences[audience]

          //Process the descriptions as rich text.
          var type_description = BBCodeParser.process(type_details["Description"]);
          var audience_description = BBCodeParser.process(audience_details["Description"]);

          //Add this activity to the Exhibit menu list.
          $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + selected_activity_url + '" data-program="' + program + '" data-activity="' + activity + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help" data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');

          //Make sure the Exhibit menu activities container knows if it's empty and start the scrollbar.
          if ($('#desktop .setup_container').hasClass('empty') == true) {
            $('#desktop .setup_container').removeClass('empty')
            $('#desktop .setup_container').addClass('populated')

            initializeContainerScrollBar("#activities",true,false,true)
          }

          


      })

    


    } 

    else {

      //Make sure the Exhibit menu activities container knows if it's empty and start the scrollbar.
      if ($('#desktop .setup_container').hasClass('populated') == true) {
        $('#desktop .setup_container').removeClass('populated')
        $('#desktop .setup_container').addClass('empty')

        destroyContainerScrollBar("#activities")
      }

    }

      


  })



  $(document).on("click","#facilitator_access_wrapper",function(){

    var counter = parseInt($("#facilitator_access_wrapper").attr('data-click-counter'));

    if(counter >= 2){
      $("#facilitator_access_wrapper").attr('data-click-counter',"0")
      $("#facilitator_access").show();
      setTimeout(
        function() 
        {
          $("#facilitator_access").hide();
          $("#facilitator_access_wrapper").attr('data-click-counter',"0")
        }, 5000
      )
    } else {
      $("#facilitator_access_wrapper").attr('data-click-counter', parseInt(counter + 1));
    }
    

  })

   $(document).on("click","#facilitator_access",function(){
      

      //Create Popup 
      /*var options = {
        "Header":{
          "Style":"warning",
          "Text": "Return to Facilitator Mode?"
        },
        "Scrollbar":"auto",
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="cancel_facilitator" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_facilitator" onclick="window.location.reload();">Confirm</button>',
          "Close":true,
          "Swipe":true,
          "Prefix":"facilitatormode_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;">Are you sure you want to return the kiosk to Facilitator Mode?</div>'
        }
      };*/

      var options = {
        "Header":{
          "Style":"warning",
          "Text": "Enter Facilitator Pincode"
        },
        "Scrollbar":"auto",
        "Interaction":{
          "Type":"custom",
          "HTML":'<button id="cancel_facilitator" onclick="$.fancybox.destroy();">Cancel</button><button id="confirm_facilitator">Confirm</button>',
          "Close":false,
          "Swipe":false,
          "Prefix":"facilitatormode_",
        },
        "Content":{
          "Type":"html",
          "Data":'<div style="height: 100%; display: flex; flex-direction: column; text-align: center; font-size: 1.5em; justify-content: center;"><div id="pinpad"><form ><input type="text" id="password" /></br><input type="button" value="1" id="1" class="pinButton calc"/><input type="button" value="2" id="2" class="pinButton calc"/><input type="button" value="3" id="3" class="pinButton calc"/><br><input type="button" value="4" id="4" class="pinButton calc"/><input type="button" value="5" id="5" class="pinButton calc"/><input type="button" value="6" id="6" class="pinButton calc"/><br><input type="button" value="7" id="7" class="pinButton calc"/><input type="button" value="8" id="8" class="pinButton calc"/><input type="button" value="9" id="9" class="pinButton calc"/><br><input type="button" value="0" id="0 " class="pinButton calc"/></form></div></div>'
        }
      };

      createLookCloserPopUp(options);

      const input_value = $("#password");

      //disable input from typing
      $("#password").keypress(function () {
        return false;
      });

      //add password
      $(".calc").click(function () {
        let value = $(this).val();
        field(value);
      });

      function field(value) {
        input_value.val(input_value.val() + value);
      }

   });

  $(document).on("click","#confirm_facilitator",function(){
    var entered = $("#password").val();
    var pin = getSetting("pin", "2546")
    var url = getCurrentURL();
    if(entered == pin){
      if(url["mode"] == "exhibit"){

        setURLParameters("kiosk");
      }
      window.location.reload();
    } else {
      $("#password").val("");
      $("#password").addClass("incorrect");
      setTimeout(
        function() 
        {
          $("#password").removeClass("incorrect");
        }, 500
      )

    }
  });
});


/* ==========================================================================
    Click Events - Small Loading screen
    ========================================================================== */

//$(document).mousedown(function(){
//  $("#small_loading").show()
//})


/* ==========================================================================
    Usage Mode Scripts
    ========================================================================== */

function loadExhibitMode(program) {
  
  var url = getCurrentURL();
  //Add the embedded class to the body
  $('body').addClass('embedded');
  
  //Set the default program as the one defined within the URL.
  setLocal("setting__default_program", program)

  //Create the object for the activities we want to use in Exhibit Mode.
  var selected_activities = {};
  
  //Request a list of the activities in the prrogram.
  var activities = requestActivities(program);

  //Apply any custom branding used within the Program's CSS file.
  $('head').append('<link rel="stylesheet" href="' + url['base'] + 'content/' + program + '/branding.css" type="text/css" />')

  //Change from Facilitator Mode to User Mode.
  $('body').removeClass('facilitator');
  $('body').addClass('user')

  //Set the title of the Exhibit Mode homescreen.
  var program_details = loadProgramDetails(program)
  $("#exhibit .exhibit_header").html(program_details["Program"])

  //Create the container for the activity carousel.
  $("#exhibit").append('<div class="owl-carousel owl-theme"></div>');

  //For each activity, load it's details and add it to the carousel if it is not marked as hidden.
  $.each(activities, function(index, val) {

    var activity_details = loadActivityDetails(program, val)

    var activity = val;
    console.log(program,activity)

    if (activity_details.Activity.Configuration['Hidden'] == "false") {
      var selected_activity = {};
      var selected_activity_url = "content/" + program + "/@Activities/" + activity + ".json"

      selected_activity["Program"] = program;
      selected_activity["Activity"] = activity;

      selected_activities[selected_activity_url] = selected_activity

      var preview_url = "content/" + program + "/@Media/" + activity_details.Activity.Info["Preview"]
      $("#exhibit .owl-carousel").append('<div class="activity_card activity_button" data-program="' + program + '" data-audience="' + activity_details.Activity["Audience"] + '" data-activity="' + activity + '"><div style="background-image: url(' + preview_url + ')" class="image closed"><div class="overlay"><span>' + activity_details.Activity.Info["Description"] + '</span></div><div class="touch heartbeat"></div></div><div class="details"><div class="title">' + activity_details.Activity.Info['Title'] + '</div><div class="info"></div></div></div>');
    
    }

  })

  setSessionObject("selected_activities",selected_activities)

  //Start the Exhibit Mode homescreen carousel.
  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    items: 1,
      animateIn: "none",
      animateOut: "none",
    autoplay: true,
    autoplayTimeout: 10000,
    onChange: function() {
      
    }
  })
}

function loadKioskMode(program) {
  $('body').removeClass('facilitator')
  $('body').addClass('kiosk')

  var audiences = getAudiences(program);
  var activity_types = getActivityTypes();
  var screensaver = getLocal("setting__screensaver", "60000")

  if(screensaver == "disabled"){
    $('body').addClass('noscreensaver');
    $('#screensaver_script').remove();
  } else {
    $('body').removeClass('noscreensaver');
  }

  //Get the last used brightness setting for the display and set the device brightness to this.
  var device_brightness = getLocal("device_brightness", "4")
  setScreenBrightness(device_brightness)
  
  var program_details = loadProgramDetails(program)

  

  //Set the title of the Program for Facilitator Mode.
  $('#setup_header').html(program_details['Program']);

  //See if there are any activities that have already been selected during this session.
  var selected_activities = getSessionObject("selected_activities", "")

  //If there are previously selected activities, do this.
  if (!jQuery.isEmptyObject(selected_activities)) {

    //Go through each activity and add it to the Facilitator Mode Exhibit menu.
    $.each(selected_activities, function(index, val) {

      //Get details for the activity.
      var activity_details = loadActivityDetails(val["Program"], val["Activity"])

      //Get information about the activity type.
      var type_details = activity_types[activity_details.Activity["Type"]]

      //Get information about the audience.
      var audience_details = audiences[activity_details.Activity["Audience"]]

      //Process the activity type and audience descriptions as rich text.
      var type_description = BBCodeParser.process(type_details["Description"]);
      var audience_description = BBCodeParser.process(audience_details["Description"]);

      //Add a button for this activity to the Exhibit menu list of currently selected activities.
      $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '"  data-title="' + activity_details.Activity.Info["Title"] + '" data-description="' + activity_details.Activity.Info["Description"] + '" data-program="' + val["Program"] + '" data-activity="' + val["Activity"] + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help"  data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');
    });

    //Change the Exhibit Menu list of selected activities from empty to populated.
    $('#desktop .setup_container').removeClass('empty')
    $('#desktop .setup_container').addClass('populated')

    //Initialize the scroll bar on the Exhibit Menu list of selected activities.
    initializeContainerScrollBar("#activities",true,false,true)
  }

  


  

  
}

function loadFacilitatorMode(program){
  var program_details = loadProgramDetails(program)

  var audiences = getAudiences(program);
  var activity_types = getActivityTypes();

  //Set the title of the Program for Facilitator Mode.
  $('#setup_header').html(program_details['Program']);

  //See if there are any activities that have already been selected during this session.
  var selected_activities = getSessionObject("selected_activities", "")

  //If there are previously selected activities, do this.
  if (!jQuery.isEmptyObject(selected_activities)) {

    //Go through each activity and add it to the Facilitator Mode Exhibit menu.
    $.each(selected_activities, function(index, val) {

      //Get details for the activity.
      var activity_details = loadActivityDetails(val["Program"], val["Activity"])

      //Get information about the activity type.
      var type_details = activity_types[activity_details.Activity["Type"]]

      //Get information about the audience.
      var audience_details = audiences[activity_details.Activity["Audience"]]

      //Process the activity type and audience descriptions as rich text.
      var type_description = BBCodeParser.process(type_details["Description"]);
      var audience_description = BBCodeParser.process(audience_details["Description"]);

      //Add a button for this activity to the Exhibit menu list of currently selected activities.
      $("#activities .selected_activities").prepend('<div class="activity ' + activity_details.Activity["Type"] + ' activity activity_button" data-url="' + index + '"  data-title="' + activity_details.Activity.Info["Title"] + '" data-description="' + activity_details.Activity.Info["Description"] + '" data-program="' + val["Program"] + '" data-activity="' + val["Activity"] + '" data-audience="' + activity_details.Activity["Audience"] + '" ><div class="controls"><div class="checkbox selected"></div></div><div class="content"><div class="activity_header"><div class="title">' + activity_details.Activity.Info["Title"] + '</div><div class="grouping"><div class="group">' + activity_details.Activity.Grouping["Section"] + '</div><div class="arrow"></div><div class="group">' + activity_details.Activity.Grouping["Exhibit"] + '</div></div></div><div class="activity_description">' + activity_details.Activity.Info["Description"] + '</div><div class="info"><span><b>Type:</b> ' + type_details["Name"] + '</span><span class="help" data-help_type="Activity Type" data-description="' + type_description + '" data-title="' + type_details["Name"] + '"></span><span><b>Audience:</b> ' + audience_details["Name"] + '</span><span class="help"  data-help_type="Audience" data-description="' + audience_description + '" data-title="' + audience_details["Name"] + '"></span></div><div class="activity_link"></div></div></div>');
    });

    //Change the Exhibit Menu list of selected activities from empty to populated.
    $('#desktop .setup_container').removeClass('empty')
    $('#desktop .setup_container').addClass('populated')

    //Initialize the scroll bar on the Exhibit Menu list of selected activities.
    initializeContainerScrollBar("#activities",true,false,true)
  }
}



function loadActivityMode(program,activity){
  $('body').addClass('standalone')
  setLocal("setting__default_program", program)
}

function loadChromelessMode(program,activity){
  $('body').addClass('chromeless')
  setLocal("setting__default_program", program)
}