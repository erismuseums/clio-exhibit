# CLIO Exhibit

[CLIO Exhibit](https://gitlab.com/erismuseums/clio-exhibit) can be used to display interactive activities in online exhibits, video conferences and physical kiosks. This software is used in conjunction with [CLIO Create](https://gitlab.com/erismuseums/clio-create) to create and manage exhibit presets. 

## Installation

**CLIO Exhibit** requires a web server with PHP v5.6 or newer.

Installation guides can be found on our wiki [here](https://www.cliomuseums.org/wiki).

## Usage

Once the **CLIO Exhibit** web application has been installed in the web server's host directory, it can be accessed through the *localhost* or *127.0.0.1* web address using a web browser.

## Contributing
Bug reports and feature requests are welcome.

## License
[MIT](https://choosealicense.com/licenses/mit/)