#!/usr/bin/python3.7

# When a user changes the brightness in the CLIO interface, it will use AJAX to load a PHP page which will evoke this script.
# This script runs a Raspberry Pi 3B+ specific command to set the brightness to 204/255.

import os
myCmd = 'echo 204 > /sys/class/backlight/rpi_backlight/brightness'
os.system(myCmd)