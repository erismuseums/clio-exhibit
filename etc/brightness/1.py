#!/usr/bin/python3.7

# When a user changes the brightness in the CLIO interface, it will use AJAX to load a PHP page which will evoke this script.
# This script runs a Raspberry Pi 3B+ specific command to set the brightness to 20/255, effectively disabling the screen backlight.
# Setting this lower will cause the screen to black out completely, making it impossible to use.

import os
myCmd = 'echo 20 > /sys/class/backlight/rpi_backlight/brightness'
os.system(myCmd)