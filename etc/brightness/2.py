#!/usr/bin/python3.7

# When a user changes the brightness in the CLIO interface, it will use AJAX to load a PHP page which will evoke this script.
# This script runs a Raspberry Pi 3B+ specific command to set the brightness to 25/255.
# Setting this to 20 effectively disables the backlight.  25 was chosen as a good low light environment option.

import os
myCmd = 'echo 25 > /sys/class/backlight/rpi_backlight/brightness'
os.system(myCmd)