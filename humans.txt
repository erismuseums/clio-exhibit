# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Joshua Frechette -- Developer and Designer -- @metaphorraccoon
    Dillon Connelly -- Education and Evaluation -- @museumCLIO

# THANKS

    University of Washington Compassion Seed Grant
    Burke Museum of Natural History and Culture
    Slater Museum of Natural History
    
    Peter Wimberger
    Mateo Garcia
    Katharine Canning
    Pamela Maldonado
    Jessica Luke
    Holly Barker

    Open-source projects like ours are built upon the countless hours of work that open-source developers dedicate to creating tools for everyone and the communities who support them.  Thank you.
    

# TECHNOLOGY COLOPHON

    CSS3, HTML5, JavaScript, AJAX, PHP, Python
    Google Material Icons
    HTML5 Boilerplate, et al.
    
    Apache Server Configs, jQuery, Modernizr, Normalize.css
    BBCode Parser by Frug, Fancybox, mCustomScrollbar, Alertify, Select2, Owl Carousel, Hammer, TwentyTwenty, jPlayer, Three, Timelinr, Reveal

    Raspberry Pi Foundation, Adafruit
    Debian, DietPi, Apache Server, Unclutter, Matchbox Window Manager
    
