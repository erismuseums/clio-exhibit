{	
	"Activity":{
		"Type":"AnnotatedImage",
		"Audience":"Online",
		"Grouping":{
			"Section":"Nature in the Classroom",
			"Exhibit":"Bird Diversity"
		},
		"Configuration":{
			"Hidden":"true",
			"Theme":"slater",
			"Accessibility":"true",
			"Subheader":"false"
		},
		"Info":{
			"Title":"Shrub-Steppe Landscape",
			"Preview":"buildabird/shrubsteppe/background.png",
			"Description": "",
			"Video":""
		}		
	},
	"Content":{
		"Configuration":{
			"Start Position":"0",
			"ScaleToFit":"true",
			"Image":{
				"Source":"buildabird/shrubsteppe/background.png",
				"Header":"Shrub-Steppe",
				"Description":"This is a shrub-steppe biome. This kind of habitat is the largest natural grassland in North America and stretches from eastern Washington all the way to western Colorado. “Shrub” refers to the most common kind of plant you’ll find here, and “steppe” refers to the lack of trees in this habitat. In addition to shrubs and grasses, tiny forms of life like algae, fungi, and moss work together to protect the soil as well as provide nutrients for plants and animals alike.",
				"Dimensions":{
					"Height":720,
					"Width": 1280
				}
			}
			
		},
		"References":{
			"Configuration":{
				"Toggleable":false,
				"Fixed":false,
				"Invisible":false
			},
			"Tooltips":{
				
				"1":{
					"Geometry":{
						"Position":{
							"x":"570",
							"y":"570"
						},
						"Radius":40
					},
					"Title":"Water",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/FrenchmanCoulee.jpg",
							"caption": "How might your bird deal with scarce water sources? Consider what food they might get it from! Some sources of food provide more water than others; just as it might take more energy to hunt down a mouse than it does to hop from one plant to another."
						}]
					}
				},
				
				"2":{
					"Geometry":{
						"Position":{
							"x":"510",
							"y":"330"
						},
						"Radius":40
					},
					"Title":"Burrow",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/WAGroundSquirrel.jpg",
							"caption": "Some animals like the Washington Ground Squirrel will keep cooler nests underground. Will your bird be one of them? Or will your bird hunt them?"
						}]
					}
				},

				"3":{
					"Geometry":{
						"Position":{
							"x":"690",
							"y":"410"
						},
						"Radius":40
					},
					"Title":"Shrubs",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/FC-sagebrush.jpg",
							"caption": "As the name suggests, there are mostly shrubs (and grasses) and few trees in this kind of habitat."
						},{
							"src": "buildabird/shrubsteppe/BigSagebrush.jpg",
							"caption": "What feet types do you think might be most useful? Think about what your bird will be eating and where."
						}]
					}
				},

				"4":{
					"Geometry":{
						"Position":{
							"x":"220",
							"y":"310"
						},
						"Radius":40
					},
					"Title":"Prey",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/PShortHornedLizard.jpg",
							"caption": "Species that are often eaten by bigger animals have well-adapted defenses; take this Pygmy Short Horned Lizard for example. When seen from above, only its sudden movement will betray the camouflage its color and texture creates."
						},{
							"src": "buildabird/shrubsteppe/SagebrushVole.jpg",
							"caption": "The tiny sagebrush vole, only about 13 cm long, is another possible food source for a bird of prey. They burrow and are mainly active at dawn and dusk to avoid the heat and visibility of day."
						}]
					}
				},

				"5":{
					"Geometry":{
						"Position":{
							"x":"1140",
							"y":"550"
						},
						"Radius":40
					},
					"Title":"Flowers and Seeds",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/NGoldendale-bloom.jpg",
							"caption": "Nectar from flowers like these provide good provisions for hummingbirds; the buds and seeds also make good meals for a bird."
						},{
							"src": "buildabird/shrubsteppe/NuttallsCottontail.jpg",
							"caption": "You might have to compete with rabbits and other herbivores for those provisions!"
						},{
							"src": "buildabird/shrubsteppe/Yellow-belliedMarmot.jpg",
							"caption": "You might have to compete with rabbits and other herbivores for those provisions!"
						}]
					}
				},

				"6":{
					"Geometry":{
						"Position":{
							"x":"150",
							"y":"550"
						},
						"Radius":40
					},
					"Title":"Predators",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/OkanoganCounty.jpg",
							"caption": "There aren’t a lot of large predators out here, but also not a lot of easy places to hide."
						},{
							"src": "buildabird/shrubsteppe/Coyote.jpg",
							"caption": "Coyotes might seem like a difficult predator to encounter, but birds aren’t very high up on their preferred meals list. Young birds and bird nests should still beware!"
						},{
							"src": "buildabird/shrubsteppe/GopherSnake.jpg",
							"caption": "Another unlikely but formidable predator your bird might encounter is the Gopher Snake. They mostly eat small mammals, but are also known to eat bird eggs and small birds, too. If your bird is big enough, this might be prey!"
						}]
					}
				},

				"7":{
					"Geometry":{
						"Position":{
							"x":"900",
							"y":"300"
						},
						"Radius":40
					},
					"Title":"Lichen",
					"Icon":{
						"Image":"icons/zoom_in-white.svg",
						"Scale":"60px"
					},
					"Content":{
						"Type":"Gallery",
						"Data":[{
							"src": "buildabird/shrubsteppe/lichen.jpg",
							"caption": "Not only is this stuff pretty--it makes all sorts of life possible! This is lichen; it’s what happens when well-suited fungi and algae thrive together."
						},{
							"src": "buildabird/shrubsteppe/CSagebrushLizard.jpg",
							"caption": "“It does make for good nesting material, but it also provides food for insects...which are good food for a bird.  Your bird might still have to compete with a lizard or two for those bugs!"
						}]
					}
				}

			}

		}

	}
		
}