#!/bin/bash
### Description: CLIO Exhibit Install script
### Install CLIO Exhibit and optionally a web server

### Boilerplate Warning
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
#LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
#OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

scriptversion="1.0.0"
scriptdate="2022-12-12"

set -euo pipefail
echo -e "\e[1;32m--------------------------------------------------\e[0m"

echo -e "\e[1;32mRunning CLIO Install Script\e[0m"
echo -e "\e[1;32mVersion [$scriptversion] as of [$scriptdate]\e[0m"

# Verify running as root

if [ "$EUID" -ne 0 ]; then
    echo "Please run as root, such as 'sudo ./clio-install.sh'"
    exit
fi

############################
#      Install Apache      #
############################

echo -e "\e[1;32m--------------------------------------------------\e[0m"

if test -f "/usr/sbin/apachectl";
then
    echo -e "\e[1;32mApache2 HTTP web server detected. Skipping installation.\e[0m"
    
    
else

    echo -e "\e[1;32mApache2 HTTP web server not installed.\e[0m"
    echo -e "\e[1;32mDo you want to install the Apache2 http web server and the PHP module? [Yes/No]\e[0m"

    select yn in "Yes" "No"; do
	    case $yn in
	    Yes) break ;;
	    No) exit 0 ;;
	    esac
	done

    echo -e "\e[1;32mYou will need to update package lists and upgrade your system before installing Apache2.\e[0m"
    echo -e "\e[1;32mContinue with the installation [Yes/No]?\e[0m"

	select yn in "Yes" "No"; do
	    case $yn in
	    Yes) break ;;
	    No) exit 0 ;;
	    esac
	done
      
      echo -e "\e[1;32mInstalling Updates\e[0m"
      apt-get update
      apt-get upgrade -y
      
      echo -e "\e[1;32mInstalling Apache2 and PHP Modules\e[0m"
      apt-get install -y apache2 php7.4 php-cli libapache2-mod-php php-mysql
      
      echo -e "\e[1;32mEnabling Apache2 web server on startup.\e[0m"
      systemctl enable apache2

      echo -e "\e[1;32mActivating PHP modules.\e[0m"
      a2dismod mpm_event
      a2enmod php7.4

      echo -e "\e[1;32mRestarting Apache2 web server.\e[0m"
      systemctl restart apache2
fi

############################
#     Install Chromium     #
############################

echo -e "\e[1;32m--------------------------------------------------\e[0m"

if test -f "/usr/bin/chromium";
then
    echo -e "\e[1;32mChromium web browser detected. Skipping installation.\e[0m"
    
    
else

    echo -e "\e[1;32mChromium web browser not installed.\e[0m"
    echo -e "\e[1;32mDo you want to install the Chromium web browser? [Yes/No]\e[0m"

    select yn in "Yes" "No"; do
        case $yn in
        Yes) break ;;
        No) exit 0 ;;
        esac
    done
      
      echo -e "\e[1;32mInstalling Chromium web browser.\e[0m"
      apt-get install -y chromium
      
fi




############################
#       Clear Dir        #
############################

echo -e "\e[1;32m--------------------------------------------------\e[0m"
echo -e "\e[1;32mCLIO will be installed into the Apache web root directory (/var/www/html) and all existing contents will be deleted.\e[0m"
echo -e "\e[1;32mContinue with the installation [Yes/No]?\e[0m"

    select yn in "Yes" "No"; do
        case $yn in
        Yes) break ;;
        No) exit 0 ;;
        esac
    done

rm -rf /var/www/html/*

############################
#       Install Git        #
############################

echo -e "\e[1;32m--------------------------------------------------\e[0m"


if test -f "/usr/bin/git";
then
    echo -e "\e[1;32mGit detected. Skipping installation.\e[0m"

else
    echo -e "\e[1;32mGit is required to download the most up-to-date CLIO releases.\e[0m"
    echo -e "\e[1;32mGit is not installed.\e[0m"
    echo -e "\e[1;32mDo you want to install Git? [Yes/No]\e[0m"

    select yn in "Yes" "No"; do
	    case $yn in
	    Yes) break ;;
	    No) exit 0 ;;
	    esac
	done
      
    echo -e "\e[1;32mInstalling Git.\e[0m"
    sudo apt-get -y install git
    
fi

############################
#       Install CLIO       #
############################

echo -e "\e[1;32m--------------------------------------------------\e[0m"
echo -e "\e[1;32mWhich web application would you like to install?\e[0m"

select app in exhibit create quit; do

    case $app in
    exhibit)
        git clone https://gitlab.com/erismuseums/clio-exhibit.git
        cd ./clio-exhibit
        mv * /var/www/html/
        cd ..
        rm -rf ./clio-exhibit

        break
        ;;

    create)
        git clone https://gitlab.com/erismuseums/clio-create.git
        cd ./clio-create
        mv * /var/www/html/
        cd ..
        rm -rf ./clio-create

        break
        ;;

    quit)
        exit 0
        ;;
    *)
        echo "Invalid option"
        ;;
    esac
done

chown -R www-data:www-data /var/www/html
chmod go-rwx /var/www/html
chmod go+x /var/www/html
chgrp -R www-data /var/www/html
chmod -R go-rwx /var/www/html
chmod -R g+rx /var/www/html
chmod -R g+rwx /var/www/html

echo -e "\e[1;32m--------------------------------------------------\e[0m"
echo -e "\e[1;32m              Installation Complete               \e[0m"
echo -e "\e[1;32m--------------------------------------------------\e[0m"


echo -e "\e[1;32mCLIO can be accessed through the Chromium web browser at http://localhost/\e[0m"

