<?php

//This PHP page is loaded through an AJAX call when a script needs to know the contents of a directory.
//Get the desired directory from the URL 'directory' parameter.  
$path = $_GET['directory'];

//Scan the directory to get all of its contents.
//Remove extraneous dots and configuration files.
$files = array_diff(scandir($path), array('.', '..','.DS_Store','._.DS_Store','desktop.ini'));

//This helps ensure that any file forks, such as ._file.txt, are removed from the list.
$files_updated = preg_grep('/^([^.])/', $files);

//This will reindex the array to ensure that it always starts at 0 and is numbered sequentially.
$files_reindex = array_values($files_updated);

//This encodes the response as a JSON object.
$json = json_encode($files_reindex);

//This prints the JSON object to be used as a response by the AJAX function that requested this directory's content.
echo $json
?>